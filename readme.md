This project includes the source codes of GFNet and MF predictor and pre-trained models and data to reproduce our results in the paper.  This file represents a high-level overview of the project's structure and demonstrates how to set up the software dependencies. 



### Project Structure

The two directories `Laplace` and `NS` are for the Laplace and Navier-Stokes equations respectively. Each directory contains the following files/sub-directories:

* `readme.md` shows how to use the code for training a model and running MF predictor for an unseen domain.

* `src/` includes the implementation of GFNet and MF predictor, the script to train GFNet, and the script to run MF predictor for unseen domains with unseen boundary conditions (BCs).
* `data/` includes the training data for GFNet and the solution data used in our evaluations.
* `models/` includes the pre-trained models used in our paper.



We evaluate our models against the state-of-the-art PINN models. We modify the open-source PINN projects and provide our modified version in the directory `./PINN` as follows:

* `PINN_Laplace_AMG.py`

  This code is released  at <https://github.com/maziarraissi/PINNs.git>. We modify it to solve the Laplace equation on square domains as described in Section 4.1.3.

* `XPINN_Laplace.py`

  This code is released at <https://github.com/AmeyaJagtap/XPINNs>. We modify it to solve the Laplace equation on a 4x4 domain.

* `NS.py` and `NS_model_tf.py` 

  Those codes are released at <https://github.com/PredictiveIntelligenceLab/GradientPathologiesPINNs.git>. They solve the lid-driven cavity benchmark with a uniform velocity on the top lid. We change the Reynolds number to Re = 500 and set the same velocity profile on the top lid as the one used in ground truth (Figure 6).

  
We compare our designed GFNets with two operator learners Fourier neural operator and DeepONet, found at ./FNO/ and ./DeepONet/ directories respectively. 

* `readme.md` shows how to use the code for training a model and running MF predictor for an unseen domain for these operators.
* `src/` includes the implementation of GFNet and MF predictor
* `Genomes/'  includes the script to run MF predictor for unseen domains with unseen boundary conditions (BCs).
* `models/` includes the pre-trained models used in our paper.
* `codes/` includes several codes to conduct tests on the operators
* `train/` includes the codes to train the GFNet

  

### Software Dependencies

Our project is built based on TensorFlow. GFNet and MF Predictor use TensorFlow features that are only available since 2.2.0 such as overriding the train\_step function, while PINN codes use features in TensorFlow 1 that have become deprecated in TensorFlow 2 such as placeholders, sessions, etc. As a result, to reproduce the results in our paper, one needs to setup two environments with TensorFlow 2 (>=2.20) and TensorFlow 1 (>=1.80), respectively.  
FNO uses Pytorch 1.9.0 and DeepONet  uses Tensorflow 2.

We use anaconda 2020.07 linux version (conda 4.8.3) to set up our software environments. We provide the .yml files (`tf2.yml` and `pinn_tf1.yml`) to detail the setup of environments using Tensorflow 2 and TensorFlow 1, respectively.  

* Each .yml file lists all the libraries and packages installed in the conda environment

* One can re-create our environment using conda:

  ```
  conda env create -f tf2.yml
  conda env create -f pinn_tf1.yml
  ```

  This downloads and installs all the dependencies. 



