#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 10:51:22 2021

@author: robertplanas
"""

import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-m', '--model', default= "DON_Lap_actB-swish_HLB-5_UB-100_actT-swish_HLT-5_UT-100_P-100_reg-0e+00_alpha-0e+00_flr-0.9_Pat-100_Iter-10000_nTrain-8000",
                    help='Select a model')
parser.add_argument('-d', '--data', default="../data_AMG/Sin_BC_1_AMG_2x2.h5",
                    help='Select a data file')
parser.add_argument('-DataType', '--DataType', default="float32",
                    help='float32 or float64, must match the model')
parser.add_argument('-n', '--name', default="BC = sin(x/Width)",
                    help='Name of the output file ')
parser.add_argument('-print', '--print', default=False,
                    help='Print to a file if true')
parser.add_argument('-reproduce', '--reproduce', default=False, action='store_true',\
                    help='reproduce the comparison in paper')
parser.add_argument('-it', '--iterations', type = int, default = 500,
                    help = 'Number of iterations')
parser.add_argument('-tol', '--tolerance', type = float, default = 3e-8,
                    help = 'MSE tolerance for the iterative approach')


args = parser.parse_args()


import sys
import os
import h5py as h5
import numpy as np
import tensorflow as tf
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../models/"))
sys.path.append('./../src/')
import DeepONet as DON
import Genomes_DON as G
import time
import Print_to_File as P2F
from matplotlib import pyplot as plt




#%% =============================================================================
# Load Model
# =============================================================================
Data_Type = args.DataType
tf.keras.backend.set_floatx(Data_Type)
modelPath = os.path.join(os.path.dirname(__file__), "../models/")
modelName = args.model

#Get the structure of the model from the name: 
act_B = modelName[int(modelName.find("_actB-") + 6):int(modelName.find("_HLB")) ]
HL_B = int(modelName[int(modelName.find("_HLB-") + 5):int(modelName.find("_UB-")) ])
UB = int(modelName[int(modelName.find("_UB-") + 4):int(modelName.find("_actT-")) ])
act_T = modelName[int(modelName.find("_actT-") + 6):int(modelName.find("_HLT")) ]
HL_T = int(modelName[int(modelName.find("_HLT-") + 5):int(modelName.find("_UT-")) ])
UT = int(modelName[int(modelName.find("_UT-") + 4):int(modelName.find("_P-")) ])
P = int(modelName[int(modelName.find("_P-") + 3):int(modelName.find("_reg-")) ]) 


DON_Model = DON.MF_DeepONet_UnStacked_Lap(HL_B = HL_B, Units_B = UB, P = P, 
                 HL_T = HL_T, Units_T = UT, 
                activation_B = act_B, activation_T = act_T,Batch = 20, Dat = 1024, nCol = 500,
                ) 
DON_Model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3))

DON_Model.compile(optimizer=tf.keras.optimizers.Adam(1e-3))
void_input = np.zeros((100,32,32))
void_input_xy = np.zeros((1024,2))
Out_1 = DON_Model((void_input, void_input_xy))
Path_res = os.path.join(os.path.dirname(__file__), "../res/")

DON_Model.load_weights((Path_res + modelName + "/" + modelName + ".h5" ))
Out_2 = DON_Model((void_input, void_input_xy))
Out_Substract = tf.reduce_mean(tf.square(Out_1 - Out_2))
print("Loaded Weights Prediction = ", Out_Substract.numpy)
DON_Model.trainable = False
# Freeze the weights
for layer in DON_Model.layers:
    layer.trainable = False

#Optimization_Error = 8.9349e-04
Optimization_Error = 2.19e-04


#%% ===========================================================================
# Set up the experiments
# =============================================================================

MAEs = []
TIMES = []
Strings = []

if args.reproduce:

    Data = ["../data_AMG/Sin_BC_1_AMG_1x1.h5",
            "../data_AMG/Sin_BC_1_AMG_2x2.h5",
            "../data_AMG/Sin_BC_1_AMG_3x3.h5",
            "../data_AMG/Sin_BC_1_AMG_4x4.h5",
            "../data_AMG/Sin_BC_1_AMG_5x5.h5",
            "../data_AMG/Sin_BC_1_AMG_6x6.h5",
            "../data_AMG/Sin_BC_1_AMG_8x8.h5",
            "../data_AMG/Sin_BC_1_AMG_10x10.h5",
            "../data_AMG/Sin_BC_2_AMG_1x1.h5",
            "../data_AMG/Sin_BC_2_AMG_2x2.h5",
            "../data_AMG/Sin_BC_2_AMG_3x3.h5",
            "../data_AMG/Sin_BC_2_AMG_4x4.h5",
            "../data_AMG/Sin_BC_2_AMG_5x5.h5",
            "../data_AMG/Sin_BC_2_AMG_6x6.h5",
            "../data_AMG/Sin_BC_2_AMG_8x8.h5",
            "../data_AMG/Sin_BC_2_AMG_10x10.h5"]
    
    Names = ["BC = sin(x/1)",
             "BC = sin(x/2)",
             "BC = sin(x/3)",
             "BC = sin(x/4)",
             "BC = sin(x/5)",
             "BC = sin(x/6)",
             "BC = sin(x/8)",
             "BC = sin(x/10)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)"]
    args.print = True 
else:
    Data = [args.data]
    Names = [args.name]
    
#%% ===========================================================================
# Run the experiments
# =============================================================================    

for index, data in enumerate(Data):    
    # =========================================================================
    # Load data, note that you have to recreate it 
    # =========================================================================
    
    File_AMG = os.path.join(os.path.dirname(__file__), data)
    psnFile = h5.File(File_AMG, 'r')
    nSample = psnFile.attrs['nSample']
    nCell = 31
    psnData = psnFile['LaplaceSolution']
    print('# Samples: {}'.format(nSample))
    print('#Genome Cells: {}*{}'.format(nCell +1, nCell +1))
    print('data shape: ', end='')
    print(psnData.shape)
    p = np.zeros(psnData.shape)
    p = psnData[:, :, :]
    pMin, pMax = np.amin(p[:, :, :]), np.amax(p[:, :, :])
    print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))
    psnFile.close()
    #%
    # Define the width of the domain
    Width = (len(p[0, :, :]) - 1) / nCell
    
    # Default max and min values used in training to normalize the inputs
    MIN = -1.199990063905716
    MAX = 1.1999822501681834
    
    
    # Normalize
    U = np.squeeze(p)
    U_N = G.Normalize_Domain(np.squeeze(
        p), Min=MIN, Max=MAX).astype(Data_Type)
    
    
    # Define the domain with ones, excluding the halo borders
    Domain_Ones = np.ones_like(U_N)
    Domain_Interior_Ones = np.zeros_like(U_N)
    Domain_Interior_Ones[1:-1,1:-1] = 1
    Domain_Borders = np.ones_like(U_N)*U_N
    Domain_Borders[1:-1, 1:-1] = 0
    
    # Generate the genomes
    Genomes_List= G.Generate_Genomes_Automatically(Width=Width, Height=Width, Genome_width=1,
                                                   Points_Borders=31, Domain_Ones=Domain_Ones,
                                                   DataType = tf.float32)
    
    # Solve the domain
    Solution, Time, MAE = G.Solve_Domain_Iterative(Genomes_List, Domain_Borders, Domain_Interior_Ones,
                 DON_Model, Batch_Size = None, DataType = tf.float32, max_iter = args.iterations, min_tol = args.tolerance,
                 Real_Solution = U, Min = MIN, Max = MAX)
    
    
    Current_Area = "\n #DeepONet ------- " + Names[index] +  " Area " + str(Width**2) + " Data_Type = " + Data_Type + "  -------"
    Preface_SP = "DON_Generalization_Error_SP = [] \n DON_MAE_Test_SP = [] \n DON_Assembly_Error_SP = [] \n DON_MF_Predictor_Error_SP = [] \n \n"
    Preface_MP = "DON_Generalization_Error_MP = [] \n DON_MAE_Test_MP = [] \n DON_Assembly_Error_MP = [] \n DON_MF_Predictor_Error_MP = [] \n \n"
    MAEs.append(MAE)
    TIMES.append(Time)
    Strings.append(Current_Area)
    print(f"MAE = {MAE:.3e}, Time = {Time:.1e}")
    
    if(args.print):
        
        Name_File = os.path.join(os.path.dirname(__file__), "Test_Iterative_Square")
        P2F.Print_String_File(Name_File, Current_Area)
        P2F.Print_vector_File(Name_File, [MAE], "MAE")
        P2F.Print_vector_File(Name_File, [Time], "Times")
    
    MF_Predictor_MAE = MAE
    
    # Compute the Test error
    
    mae_test = 0
    
    Basic_Genomes = Genomes_List[0]
    for Genome in Basic_Genomes:
        Input = Genome.Update_Inputs(U_N)
        Input = tf.expand_dims(tf.convert_to_tensor(Input), axis = 0)
        U_p = DON_Model.Predict(Input)
        U_p = G.Un_Normalize_Domain(U_p, MIN, MAX)
        err  = abs(U[Genome.i0 +1:Genome.i0+31, Genome.j0+1:Genome.j0 + 31] - U_p[0, 1:-1, 1:-1])
        mae_test += np.sum(err, axis=(0,1))
    mae_test = mae_test / (31-1) / (31-1) / len(Basic_Genomes)
        

    Name_File = os.path.join(os.path.dirname(__file__), "Operator_Comparison")
    if(index == 0):
        P2F.Print_String_File(Name_File, Preface_SP)
        P2F.Print_String_File(Name_File, Preface_MP)
    P2F.Print_String_File(Name_File, Current_Area)
    P2F.Print_vector_File(Name_File, [mae_test - Optimization_Error], "Generalization_Error")
    P2F.Print_vector_File(Name_File, [mae_test], "MAE_Test")
    P2F.Print_vector_File(Name_File, [MF_Predictor_MAE - mae_test], "Assembly_Error")
    P2F.Print_vector_File(Name_File, [MF_Predictor_MAE], "MF_Predictor_Error")
        

print("Final resutls -----------")
for index, MAE in enumerate(MAEs):
    Time = TIMES[index]
    print(Strings[index])
    print("       MAE = ", MAE)
    print("       Elapsed Time = ", Time)
