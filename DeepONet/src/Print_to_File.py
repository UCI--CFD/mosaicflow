#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 11:03:40 2021

@author: robertplanas
"""

# =============================================================================
# Print to  File
# =============================================================================

import os 
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
import numpy as np

def Print_vector_File(File_Name, Vector, Vector_Name):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt, "a")
    file1.write(Vector_Name + " = [")
    for index, element in enumerate(Vector):
        if(index == len(Vector) - 1):
            file1.write(f" {element:.2e}" + "]" + "\n")
        else:
            file1.write(f" {element:.2e}" + ',')
    file1.close()


def Print_String_File(File_Name, String):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt, "a")
    file1.write(String + " \n")
    file1.close()

def Plot_Cavity_Single(Solution, n = 65, Width = 1, Name = "Example"):
    
    Path = "../Genomes/" + Name + "/"
    Path_To_Save_Res = os.path.join(os.path.dirname(__file__), Path)
    if not os.path.exists(Path_To_Save_Res):
        os.makedirs(Path_To_Save_Res)
        
    Solution_Magnitude =  np.sqrt(np.square(Solution[:,:,0]) + np.square(Solution[:,:,1]))
    plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.sans-serif": ["Helvetica"],
    'font.size' : 14})
    
    plt.figure()
    ax = plt.gca()
    im2 = plt.imshow(np.flip(np.flip(Solution_Magnitude, axis = 1)), cmap='rainbow', extent=[0,2,0,2], aspect=1)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    plt.colorbar(im2, cax=cax, ticks = [0,0.5, 1])
    plt.clim(0,1)
    Path_File = Path_To_Save_Res + "/" + 'NS_Velocity_Magnitude_single_cavity_' + Name + '.pdf'
    plt.savefig(Path_File, format='pdf', bbox_inches='tight')  
    
    nx = n
    ny= n 
    x = np.linspace(0.0, Width, nx)
    y = np.linspace(0.0, Width, ny)
    X, Y = np.meshgrid(x, y)
    
    plt.figure()
    ax = plt.gca()
    plt.streamplot(x[:], y[:], Solution[:,:,0], Solution[:,:,1], color = Solution_Magnitude, cmap = 'rainbow', density=1.5)
    ax.set_aspect(aspect=1)
    plt.xlim(((0,Width)))
    plt.ylim(((0,Width))) 
    plt.clim(0,1)
    Path_File = Path_To_Save_Res + "/" + 'NS_Streamlines_single_cavity_' + Name + '.pdf'
    plt.savefig(Path_File, format='pdf', bbox_inches='tight') 
    
def Plot_Cavity_L(Solution, n = 129, Width = 2, Name = "Example"):
    
    Path = "../Genomes/" + Name + "/"
    Path_To_Save_Res = os.path.join(os.path.dirname(__file__), Path)
    if not os.path.exists(Path_To_Save_Res):
        os.makedirs(Path_To_Save_Res)
        
    Solution_Magnitude =  np.sqrt(np.square(Solution[:,:,0]) + np.square(Solution[:,:,1]))
    plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.sans-serif": ["Helvetica"],
    'font.size' : 14})
    
    plt.figure()
    ax = plt.gca()
    im2 = plt.imshow(np.flip(np.flip(Solution_Magnitude, axis = 1)), cmap='rainbow', extent=[0,2,0,2], aspect=1)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    plt.colorbar(im2, cax=cax, ticks = [0,0.5, 1])
    plt.clim(0,1)
    Path_File = Path_To_Save_Res + "/" + 'NS_Velocity_Magnitude_L_cavity_' + Name + '.pdf'
    plt.savefig(Path_File, format='pdf', bbox_inches='tight')  
    
    nx = n
    ny= n 
    x = np.linspace(0.0, Width, nx)
    y = np.linspace(0.0, Width, ny)
    X, Y = np.meshgrid(x, y)
    
    plt.figure()
    ax = plt.gca()
    plt.streamplot(x[:], y[:], Solution[:,:,0], Solution[:,:,1], color = Solution_Magnitude, cmap = 'rainbow', density=1.5)
    ax.set_aspect(aspect=1)
    plt.xlim(((0,Width)))
    plt.ylim(((0,Width))) 
    plt.clim(0,1)
    Path_File = Path_To_Save_Res + "/" + 'NS_Streamlines_L_cavity_' + Name + '.pdf'
    plt.savefig(Path_File, format='pdf', bbox_inches='tight') 