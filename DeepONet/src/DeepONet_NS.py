#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  7 14:22:14 2021

@author: robertplanas
"""
import tensorflow as tf
import math as math
from tensorflow import keras
import numpy as np
from matplotlib import pyplot as plt


Loss = tf.keras.metrics.Mean(name='loss')

# NS ---
Mae_u = tf.keras.metrics.MeanAbsoluteError(name='mae_u')
Mae_v = tf.keras.metrics.MeanAbsoluteError(name='mae_v')
Mae_p = tf.keras.metrics.MeanAbsoluteError(name='mae_p')

Mse_u = tf.keras.metrics.Mean(name='mse_u')
Mse_v = tf.keras.metrics.Mean(name='mse_v')
Mse_p = tf.keras.metrics.Mean(name='mse_p')

Pde_0 = tf.keras.metrics.Mean(name='pde_0')
Pde_1 = tf.keras.metrics.Mean(name='pde_1')
Pde_2 = tf.keras.metrics.Mean(name='pde_2')


# =============================================================================
# DeepONet, Unstacked, NS        
# =============================================================================
        

class DeepONet_Unstacked_NS(tf.keras.Model):
    
    def __init__(self, HL_B = 5, Units_B = 100, P = 100, 
                 HL_T = 5, Units_T = 100, 
                alpha_CM = 0, alpha_CMM = 0, Dimensions = 3, activation_B = 'tanh', activation_T = 'tanh', 
                reg = 0, nCell = 33, Batch = 128, Dat = 1024, nCol = 500,
                Gamma = 0.5, 
                data_type = 'float32', **kwargs ):
    
        super(DeepONet_Unstacked_NS, self).__init__(**kwargs)    
        
        # Structure
        self.nCell = nCell 
        self.HL_B = HL_B
        self.HL_T = HL_T
        self.Units_B = Units_B
        self.Units_T = Units_T
        self.act_B = activation_T
        self.act_T = activation_B
        self.P = P
        
        # Training parameters       
        self.Batch = 128
        self.Dat = Dat
        self.nCol = nCol
        
        # Navier stokes parameters
        self.alpha_CM = alpha_CM
        self.alpha_CMM = alpha_CMM
        self.Gamma = Gamma
        self.Dimensions = Dimensions
        
        if(data_type =='float32'):
            self.data_type = tf.float32
        elif(data_type =='float64'):
            self.data_type = tf.float64
        else:
            assert 1 == 0, "Invalid data type. Options: 'float32' or 'float64'"
            
        self.reg = tf.keras.regularizers.l2(reg)
        
        # Define the Branch Net
        self.mlp_B = []
        for H in range(self.HL_B):
            self.mlp_B.append(tf.keras.layers.Dense(self.Units_B, activation=self.act_B, 
                                                 kernel_regularizer=self.reg, 
                                                 bias_regularizer=self.reg,
                                                 name = "HL_B_" + str(H),
                                                 ))
        self.Layer_U = tf.keras.layers.Dense(self.P, activation='linear', 
                                             kernel_regularizer=self.reg, 
                                             bias_regularizer=self.reg,
                                             name = "HL_B_Final_U",
                                             )
        
        self.Layer_V = tf.keras.layers.Dense(self.P, activation='linear', 
                                             kernel_regularizer=self.reg, 
                                             bias_regularizer=self.reg,
                                             name = "HL_B_Final_V",
                                             )
        
        self.Layer_P = tf.keras.layers.Dense(self.P, activation='linear', 
                                     kernel_regularizer=self.reg, 
                                     bias_regularizer=self.reg,
                                     name = "HL_B_Final_P",
                                     )

        self.mlp_T = []
        for H in range(self.HL_T):
            self.mlp_T.append(tf.keras.layers.Dense(self.Units_T, activation=self.act_T, 
                                                 kernel_regularizer=self.reg, 
                                                 bias_regularizer=self.reg,
                                                 name = "HL_T_" + str(H),
                                                 ))
        self.mlp_T.append(tf.keras.layers.Dense(self.P, activation='linear', 
                                             kernel_regularizer=self.reg, 
                                             bias_regularizer=self.reg,
                                             name = "HL_T_Final",
                                             ))
        

    
    def call(self, Inputs):
        BC, xy, *_ = Inputs
        # Compute the BC
        bc = []
        for i in range(self.Dimensions):
            BC_aux= tf.concat([BC[:, 0:self.nCell -1, 0, i], BC[:, -1, 0:self.nCell -1, i], BC[:, 1:self.nCell, -1, i], BC[:,0, 1:self.nCell, i ]],axis = -1  )
            if(i == 0):
                bc = BC_aux
            else:
                bc = tf.concat([bc,BC_aux], axis = -1)
        for layer in self.mlp_B:
            bc = layer(bc)
        
        U = self.Layer_U(bc)
        V = self.Layer_V(bc)
        P = self.Layer_P(bc)
        
        for layer in self.mlp_T:
            xy = layer(xy)
            
        Dat_Points = xy.shape[0]
        BC_Cond = bc.shape[0]
        
        U = tf.repeat(U, repeats = Dat_Points, axis = 0 )
        V = tf.repeat(V, repeats = Dat_Points, axis = 0 )
        P = tf.repeat(P, repeats = Dat_Points, axis = 0 )
        
        xy = tf.tile(xy, multiples = [BC_Cond, 1])
        Out_U = tf.reduce_sum(tf.multiply(U, xy), axis = -1)
        Out_V = tf.reduce_sum(tf.multiply(V, xy), axis = -1)
        Out_P = tf.reduce_sum(tf.multiply(P, xy), axis = -1)
        
        Out_U_reshape = tf.reshape(Out_U, (BC_Cond, Dat_Points,1))
        Out_V_reshape = tf.reshape(Out_V, (BC_Cond, Dat_Points,1))
        Out_P_reshape = tf.reshape(Out_P, (BC_Cond, Dat_Points,1))
        
        Out_reshape = tf.concat([Out_U_reshape, Out_V_reshape, Out_P_reshape], axis = -1)
        return Out_reshape
    
    def Predict_Shape(self, Inputs):
        BC, xy, *_ = Inputs
        # Compute the BC
        bc = []
        for i in range(self.Dimensions):
            BC_aux= tf.concat([BC[:, 0:self.nCell -1, 0, i], BC[:, -1, 0:self.nCell -1, i], BC[:, 1:self.nCell, -1, i], BC[:,0, 1:self.nCell, i ]],axis = -1  )
            if(i == 0):
                bc = BC_aux
            else:
                bc = tf.concat([bc,BC_aux], axis = -1)
        
        print("BC Input = ", bc.shape)
        for layer in self.mlp_B:
            bc = layer(bc)
        print("bc mlp_B = ",bc.shape )
        U = self.Layer_U(bc)
        V = self.Layer_V(bc)
        P = self.Layer_P(bc)
        print("U = ", U.shape, " V = ", V.shape, " P = ", P.shape)
        
        print("xy = ", xy.shape)
        for layer in self.mlp_T:
            xy = layer(xy)
        print("xy mlp = ", xy.shape)
            
        Dat_Points = xy.shape[0]
        BC_Cond = bc.shape[0]
        
        U = tf.repeat(U, repeats = Dat_Points, axis = 0 )
        V = tf.repeat(V, repeats = Dat_Points, axis = 0 )
        P = tf.repeat(P, repeats = Dat_Points, axis = 0 )
        
        xy = tf.tile(xy, multiples = [BC_Cond, 1])
        Out_U = tf.reduce_sum(tf.multiply(U, xy), axis = -1)
        Out_V = tf.reduce_sum(tf.multiply(V, xy), axis = -1)
        Out_P = tf.reduce_sum(tf.multiply(P, xy), axis = -1)
        
        print("Out_U = ", Out_U.shape, " Out_V = ", Out_V.shape, " Out_P = ", Out_P.shape)
        
        Out_U_reshape = tf.reshape(Out_U, (BC_Cond, Dat_Points,1))
        Out_V_reshape = tf.reshape(Out_V, (BC_Cond, Dat_Points,1))
        Out_P_reshape = tf.reshape(Out_P, (BC_Cond, Dat_Points,1))
        
        Out_reshape = tf.concat([Out_U_reshape, Out_V_reshape, Out_P_reshape], axis = -1)
        print("Out_reshape = ", Out_reshape.shape)
        return Out_reshape
    
    def Predict_mlp_B(self, BC):
        # Compute the BC
        bc = []
        for i in range(self.Dimensions):
            BC_aux= tf.concat([BC[:, 0:self.nCell -1, 0, i], BC[:, -1, 0:self.nCell -1, i], BC[:, 1:self.nCell, -1, i], BC[:,0, 1:self.nCell, i ]],axis = -1  )
            if(i == 0):
                bc = BC_aux
            else:
                bc = tf.concat([bc,BC_aux], axis = -1)
        for layer in self.mlp_B:
            bc = layer(bc)
        
        U = self.Layer_U(bc)
        V = self.Layer_V(bc)
        P = self.Layer_P(bc)
        
        return U,V,P
        
    
    def Predict_mlp_T(self, xy_p):
        a = xy_p
        for layer in self.mlp_T:
            a = layer(a)
        #print(xy_p.shape)
        return a
    
    def train_step(self, data):
        Input, Output = data
        BC, xy_dat, xy_col = Input
        UVP_dat = Output
        with tf.GradientTape(persistent=False) as tape0:
            BU_Pred, BV_Pred, BP_Pred = self.Predict_mlp_B(BC)
            if(self.alpha_CM*self.alpha_CMM != 0):
                xy_col = tf.tile(xy_col, multiples = [self.Batch, 1])
                BU_Pred, BV_Pred, BP_Pred = self.Predict_mlp_B(BC)
                BU_Pred_col= tf.repeat(BU_Pred, repeats = self.nCol, axis = 0 ) 
                BV_Pred_col= tf.repeat(BV_Pred, repeats = self.nCol, axis = 0 ) 
                BP_Pred_col= tf.repeat(BP_Pred, repeats = self.nCol, axis = 0 ) 
                with tf.GradientTape(persistent=True, watch_accessed_variables=False) as tape2:
                    tape2.watch(xy_col)
                    with tf.GradientTape(persistent=True,  watch_accessed_variables=False ) as tape1:
                        tape1.watch(xy_col)                        
                        T_col_Pred = self.Predict_mlp_T(xy_col)
                        U_col = tf.reduce_sum(tf.multiply(BU_Pred_col, T_col_Pred), axis = -1)
                        V_col = tf.reduce_sum(tf.multiply(BV_Pred_col, T_col_Pred), axis = -1)
                        P_col = tf.reduce_sum(tf.multiply(BP_Pred_col, T_col_Pred), axis = -1)
                    
                    U_col_grad = tape1.gradient(U_col, xy_col)
                    V_col_grad = tape1.gradient(V_col, xy_col)
                    P_col_grad = tape1.gradient(P_col, xy_col)
                    U_col_grad_x = U_col_grad[:,0]
                    U_col_grad_y = U_col_grad[:,1]
                    V_col_grad_x = V_col_grad[:,0]
                    V_col_grad_y = V_col_grad[:,1]
                    P_col_grad_x = P_col_grad[:,0]
                    P_col_grad_y = P_col_grad[:,1]
                    del tape1
                    
                U_col_grad_xx = tape2.gradient(U_col_grad_x, xy_col)[:,0]
                U_col_grad_yy = tape2.gradient(U_col_grad_y, xy_col)[:,1]
                V_col_grad_xx = tape2.gradient(V_col_grad_x, xy_col)[:,0]
                V_col_grad_yy = tape2.gradient(V_col_grad_y, xy_col)[:,1]
                
                
                pde0 = tf.reduce_mean(tf.square(U_col_grad_x + V_col_grad_y))
                pde1 = tf.reduce_mean(tf.square(U_col*U_col_grad_x + V_col*U_col_grad_y + P_col_grad_x - (U_col_grad_xx + U_col_grad_yy)/500.0))
                pde2 = tf.reduce_mean(tf.square(U_col*V_col_grad_x + V_col*V_col_grad_y + P_col_grad_y - (V_col_grad_xx + V_col_grad_yy)/500.0))

            else:
                pde0 = 0
                pde1 = 0
                pde2 = 0
                
            BU_Pred_dat = tf.repeat(BU_Pred, repeats = self.Dat, axis = 0 ) 
            BV_Pred_dat = tf.repeat(BV_Pred, repeats = self.Dat, axis = 0 )
            BP_Pred_dat = tf.repeat(BP_Pred, repeats = self.Dat, axis = 0 )                  
            T_dat_Pred = self.Predict_mlp_T(xy_dat)
            
            T_dat_Pred_B = tf.tile(T_dat_Pred, multiples = [self.Batch, 1])
            
            U_dat_Pred = tf.reduce_sum(tf.multiply(BU_Pred_dat, T_dat_Pred_B), axis = -1)
            U_dat_Pred = tf.reshape(U_dat_Pred, (self.Batch, self.Dat))
            
            V_dat_Pred = tf.reduce_sum(tf.multiply(BV_Pred_dat, T_dat_Pred_B), axis = -1)
            V_dat_Pred = tf.reshape(V_dat_Pred, (self.Batch, self.Dat))
            
            P_dat_Pred = tf.reduce_sum(tf.multiply(BP_Pred_dat, T_dat_Pred_B), axis = -1)
            P_dat_Pred = tf.reshape(P_dat_Pred, (self.Batch, self.Dat))
            
            MSE_u = tf.reduce_mean(tf.square(U_dat_Pred - UVP_dat[:,:,0]))
            MSE_v = tf.reduce_mean(tf.square(V_dat_Pred - UVP_dat[:,:,1]))
            MSE_p = tf.reduce_mean(tf.square(P_dat_Pred - UVP_dat[:,:,2]))
            
            
            loss = MSE_u + MSE_v + self.Gamma*MSE_p + self.alpha_CM*pde0 + self.alpha_CMM*(pde1 + pde2) + tf.reduce_sum(self.losses)
            
        lossGrad = tape0.gradient(loss, self.trainable_variables)
        del tape0
        self.optimizer.apply_gradients(zip(lossGrad, self.trainable_variables))
        
        # update loss and mae, tf internally handles distribution
        Loss.update_state(loss) 
        Mae_u.update_state(U_dat_Pred, UVP_dat[:,:,0])
        Mae_v.update_state(V_dat_Pred, UVP_dat[:,:,1])
        Mae_p.update_state(P_dat_Pred, UVP_dat[:,:,2])
        
        Mse_u.update_state(MSE_u)
        Mse_v.update_state(MSE_v)
        Mse_p.update_state(MSE_p)
        
        Pde_0.update_state(pde0)
        Pde_1.update_state(pde1)
        Pde_2.update_state(pde2)
        
        return {'loss':Loss.result(), 'mae_u':Mae_u.result(), 'mae_v':Mae_v.result(), 'mae_p':Mae_p.result(),
                'mse_u':Mse_u.result(), 'mse_v':Mse_v.result(), 'mse_p':Mse_p.result(),
                'pde_0':Pde_0.result(), 'pde_1':Pde_1.result(), 'pde_2':Pde_2.result()}
    
    def test_step(self, data):
        Input, Output = data
        BC, xy_dat, xy_col = Input
        UVP_dat = Output
        BU_Pred, BV_Pred, BP_Pred = self.Predict_mlp_B(BC)
        if(self.alpha_CM*self.alpha_CMM != 0):
            xy_col = tf.tile(xy_col, multiples = [self.Batch, 1])
            BU_Pred, BV_Pred, BP_Pred = self.Predict_mlp_B(BC)
            BU_Pred_col= tf.repeat(BU_Pred, repeats = self.nCol, axis = 0 ) 
            BV_Pred_col= tf.repeat(BV_Pred, repeats = self.nCol, axis = 0 ) 
            BP_Pred_col= tf.repeat(BP_Pred, repeats = self.nCol, axis = 0 ) 
            with tf.GradientTape(persistent=True, watch_accessed_variables=False) as tape2:
                tape2.watch(xy_col)
                with tf.GradientTape(persistent=True,  watch_accessed_variables=False ) as tape1:
                    tape1.watch(xy_col)                        
                    T_col_Pred = self.Predict_mlp_T(xy_col)
                    U_col = tf.reduce_sum(tf.multiply(BU_Pred_col, T_col_Pred), axis = -1)
                    V_col = tf.reduce_sum(tf.multiply(BV_Pred_col, T_col_Pred), axis = -1)
                    P_col = tf.reduce_sum(tf.multiply(BP_Pred_col, T_col_Pred), axis = -1)
                
                U_col_grad = tape1.gradient(U_col, xy_col)
                V_col_grad = tape1.gradient(V_col, xy_col)
                P_col_grad = tape1.gradient(P_col, xy_col)
                U_col_grad_x = U_col_grad[:,0]
                U_col_grad_y = U_col_grad[:,1]
                V_col_grad_x = V_col_grad[:,0]
                V_col_grad_y = V_col_grad[:,1]
                P_col_grad_x = P_col_grad[:,0]
                P_col_grad_y = P_col_grad[:,1]
                del tape1
                
            U_col_grad_xx = tape2.gradient(U_col_grad_x, xy_col)[:,0]
            U_col_grad_yy = tape2.gradient(U_col_grad_y, xy_col)[:,1]
            V_col_grad_xx = tape2.gradient(V_col_grad_x, xy_col)[:,0]
            V_col_grad_yy = tape2.gradient(V_col_grad_y, xy_col)[:,1]
            
            
            pde0 = tf.reduce_mean(tf.square(U_col_grad_x + V_col_grad_y))
            pde1 = tf.reduce_mean(tf.square(U_col*U_col_grad_x + V_col*U_col_grad_y + P_col_grad_x - (U_col_grad_xx + U_col_grad_yy)/500.0))
            pde2 = tf.reduce_mean(tf.square(U_col*V_col_grad_x + V_col*V_col_grad_y + P_col_grad_y - (V_col_grad_xx + V_col_grad_yy)/500.0))
        else:
            pde0 = 0
            pde1 = 0
            pde2 = 0
            
        BU_Pred_dat = tf.repeat(BU_Pred, repeats = self.Dat, axis = 0 ) 
        BV_Pred_dat = tf.repeat(BV_Pred, repeats = self.Dat, axis = 0 )
        BP_Pred_dat = tf.repeat(BP_Pred, repeats = self.Dat, axis = 0 )                  
        T_dat_Pred = self.Predict_mlp_T(xy_dat)
        
        T_dat_Pred_B = tf.tile(T_dat_Pred, multiples = [self.Batch, 1])
        
        U_dat_Pred = tf.reduce_sum(tf.multiply(BU_Pred_dat, T_dat_Pred_B), axis = -1)
        U_dat_Pred = tf.reshape(U_dat_Pred, (self.Batch, self.Dat))
        
        V_dat_Pred = tf.reduce_sum(tf.multiply(BV_Pred_dat, T_dat_Pred_B), axis = -1)
        V_dat_Pred = tf.reshape(V_dat_Pred, (self.Batch, self.Dat))
        
        P_dat_Pred = tf.reduce_sum(tf.multiply(BP_Pred_dat, T_dat_Pred_B), axis = -1)
        P_dat_Pred = tf.reshape(P_dat_Pred, (self.Batch, self.Dat))
        
        MSE_u = tf.reduce_mean(tf.square(U_dat_Pred - UVP_dat[:,:,0]))
        MSE_v = tf.reduce_mean(tf.square(V_dat_Pred - UVP_dat[:,:,1]))
        MSE_p = tf.reduce_mean(tf.square(P_dat_Pred - UVP_dat[:,:,2]))
        
        
        loss = MSE_u + MSE_v + self.Gamma*MSE_p + self.alpha_CM*pde0 + self.alpha_CMM*(pde1 + pde2) + tf.reduce_sum(self.losses)
        
        # update loss and mae, tf internally handles distribution
        Loss.update_state(loss) 
        Mae_u.update_state(U_dat_Pred, UVP_dat[:,:,0])
        Mae_v.update_state(V_dat_Pred, UVP_dat[:,:,1])
        Mae_p.update_state(P_dat_Pred, UVP_dat[:,:,2])
        
        Mse_u.update_state(MSE_u)
        Mse_v.update_state(MSE_v)
        Mse_p.update_state(MSE_p)
        
        Pde_0.update_state(pde0)
        Pde_1.update_state(pde1)
        Pde_2.update_state(pde2)
        
        return {'loss':Loss.result(), 'mae_u':Mae_u.result(), 'mae_v':Mae_v.result(), 'mae_p':Mae_p.result(),
                'mse_u':Mse_u.result(), 'mse_v':Mse_v.result(), 'mse_p':Mse_p.result(),
                'pde_0':Pde_0.result(), 'pde_1':Pde_1.result(), 'pde_2':Pde_2.result()}
    
    @property
    def metrics(self):
        return [Loss,Mae_u,Mae_v,Mae_p,Mse_u,Mse_v,Mse_p, Pde_0, Pde_1, Pde_2]

        
        
class DeepONet_Stacked_NS(DeepONet_Unstacked_NS):        
    def __init__(self, HL_B = 5, Units_B = 100, P = 100, 
                 HL_T = 5, Units_T = 100, 
                alpha_CM = 0, alpha_CMM = 0, Dimensions = 3, activation_B = 'tanh', activation_T = 'tanh', 
                reg = 0, nCell = 33, Batch = 128, Dat = 1024, nCol = 500,
                Gamma = 0.5, 
                data_type = 'float32', **kwargs ):
        
        super(DeepONet_Stacked_NS, self).__init__(**kwargs)    
        # Structure
        self.nCell = nCell 
        self.HL_B = HL_B
        self.HL_T = HL_T
        self.Units_B = Units_B
        self.Units_T = Units_T
        self.act_B = activation_T
        self.act_T = activation_B
        self.P = P
        
        # Training parameters       
        self.Batch = 128
        self.Dat = Dat
        self.nCol = nCol
        
        # Navier stokes parameters
        self.alpha_CM = alpha_CM
        self.alpha_CMM = alpha_CMM
        self.Gamma = Gamma
        self.Dimensions = Dimensions

        if(data_type =='float32'):
            self.data_type = tf.float32
        elif(data_type =='float64'):
            self.data_type = tf.float64
        else:
            assert 1 == 0, "Invalid data type. Options: 'float32' or 'float64'"
            
        self.reg = tf.keras.regularizers.l2(reg)
        
        # Define the Branch Nets Stacked
        self.BN = []
        for i in range(3*self.P):
            mlp_B = []
            for H in range(self.HL_B):
                mlp_B.append(tf.keras.layers.Dense(self.Units_B, activation=self.act_B, 
                                                         kernel_regularizer=self.reg, 
                                                         bias_regularizer=self.reg,
                                                         name = "HL_B_" + str(H) + "_" + str(i),
                                                         ))
            mlp_B.append(tf.keras.layers.Dense(1, activation='linear', 
                                                     kernel_regularizer=self.reg, 
                                                     bias_regularizer=self.reg,
                                                     name = "HL_B_Final" + "_" + str(i),
                                                     ))
            self.BN.append(mlp_B)
        
        self.Layer_U = tf.keras.layers.Dense(self.P, activation='linear', 
                                             kernel_regularizer=self.reg, 
                                             bias_regularizer=self.reg,
                                             name = "HL_B_Final_U",
                                             )
        
        self.Layer_V = tf.keras.layers.Dense(self.P, activation='linear', 
                                             kernel_regularizer=self.reg, 
                                             bias_regularizer=self.reg,
                                             name = "HL_B_Final_V",
                                             )
        
        self.Layer_P = tf.keras.layers.Dense(self.P, activation='linear', 
                                     kernel_regularizer=self.reg, 
                                     bias_regularizer=self.reg,
                                     name = "HL_B_Final_P",
                                     )
        
        # Define the Trunk Net
        self.mlp_T = []
        for H in range(self.HL_T):
            self.mlp_T.append(tf.keras.layers.Dense(self.Units_T, activation=self.act_T, 
                                                 kernel_regularizer=self.reg, 
                                                 bias_regularizer=self.reg,
                                                 name = "HL_T_" + str(H),
                                                 ))
        self.mlp_T.append(tf.keras.layers.Dense(self.P, activation='linear', 
                                             kernel_regularizer=self.reg, 
                                             bias_regularizer=self.reg,
                                             name = "HL_T_Final",
                                             ))
        
    def call(self, Inputs):
        BC, xy, *_ = Inputs
        # Compute the BC
        bc = []
        for i in range(self.Dimensions):
            BC_aux= tf.concat([BC[:, 0:self.nCell -1, 0, i], BC[:, -1, 0:self.nCell -1, i], BC[:, 1:self.nCell, -1, i], BC[:,0, 1:self.nCell, i ]],axis = -1  )
            if(i == 0):
                bc = BC_aux
            else:
                bc = tf.concat([bc,BC_aux], axis = -1)
                
                
        BN = []
        for index_n, net in enumerate(self.BN):
            BC_i = bc
            for layer in net:
                BC_i = layer(BC_i)
            if(index_n == 0):
                BN = BC_i
            else: 
                BN = tf.concat([BN,BC_i], axis = -1)
                
        
        U = self.Layer_U(BN)
        V = self.Layer_V(BN)
        P = self.Layer_P(BN)
        
        for layer in self.mlp_T:
            xy = layer(xy)
            
        Dat_Points = xy.shape[0]
        BC_Cond = bc.shape[0]
        
        U = tf.repeat(U, repeats = Dat_Points, axis = 0 )
        V = tf.repeat(V, repeats = Dat_Points, axis = 0 )
        P = tf.repeat(P, repeats = Dat_Points, axis = 0 )
        
        xy = tf.tile(xy, multiples = [BC_Cond, 1])
        Out_U = tf.reduce_sum(tf.multiply(U, xy), axis = -1)
        Out_V = tf.reduce_sum(tf.multiply(V, xy), axis = -1)
        Out_P = tf.reduce_sum(tf.multiply(P, xy), axis = -1)
        
        Out_U_reshape = tf.reshape(Out_U, (BC_Cond, Dat_Points,1))
        Out_V_reshape = tf.reshape(Out_V, (BC_Cond, Dat_Points,1))
        Out_P_reshape = tf.reshape(Out_P, (BC_Cond, Dat_Points,1))
        
        Out_reshape = tf.concat([Out_U_reshape, Out_V_reshape, Out_P_reshape], axis = -1)
        return Out_reshape
    
    def Predict_Shape(self, Inputs):
        BC, xy, *_ = Inputs
        # Compute the BC
        bc = []
        for i in range(self.Dimensions):
            BC_aux= tf.concat([BC[:, 0:self.nCell -1, 0, i], BC[:, -1, 0:self.nCell -1, i], BC[:, 1:self.nCell, -1, i], BC[:,0, 1:self.nCell, i ]],axis = -1  )
            if(i == 0):
                bc = BC_aux
            else:
                bc = tf.concat([bc,BC_aux], axis = -1)
        
        print("BC Input = ", bc.shape)
                
        BN = []
        for index_n, net in enumerate(self.BN):
            BC_i = bc
            for layer in net:
                BC_i = layer(BC_i)
            if(index_n == 0):
                BN = BC_i
            else: 
                BN = tf.concat([BN,BC_i], axis = -1)
                
        print("bc BN = ", BN.shape )
        U = self.Layer_U(BN)
        V = self.Layer_V(BN)
        P = self.Layer_P(BN)
        
        print("U = ", U.shape, " V = ", V.shape, " P = ", P.shape)
        
        print("xy = ", xy.shape)
        for layer in self.mlp_T:
            xy = layer(xy)
        print("xy mlp = ", xy.shape)
            
        Dat_Points = xy.shape[0]
        BC_Cond = bc.shape[0]
        
        U = tf.repeat(U, repeats = Dat_Points, axis = 0 )
        V = tf.repeat(V, repeats = Dat_Points, axis = 0 )
        P = tf.repeat(P, repeats = Dat_Points, axis = 0 )
        
        xy = tf.tile(xy, multiples = [BC_Cond, 1])
        Out_U = tf.reduce_sum(tf.multiply(U, xy), axis = -1)
        Out_V = tf.reduce_sum(tf.multiply(V, xy), axis = -1)
        Out_P = tf.reduce_sum(tf.multiply(P, xy), axis = -1)
        
        print("Out_U = ", Out_U.shape, " Out_V = ", Out_V.shape, " Out_P = ", Out_P.shape)
        
        Out_U_reshape = tf.reshape(Out_U, (BC_Cond, Dat_Points,1))
        Out_V_reshape = tf.reshape(Out_V, (BC_Cond, Dat_Points,1))
        Out_P_reshape = tf.reshape(Out_P, (BC_Cond, Dat_Points,1))
        
        Out_reshape = tf.concat([Out_U_reshape, Out_V_reshape, Out_P_reshape], axis = -1)
        print("Out_reshape = ", Out_reshape.shape)
        return Out_reshape
    
    def Predict_mlp_B(self, BC):
        # Compute the BC
        bc = []
        for i in range(self.Dimensions):
            BC_aux= tf.concat([BC[:, 0:self.nCell -1, 0, i], BC[:, -1, 0:self.nCell -1, i], BC[:, 1:self.nCell, -1, i], BC[:,0, 1:self.nCell, i ]],axis = -1  )
            if(i == 0):
                bc = BC_aux
            else:
                bc = tf.concat([bc,BC_aux], axis = -1)
                
        BN = []
        for index_n, net in enumerate(self.BN):
            BC_i = bc
            for layer in net:
                BC_i = layer(BC_i)
            if(index_n == 0):
                BN = BC_i
            else: 
                BN = tf.concat([BN,BC_i], axis = -1)
        
        U = self.Layer_U(BN)
        V = self.Layer_V(BN)
        P = self.Layer_P(BN)
        
        return U,V,P
        
        
        