# -*- coding: utf-8 -*-


import numpy as np
import pyamg as amg
import h5py as h5
import matplotlib.pyplot as plt

# Select witdth for square domains 
All_Widths = [1,2,3,4,5,6,8]

# Select BC
# "1": Simple boundary conditions
# "2": Complex boundary conditinos
Select_BC = "1"

# Check: Bool to check correctness of the solutino
Check = True

for Width in All_Widths:
    
    # num samples
    nSample = 1
    # num grid cells along an edge
    nCell = 31
    # file name for the bc data
    FileName = "Sin_BC_" + Select_BC + "_AMG_" + str(Width) + "x" + str(Width)
    
    # space step, domain length is 1
    h = 1.0 / nCell
    
    # matrix
    A = amg.gallery.poisson((nCell*Width - 1, nCell*Width -1), format='csr')
        
    # create datasets to contain solutons
    solFile = h5.File(FileName + '.h5', 'w')
    solFile.attrs['nSample']    = nSample
    solFile.attrs['domainSize'] = np.array([nCell*Width, nCell*Width])
    solData = solFile.create_dataset('LaplaceSolution', (nSample, Width*nCell+1, Width*nCell+1), \
                                      compression='gzip', compression_opts=9, \
                                      dtype='float64', chunks=True)
    
    # Define the grid for the solution
    p = np.zeros((1, Width*nCell+1, Width*nCell+1))
    
    #Define the boundary conditions
    y = np.linspace(0, 4*Width-h, 4*Width*nCell)
    if(Select_BC == "1"):
        BC = np.sin(2*np.pi*y/Width)
    elif(Select_BC == "2"):
        BC = np.sin(2*np.pi*y)
        
    # Define the RHS    
    RHS = np.zeros((nCell*Width-1, nCell*Width-1))
    RHS[0,:] += BC[1:nCell*Width]
    RHS[:,-1] += BC[nCell*Width +1 :2*nCell*Width ]
    RHS[-1,:] += np.flip( BC[2*nCell*Width+1 :3*nCell*Width ])
    RHS[:,0] += np.flip(BC[3*nCell*Width +1:4*nCell*Width])
    
    # transform rhs to a vector
    RHS = np.reshape(RHS, ((Width*nCell-1)*(Width*nCell-1)), order='C')
            
    print("Solving....")
    x = amg.solve(A, RHS, verb=False, tol=1e-14)
    
    #Save the values
    p[0,1:-1,1:-1] = np.reshape(x, (1, Width*nCell-1, Width*nCell-1), order='C') 
    p[0,0,0:-1] = BC[:nCell*Width]
    p[0,0:-1,-1] = BC[nCell*Width:2*nCell*Width]
    p[0,-1,1:] = np.flip( BC[2*nCell*Width:3*nCell*Width])
    p[0,1:,0] = np.flip(BC[3*nCell*Width:4*nCell*Width])
    
    
    
    #Check the residual and the accuracy of the BC
    if(Check):
        #Check RHS with discrete difference

        RHS = np.zeros((Width*nCell-1,Width*nCell-1))
        for k in range(Width*nCell -1 ):
            for l in range(Width*nCell -1 ):
                RHS[k,l] = (4*p[0,k+1,l+1] - p[0,k+1, l] - p[0,k,l+1] - p[0,k+2, l+1] - p[0,k+1, l+2])/(h**2)
        
        max_RHS = np.max(np.abs(RHS))
        print('Max Absolute Error RHS ',max_RHS )
    
    solData[0,:,:] = p
    solFile.close()
                   





