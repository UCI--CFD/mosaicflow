#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 15:54:55 2021

@author: robertplanas
"""

import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../res/"))
import argparse
import h5py as h5
import numpy as np

parser = argparse.ArgumentParser()

# Model Parameters Branch
parser.add_argument('-HL_B', '--HL_B', type = int, default = 3,
                    help = 'Hidden Layers for the brach model')
parser.add_argument('-Units_B', '--Units_B', type = int, default = 20,
                    help = 'Units for the brach model')
parser.add_argument('-act_B', '--activation_B', default = 'swish', 
                    help = 'Activation function for Branch Model')

# Model Parameters Trunk
parser.add_argument('-HL_T', '--HL_T', type = int, default = 5,
                    help = 'Hidden Layers for the trunk model')
parser.add_argument('-Units_T', '--Units_T', type = int, default = 100,
                    help = 'Units for the trunk model')
parser.add_argument('-act_T', '--activation_T', default = 'swish', 
                    help = 'Activation function for trunk Model')

# Model Parameters P
parser.add_argument('-P', '--P', default = 100, type = int, 
                    help = 'Output shape from the nets')

# Alpha for residual
parser.add_argument('-alpha_CM', '--alpha_CM', default = 0, type = float, 
                 help = 'Alpha parameter for the CM equation. Set to 0 if do not want to use')
parser.add_argument('-alpha_CMM', '--alpha_CMM', default = 0, type = float, 
                 help = 'Alpha parameter for the CMM equation. Set to 0 if do not want to use')

# Regularization
parser.add_argument('-reg', '--reg', default = 0, type = float, 
                 help = 'L2 regularization')
#Data
parser.add_argument('-f', '--file', default='../../CNN/data/genomes_merged.h5',
                    help='data file')
parser.add_argument('-f2', '--file2', default='../../CNN/data/genomes_channel.h5',
                    help='data file')

# Use_Extra_Data
parser.add_argument('-Use_SC', '--Use_SC', default = False, action = 'store_true',
                    help='Use Extra Single Cavity Data')
parser.add_argument('-Use_LC', '--Use_LC', default = False, action = 'store_true',
                    help='Use Extra L Cavity Data')
parser.add_argument('-Use_CH', '--Use_CH', default = False, action = 'store_true',
                    help='Use Extra Channel Cavity Data')


#Traing Parameters
parser.add_argument('-TI', '--Training_Iterations', type=int, default=25,
                    help='Number of Optimizations')
parser.add_argument('-e', '--nEpoch', type=int, default=120,
                    help='initial train epochs')
parser.add_argument('-B', '--Batch', type=int, default=128,
                    help='Batch Size')

# learning rate adjustment
parser.add_argument('-f_lr', '--factor_lr', type=float, default = 0.9,
                    help = 'Reducing the learning rate')
parser.add_argument('-lr0', '--lr0', type=float,
                    default=1e-3, help='init leanring rate')


#Restart 
parser.add_argument('-restart', '--restart', default=False, action='store_true',
                    help='restart from checkpoint')
parser.add_argument('-ckpnt', '--checkpoint', default=None,
                    help='Checkpoint name to restart the training from')



# Choose the data type
parser.add_argument('-dt', '--DataType', default='float32',
                    help='DataType')
parser.add_argument('-fV', '--Factor_Validation', type=int, default=0.1,
                    help='number of validation solutions ')
parser.add_argument('-fT', '--Factor_Test', type=int, default=0.1,
                    help='number of test solutions ')

# Choose nCol and nDat
parser.add_argument('-nCol', '--nCol', default=100, type = int,
                    help='Number of collocation points')




args = parser.parse_args()

import os
import tensorflow as tf
from tensorflow import keras
import DeepONet_NS as DON
import utils as utils
import NS_dataset as NS_D 

#%% =============================================================================
#  Define the name of the model 
# =============================================================================


act_B = "_actB-" + str(args.activation_B)
HL_B = "_HLB-" + str(args.HL_B) 
Units_B = "_UB-" + str(args.Units_B)

act_T = "_actT-" + str(args.activation_T)
HL_T = "_HLT-" + str(args.HL_T) 
Units_T = "_UT-" + str(args.Units_T)

P_para = "_P-" + str(args.P)

Reg = "_reg-" + f"{args.reg:.0e}"
CM = "_aCM-" + f"{args.alpha_CM:.0e}"
CMM = "_aCMM-" + f"{args.alpha_CMM:.0e}"

Fac = "_flr-" + str(args.factor_lr)
Patience = "_Pat-" + str(args.nEpoch)
Iterations = "_Iter-" + str(args.Training_Iterations)


modelName = "DON_NS_S" + act_B + HL_B + Units_B + act_T + HL_T + Units_T + P_para + Reg  + CM + CMM + Fac + Patience + Iterations

print(f"Model Name : {modelName}")

#%% =============================================================================
# Load the data  
# =============================================================================

# AutoEncoder File
Data_File = os.path.join(os.path.dirname(__file__), args.file)

# load data
dataSet = NS_D.NSDataSet()
dataSet.add_file(Data_File)
dataSet.load_data()
dataSet.extract_genome_bc()
dataSet.summary()

C_Samples = dataSet.var.shape[0]

Input_Variables = 3
Output_Variables = 3
nCell = 33

DATA = dataSet.var[:,:,:,:]

if(args.Use_SC):
    Name_Data = "../../CNN/data_NS/Extra_Single_Cavity_9_GxF-100.h5"
    Name_Data = os.path.join(os.path.dirname(__file__), Name_Data)
    
    
    # load hdf5 data and test residual of Poisson (psn)
    NSFile = h5.File(Name_Data, 'r')
    nSample = NSFile.attrs['nSample']
    nCell = NSFile.attrs['domainSize'][0] 
    NS_Data = NSFile['NS_Solution']
    print('# Samples: {}'.format(nSample))
    print('# Cells: {}*{}*{}'.format(nCell, nCell, 3))
    print('data shape: ', end='')
    print(NS_Data.shape)
    
    # read the data into numpy array
    DATA_SC = np.zeros(NS_Data.shape)
    DATA_SC = NS_Data[:,:, :, :]
    
    DATA = np.concatenate((DATA, DATA_SC))

SC_Samples = len(DATA) - C_Samples 


if(args.Use_LC):
    Name_Data = "../../CNN/data_NS/Extra_L_Cavity_9_GxF-200.h5"
    Name_Data = os.path.join(os.path.dirname(__file__), Name_Data)
    
    
    # load hdf5 data and test residual of Poisson (psn)
    NSFile = h5.File(Name_Data, 'r')
    nSample = NSFile.attrs['nSample']
    nCell = NSFile.attrs['domainSize'][0] 
    NS_Data = NSFile['NS_Solution']
    print('# Samples: {}'.format(nSample))
    print('# Cells: {}*{}*{}'.format(nCell, nCell, 3))
    print('data shape: ', end='')
    print(NS_Data.shape)
    
    # read the data into numpy array
    DATA_LC = np.zeros(NS_Data.shape)
    DATA_LC = NS_Data[:,:, :, :]
    
    DATA = np.concatenate((DATA, DATA_LC))

LC_Samples = len(DATA) - SC_Samples - C_Samples

if(args.Use_CH):
    
        
    # File for the first data set
    Data_File_2 = os.path.join(os.path.dirname(__file__), args.file2)
        
    # load data
    dataSet2 = NS_D.NSDataSet()
    dataSet2.add_file(Data_File_2)
    dataSet2.load_data()
    dataSet2.extract_genome_bc()
    dataSet2.summary()
    
    DATA_CH = dataSet2.var[:,:,:,:]
    DATA = np.concatenate((DATA, DATA_CH))
    
CH_Samples = len(DATA) - SC_Samples - C_Samples - LC_Samples

print('--------------------------------')
print('generator:')
print('{} Basic Samples'.format(C_Samples))
print('{} SC Samples'.format(SC_Samples))
print('{} LC Samples'.format(LC_Samples))
print('{} CH Samples'.format(CH_Samples))
print('--------------------------------')   

DATA = DATA[:,:,:,:Input_Variables]
#np.random.shuffle(DATA)
Total_Samples = len(DATA)
nValid = int(args.Factor_Validation*Total_Samples)
nTest = int(args.Factor_Test*Total_Samples)
nTrain = Total_Samples - nTest - nValid 

#%% =============================================================================
# Create the inputs
# =============================================================================

# Define the outputs

Y_Train = DATA[:nTrain ,:,:,:Output_Variables]
Y_Val = DATA[nTrain:nTrain + nValid,:,:, :Output_Variables]
Y_Test = DATA[nTrain + nValid:,:,:, :Output_Variables] 

X = np.zeros_like(DATA[:,:,:,:Input_Variables])
X[:,0,:,:] = DATA[:,0,:,:Input_Variables]
X[:,-1,:,:] = DATA[:,-1,:,:Input_Variables]
X[:,:,0,:] = DATA[:,:,0,:Input_Variables]
X[:,:,-1,:] = DATA[:,:,-1,:Input_Variables]

X_Train = X[:nTrain, :, :,:]
X_Val = X[nTrain:nTrain + nValid,:,:,:]
X_Test = X[nTrain + nValid:nTrain + nValid +nTest, : ,:,:]

X_points = np.linspace(0,0.5, num = nCell)[None, :]
Y_points = np.linspace(0,0.5, num = nCell)[None, :]
xy_dat = np.reshape(np.asarray(np.meshgrid(X_points, Y_points)).T, (nCell*nCell, 2))

TrainGen = utils.Generator_NS(X_Train, Y_Train, Batch = args.Batch, nCol = args.nCol, nCell = nCell, width = 0.5)
ValidGen = utils.Generator_NS(X_Val, Y_Val, Batch = args.Batch, nCol = args.nCol, nCell = nCell, width = 0.5)
TestGen = utils.Generator_NS(X_Test, Y_Test, Batch = args.Batch, nCol = args.nCol, nCell = nCell, width = 0.5)


 

#%% ===========================================================================
# Load the model
# =============================================================================
DON_Model = DON.DeepONet_Stacked_NS(HL_B = args.HL_B, Units_B = args.Units_B, P = args.P, 
                 HL_T = args.HL_T, Units_T = args.Units_T, 
                alpha_CM = args.alpha_CM, alpha_CMM = args.alpha_CMM,
                activation_B = args.activation_B, activation_T = args.activation_T, 
                reg = args.reg, nCell = nCell, Batch = args.Batch, Dat =xy_dat.shape[0], nCol = args.nCol ) 
DON_Model.compile(optimizer=keras.optimizers.Adam(learning_rate=args.lr0))

#%% ===========================================================================
# Predict Fake Input
# =============================================================================
print("Prediction Data points -----")
Out_dat = DON_Model.Predict_Shape((X[0:2,:,:,:], xy_dat) )

#%% =============================================================================
# Set Callbacks
# =============================================================================
Path_res = os.path.join(os.path.dirname(__file__), "../res/")
if not os.path.exists(Path_res + modelName):
    os.mkdir(Path_res + modelName)
Callbacks = [tf.keras.callbacks.ModelCheckpoint(filepath=Path_res + modelName +"/" +  modelName + '.h5',
                                                monitor='loss', save_best_only=True,
                                                save_weights_only=True, verbose=1),
                tf.keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.8,
                                                  patience=args.nEpoch, min_delta=0.01,
                                                  min_lr=1e-7),
                tf.keras.callbacks.CSVLogger(Path_res + modelName + "/" + modelName + '.log', append=True)]


#%% =============================================================================
# Train the model in modules to avoid losing the optimum
# =============================================================================
for iteration in range(args.Training_Iterations):
    DON_Model.fit(TrainGen, validation_data=ValidGen, epochs=args.nEpoch, 
                  steps_per_epoch = nTrain//args.Batch,
                  validation_steps=nValid//args.Batch,
                  callbacks=Callbacks, verbose = 2)
    
  
    DON_Model.load_weights((Path_res + modelName + "/" + modelName + ".h5" ))
    DON_Model.optimizer.lr.assign(DON_Model.optimizer.lr*args.factor_lr)
#%% =============================================================================
# Evaluate model
# =============================================================================
DON_Model = DON.DeepONet_Stacked_NS(HL_B = args.HL_B, Units_B = args.Units_B, P = args.P, 
                 HL_T = args.HL_T, Units_T = args.Units_T, 
                alpha_CM = 1, alpha_CMM = 1,
                activation_B = args.activation_B, activation_T = args.activation_T, 
                reg = args.reg, nCell = nCell, Batch = args.Batch, Dat =xy_dat.shape[0], nCol = args.nCol ) 
DON_Model.compile(optimizer=keras.optimizers.Adam(learning_rate=args.lr0))
Out_dat = DON_Model((X[0:2,:,:], xy_dat) )
DON_Model.load_weights((Path_res + modelName + "/" + modelName + ".h5" ))
Answer = DON_Model.evaluate(TestGen, steps = nTest//args.Batch)


Loss = Answer[0]
mae_u = Answer[1]
mae_v = Answer[2]
mae_p = Answer[3]
mse_u = Answer[4]
mse_v = Answer[5]
mse_p = Answer[6]
pde_0 = Answer[7]
pde_1 = Answer[8]
pde_2 = Answer[9]
resName = Path_res + "Training_DeepONet_NS_Stacked"
with open(resName + ".txt", "a") as f:
    f.write(f"Model = {modelName}: \n \
    Loss = {Loss:.2e}, mae_u = {mae_u:.2e}, mae_v = {mae_v:.2e}, mae_p = {mae_p:.2e}, \n \
    mse_u = {mse_u:.2e}, mse_v = {mse_v:.2e}, mse_p = {mse_p:.2e}, \n \
    pde_0 = {pde_0:.2e}, pde_1 = {pde_1:.2e}, pde_2 = {pde_2:.2e} \n \n ")
    f.close()



#%%

