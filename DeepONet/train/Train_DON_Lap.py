#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 15:54:55 2021

@author: robertplanas
"""

import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../res/"))
import argparse
import h5py as h5
import numpy as np

parser = argparse.ArgumentParser()

# Model Parameters Branch
parser.add_argument('-HL_B', '--HL_B', type = int, default = 5,
                    help = 'Hidden Layers for the brach model')
parser.add_argument('-Units_B', '--Units_B', type = int, default = 100,
                    help = 'Units for the brach model')
parser.add_argument('-act_B', '--activation_B', default = 'swish', 
                    help = 'Activation function for Branch Model')

# Model Parameters Trunk
parser.add_argument('-HL_T', '--HL_T', type = int, default = 5,
                    help = 'Hidden Layers for the trunk model')
parser.add_argument('-Units_T', '--Units_T', type = int, default = 100,
                    help = 'Units for the trunk model')
parser.add_argument('-act_T', '--activation_T', default = 'swish', 
                    help = 'Activation function for trunk Model')

# Model Parameters P
parser.add_argument('-P', '--P', default = 100, type = int, 
                    help = 'Output shape from the nets')

# Alpha for residual
parser.add_argument('-alpha', '--alpha', default = 0, type = float, 
                 help = 'Alpha parameter for the residual. Set to 0 if do not want to use')

# Regularization
parser.add_argument('-reg', '--reg', default = 0, type = float, 
                 help = 'L2 regularization')

#Data
parser.add_argument('-f', '--file', default='../../CNN/data/Laplace_BC_AMG_ED_31.h5',
                    help='data file')

#Traing Parameters
parser.add_argument('-TI', '--Training_Iterations', type=int, default=25,
                    help='Number of Optimizations')
parser.add_argument('-e', '--nEpoch', type=int, default=1000,
                    help='initial train epochs')
parser.add_argument('-patience', '--patience', type=int, default=100,
                    help='patience')

parser.add_argument('-B', '--Batch', type=int, default=20,
                    help='Batch Size')

# learning rate adjustment
parser.add_argument('-f_lr', '--factor_lr', type=float, default = 0.9,
                    help = 'Reducing the learning rate')
parser.add_argument('-lr0', '--lr0', type=float,
                    default=1e-3, help='init leanring rate')


#Restart 
parser.add_argument('-restart', '--restart', default=False, action='store_true',
                    help='restart from checkpoint')
parser.add_argument('-ckpnt', '--checkpoint', default=None,
                    help='Checkpoint name to restart the training from')



# Choose the data type
parser.add_argument('-dt', '--DataType', default='float32',
                    help='DataType')
parser.add_argument('-nVal', '--nVal', type=int, default=1000,
                    help='number of validation solutions ')
parser.add_argument('-nTest', '--nTest', type=int, default=1000,
                    help='number of test solutions ')
parser.add_argument('-nTrain', '--nTrain', type=int, default=8000,
                    help='number of test solutions ')

# Choose nCol and nDat
parser.add_argument('-nCol', '--nCol', default=500, type = int,
                    help='Number of collocation points')




args = parser.parse_args()

import os
import tensorflow as tf
from tensorflow import keras
import DeepONet as DON
import utils as utils 

#%% =============================================================================
#  Define the name of the model 
# =============================================================================


act_B = "_actB-" + str(args.activation_B)
HL_B = "_HLB-" + str(args.HL_B) 
Units_B = "_UB-" + str(args.Units_B)

act_T = "_actT-" + str(args.activation_T)
HL_T = "_HLT-" + str(args.HL_T) 
Units_T = "_UT-" + str(args.Units_T)

P_para = "_P-" + str(args.P)

Reg = "_reg-" + f"{args.reg:.0e}"
Res = "_alpha-" + f"{args.alpha:.0e}"

Fac = "_flr-" + str(args.factor_lr)
Patience = "_Pat-" + str(args.patience)
Iterations = "_Iter-" + str(args.nEpoch)
Data_nTrain = '_nTrain-' + str(args.nTrain)


modelName = "DON_Lap" + act_B + HL_B + Units_B + act_T + HL_T + Units_T + P_para + Reg  + Res + Fac + Patience + Iterations + Data_nTrain

print(f"Model Name : {modelName}")

#%% =============================================================================
# Load the data  
# =============================================================================

# AutoEncoder File
Data_File = os.path.join(os.path.dirname(__file__), args.file)

# load hdf5 data and test residual of Poisson (psn)
psnFile = h5.File(Data_File, 'r')
nSample = psnFile.attrs['nSample']
nCell = psnFile.attrs['domainSize'][0] 
psnData = psnFile['LaplaceSolution']
print('# Samples: {}'.format(nSample))
print('# Cells: {}*{}'.format(nCell, nCell))
print('data shape: ', end='')
print(psnData.shape)

# read the data into numpy array
p = np.zeros(psnData.shape)
p = psnData[:, :, :]
pMin, pMax = np.amin(p[:, :, :]), np.amax(p[:, :, :])
print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))

# split traning and validation
nValid = int(args.nVal)
nTest = int(args.nTest)
nTrain = int(args.nTrain)
print('{} solutions in training, {} in validation, {} in testing'.format(
    nTrain, nValid, nTest))

p = (p -pMin)/(pMax - pMin)*2 -1

#%% =============================================================================
# Create the inputs
# =============================================================================
Y_Train = p[:nTrain ,:,:]
Y_Val = p[nTrain:nTrain + nValid,:,:]
Y_Test = p[nTrain + nValid:,:,:] 

X = np.zeros_like(p)
X[:,0,:] = p[:,0,:]
X[:,-1,:] = p[:,-1,:]
X[:,:,0] = p[:,:,0]
X[:,:,-1] = p[:,:,-1]

X_Train = X[:nTrain, :, :]
X_Val = X[nTrain:nTrain + nValid,:,:]
X_Test = X[nTrain + nValid:, : ,:]

X_points = np.linspace(0,1, num = nCell)[None, :]
Y_points = np.linspace(0,1, num = nCell)[None, :]
xy_dat = np.reshape(np.asarray(np.meshgrid(X_points, Y_points)).T, (nCell*nCell, 2))

TrainGen = utils.Generator(X_Train, Y_Train, Batch = args.Batch, nCol = args.nCol)
ValidGen = utils.Generator(X_Val, Y_Val, Batch = args.Batch, nCol = args.nCol)
TestGen = utils.Generator(X_Test, Y_Test, Batch = args.Batch, nCol = args.nCol)


 

#%% ===========================================================================
# Load the model
# =============================================================================
DON_Model = DON.DeepONet_Unstacked_Lap(HL_B = args.HL_B, Units_B = args.Units_B, P = args.P, 
                 HL_T = args.HL_T, Units_T = args.Units_T, 
                alpha = args.alpha, activation_B = args.activation_B, activation_T = args.activation_T, 
                reg = args.reg, nCell = nCell, Batch = args.Batch, Dat =xy_dat.shape[0], nCol = args.nCol ) 
DON_Model.compile(optimizer=keras.optimizers.Adam(learning_rate=args.lr0))

#%% ===========================================================================
# Predict Fake Input
# =============================================================================
print("Prediction Data points -----")
Out_dat = DON_Model.Predict_Shape((X[0:2,:,:], xy_dat) )

#%% =============================================================================
# Set Callbacks
# =============================================================================
Path_res = os.path.join(os.path.dirname(__file__), "../res/")
if not os.path.exists(Path_res + modelName):
    os.mkdir(Path_res + modelName)
Callbacks = [tf.keras.callbacks.ModelCheckpoint(filepath=Path_res + modelName +"/" +  modelName + '.h5',
                                                monitor='loss', save_best_only=True,
                                                save_weights_only=True, verbose=1),
                tf.keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=args.factor_lr,
                                                  patience=args.patience, min_delta=0.01,
                                                  min_lr=1e-7),
                tf.keras.callbacks.CSVLogger(Path_res + modelName + "/" + modelName + '.log', append=True)]


#%% =============================================================================
# Train the model in modules to avoid losing the optimum
# =============================================================================

DON_Model.fit(TrainGen, validation_data=ValidGen, epochs=args.nEpoch, 
              steps_per_epoch = nTrain//args.Batch,
              validation_steps=nValid//args.Batch,
              callbacks=Callbacks, verbose = 2)

  
DON_Model.load_weights((Path_res + modelName + "/" + modelName + ".h5" ))
DON_Model.optimizer.lr.assign(DON_Model.optimizer.lr*args.factor_lr)
#%% =============================================================================
# Evaluate model
# =============================================================================
DON_Model = DON.DeepONet_Unstacked_Lap(HL_B = args.HL_B, Units_B = args.Units_B, P = args.P, 
                 HL_T = args.HL_T, Units_T = args.Units_T, 
                alpha = 1, activation_B = args.activation_B, activation_T = args.activation_T, 
                reg = args.reg, nCell = nCell, Batch = args.Batch, Dat =xy_dat.shape[0], nCol = args.nCol ) 
DON_Model.compile(optimizer=keras.optimizers.Adam(learning_rate=args.lr0))
Out_dat = DON_Model((X[0:2,:,:], xy_dat) )
DON_Model.load_weights((Path_res + modelName + "/" + modelName + ".h5" ))
Answer = DON_Model.evaluate(TestGen, steps = nTest//args.Batch)
Loss = Answer[0]
MSE = Answer[1]
MAE = Answer[2]
Res = Answer[3]
resName = Path_res + "Training_DeepONet"


Answer = DON_Model.evaluate(TrainGen, steps = nTrain//args.Batch)
Loss_Train = Answer[0]
MSE_Train = Answer[1]
MAE_Train = Answer[2]
Res_Train = Answer[3]
with open(resName + ".txt", "a") as f:
    f.write(f"Model = {modelName} \n \t Test -- Loss = {Loss:.2e}, Res = {Res:.2e}, MSE = {MSE:.2e}, MAE = {MAE:.2e}  \n")
    f.write(f"\t Train -- Loss = {Loss_Train:.2e}, Res = {Res_Train:.2e}, MSE = {MSE_Train:.2e}, MAE = {MAE_Train:.2e}  \n")
    f.close()
    
    



#%%

