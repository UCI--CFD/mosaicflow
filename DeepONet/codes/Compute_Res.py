#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 14 11:19:23 2021

@author: robertplanas
"""



import sys
import os
import h5py as h5
import numpy as np
import tensorflow as tf
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../models/"))
sys.path.append('./../src/')
import xy_models as model
import Genomes as G
import time
import Print_to_File as P2F
from matplotlib import pyplot as plt
import utils_og as utils 
import DeepONet as DON

import time
import Print_to_File as P2F
from matplotlib import pyplot as plt


def Compute_Res_Laplace(Solution):
    h = 1/31
    Shape_Solution = Solution.shape
    RHS = np.zeros((Shape_Solution[0] -2, Shape_Solution[1]- 2))  
    for k in range( Shape_Solution[0] -2 ):
        for l in range(Shape_Solution[1] -2 ):
            RHS[k,l] = (4*Solution[k+1,l+1] - Solution[k+1, l] - Solution[k,l+1] - Solution[k+2, l+1] - Solution[k+1, l+2])/(h**2)
        
    mean_RHS = np.mean(np.square(RHS))
    max_RHS = np.max(np.abs(RHS))
    print('Max Absolute Error RHS ',max_RHS )
    return mean_RHS


    
    
    
    

################# LAPLACE ######################

#%% =============================================================================
# Load Model
# =============================================================================
Data_Type = 'float32'
tf.keras.backend.set_floatx(Data_Type)
Path = os.path.join(os.path.dirname(__file__), "../models/")
modelName ="DON_Lap_actB-swish_HLB-5_UB-100_actT-swish_HLT-5_UT-100_P-100_reg-0e+00_alpha-0e+00_flr-0.9_Pat-100_Iter-1000"
modelPath = Path + modelName
modelPath = os.path.join(os.path.dirname(__file__), "../res/")

#Get the structure of the model from the name: 
act_B = modelName[int(modelName.find("_actB-") + 6):int(modelName.find("_HLB")) ]
HL_B = int(modelName[int(modelName.find("_HLB-") + 5):int(modelName.find("_UB-")) ])
UB = int(modelName[int(modelName.find("_UB-") + 4):int(modelName.find("_actT-")) ])
act_T = modelName[int(modelName.find("_actT-") + 6):int(modelName.find("_HLT")) ]
HL_T = int(modelName[int(modelName.find("_HLT-") + 5):int(modelName.find("_UT-")) ])
UT = int(modelName[int(modelName.find("_UT-") + 4):int(modelName.find("_P-")) ])
P = int(modelName[int(modelName.find("_P-") + 3):int(modelName.find("_reg-")) ]) 


DON_Model = DON.MF_DeepONet_UnStacked_Lap(HL_B = HL_B, Units_B = UB, P = P, 
                 HL_T = HL_T, Units_T = UT, 
                activation_B = act_B, activation_T = act_T,Batch = 20, Dat = 1024, nCol = 500,
                ) 
DON_Model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3))

DON_Model.compile(optimizer=tf.keras.optimizers.Adam(1e-3))
void_input = np.zeros((100,32,32))
void_input_xy = np.zeros((1024,2))
Out_1 = DON_Model((void_input, void_input_xy))
Path_res = os.path.join(os.path.dirname(__file__), "../models/")

DON_Model.load_weights((Path_res + modelName + "/" + modelName + ".h5" ))
Out_2 = DON_Model((void_input, void_input_xy))
Out_Substract = tf.reduce_mean(tf.square(Out_1 - Out_2))
print("Loaded Weights Prediction = ", Out_Substract.numpy)
DON_Model.trainable = False
# Freeze the weights
for layer in DON_Model.layers:
    layer.trainable = False



#%% =============================================================================
# Load the data  
# =============================================================================

# AutoEncoder File
Data_File = os.path.join(os.path.dirname(__file__), '../../CNN/data/Laplace_BC_AMG_ED_31.h5')

# load hdf5 data and test residual of Poisson (psn)
psnFile = h5.File(Data_File, 'r')
nSample = psnFile.attrs['nSample']
nCell = psnFile.attrs['domainSize'][0] 
psnData = psnFile['LaplaceSolution']
print('# Samples: {}'.format(nSample))
print('# Cells: {}*{}'.format(nCell, nCell))
print('data shape: ', end='')
print(psnData.shape)

# read the data into numpy array
p = np.zeros(psnData.shape)
p = psnData[:, :, :]
pMin, pMax = np.amin(p[:, :, :]), np.amax(p[:, :, :])
print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))

# split traning and validation
nValid = int(100)
nTest = int(100)
nTrain = int(100)
print('{} solutions in training, {} in validation, {} in testing'.format(
    nTrain, nValid, nTest))

p = (p -pMin)/(pMax - pMin)*2 -1

#%% =============================================================================
# Create the inputs
# =============================================================================
Y_Train = p[:nTrain ,:,:]
Y_Val = p[nTrain:nTrain + nValid,:,:]
Y_Test = p[nTrain + nValid:nTrain + nValid + nTest,:,:] 

X = np.zeros_like(p)
X[:,0,:] = p[:,0,:]
X[:,-1,:] = p[:,-1,:]
X[:,:,0] = p[:,:,0]
X[:,:,-1] = p[:,:,-1]

X_Train = X[:nTrain, :, :]
X_Val = X[nTrain:nTrain + nValid,:,:]
X_Test = X[nTrain + nValid:nTrain + nValid + nTest, : ,:]

X_points = np.linspace(0,1, num = nCell)[None, :]
Y_points = np.linspace(0,1, num = nCell)[None, :]
xy_dat = np.reshape(np.asarray(np.meshgrid(X_points, Y_points)).T, (nCell*nCell, 2))



TrainGen = utils.Generator(X_Train, Y_Train, Batch = 20, nCol = 900)
ValidGen = utils.Generator(X_Val, Y_Val, Batch = 20, nCol = 900)
TestGen = utils.Generator(X_Test, Y_Test, Batch = 100, nCol = 900)

h = 1/31
x = np.linspace(0 + h, 1 - h, 32 -2 )
y = np.linspace(0 + h, 1 - h, 32 -2 )
xy_col = np.reshape(np.asarray(np.meshgrid(x,y)).T, (900,2))
x = np.linspace(0 , 1, 32 )
y = np.linspace(0 , 1 , 32 )
xy_dat = np.reshape(np.asarray(np.meshgrid(x,y)).T, (1024,2))
#%% =============================================================================
# Test the residual on the test data
# =============================================================================

Mean_RHS_AD = []
Mean_RHS_FD = []
for i in range(100):
    print(i)
    Input_Sol = tf.convert_to_tensor(X_Test[i:i+1, :, :], dtype = tf.float32)
    BC_col = DON_Model.Generate_BC(Input_Sol, 900)
    BC_col = DON_Model.Predict_mlp_B_1(BC_col)
    xy_col_mul = tf.tile(xy_col, multiples = [1, 1])
    with tf.GradientTape(persistent=True, watch_accessed_variables=False) as tape2:
        tape2.watch(xy_col_mul)
        with tf.GradientTape(persistent=False,  watch_accessed_variables=False ) as tape1:
            tape1.watch(xy_col_mul)
            xy_col_fp = DON_Model.Predict_mlp_T(xy_col_mul)
            U_col = tf.reduce_sum(tf.multiply(BC_col, xy_col_fp), axis = -1)
            
        U_col_grad = tape1.gradient(U_col, xy_col_mul)
        U_col_grad_x = U_col_grad[:,0]
        U_col_grad_y = U_col_grad[:,1]
    
    U_col_grad_xx = tape2.gradient(U_col_grad_x, xy_col_mul)[:,0]
    U_col_grad_yy = tape2.gradient(U_col_grad_y, xy_col_mul)[:,1]
    Res = tf.reduce_mean(tf.square(U_col_grad_xx + U_col_grad_yy )) 
    Mean_RHS_AD.append(Res)
    BC_dat = DON_Model.Generate_BC(Input_Sol, 1024)
    BC_dat = DON_Model.Predict_mlp_B_1(BC_dat)
    xy_dat_mul = tf.tile(xy_dat, multiples = [1, 1])
    xy_dat_fp = DON_Model.Predict_mlp_T(xy_dat_mul)
    U_dat = tf.reduce_sum(tf.multiply(BC_dat, xy_dat_fp), axis = -1)
    U_dat = np.reshape(U_dat.numpy(), (32,32))
    Res = Compute_Res_Laplace(U_dat)
    Mean_RHS_FD.append(Res)

Mean_RHS_AD= np.asarray(Mean_RHS_AD)
Mean_RHS_FD = np.asarray(Mean_RHS_FD)

MEAN_RHS_AD = np.mean(Mean_RHS_AD) 
MEAN_RHS_FD = np.mean(Mean_RHS_FD)
