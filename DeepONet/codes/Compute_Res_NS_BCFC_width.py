#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 14 11:19:23 2021

@author: robertplanas
"""



import sys
import os
import h5py as h5
import numpy as np
import tensorflow as tf
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../models/"))
sys.path.append('./../src/')
import time
import Print_to_File as P2F
from matplotlib import pyplot as plt
import NS_model as DON

import time
import Print_to_File as P2F
from matplotlib import pyplot as plt
import NS_dataset as NS_D 


def Compute_Residual_Whole(U_Predicted, V_Predicted, P_Predicted,
                     Width = 0.5, nCell = 33) :
    h = 1/Width*(nCell -1)
              
    #Compute the derivatives for U 
    U_Pred_R =  tf.roll(U_Predicted, shift = 1, axis = -1)
    U_Pred_U = tf.roll(U_Predicted, shift = -1, axis = -2)
    U_Pred_L = tf.roll(U_Predicted, shift = -1, axis = -1)
    U_Pred_D = tf.roll(U_Predicted, shift = 1, axis = -2)
    
    U_Pred_L_s = (U_Pred_L - U_Predicted)*h
    U_Pred_R_s = (U_Predicted - U_Pred_R)*h
    U_Pred_D_s = ( U_Predicted - U_Pred_D )*h
    U_Pred_U_s = (U_Pred_U - U_Predicted)*h
    
    U_Pred_x = (U_Pred_L_s + U_Pred_R_s)/2
    U_Pred_y = (U_Pred_U_s + U_Pred_D_s)/2
    U_Pred_xx = (U_Pred_L_s -U_Pred_R_s )*h
    U_Pred_yy = (U_Pred_U_s - U_Pred_D_s )*h
    
    #Compute the derivatives for V 
    V_Pred_R =  tf.roll(V_Predicted, shift = 1, axis = -1)
    V_Pred_U = tf.roll(V_Predicted, shift = -1, axis = -2)
    V_Pred_L = tf.roll(V_Predicted, shift = -1, axis = -1)
    V_Pred_D = tf.roll(V_Predicted, shift = 1, axis = -2)
    
    V_Pred_L_s = (V_Pred_L - V_Predicted )*h
    V_Pred_R_s = (V_Predicted - V_Pred_R)*h
    V_Pred_D_s = (V_Predicted- V_Pred_D)*h
    V_Pred_U_s = (V_Pred_U- V_Predicted)*h
    
    V_Pred_x = (V_Pred_L_s + V_Pred_R_s)/2
    V_Pred_y = (V_Pred_U_s + V_Pred_D_s)/2
    V_Pred_xx = (V_Pred_L_s - V_Pred_R_s)*h
    V_Pred_yy = (V_Pred_U_s - V_Pred_D_s)*h
    
    #Compute the derivatives for P
    P_Pred_R =  tf.roll(P_Predicted, shift = 1, axis = -1)
    P_Pred_U = tf.roll(P_Predicted, shift = -1, axis = -2)
    P_Pred_L = tf.roll(P_Predicted, shift = -1, axis = -1)
    P_Pred_D = tf.roll(P_Predicted, shift = 1, axis = -2)
    
    P_Pred_L_s = (P_Pred_L - P_Predicted )*h
    P_Pred_R_s = ( P_Predicted - P_Pred_R)*h
    P_Pred_D_s = (P_Predicted- P_Pred_D)*h
    P_Pred_U_s = (P_Pred_U- P_Predicted)*h
    
    P_Pred_x = (P_Pred_L_s + P_Pred_R_s)/2
    P_Pred_y = (P_Pred_U_s + P_Pred_D_s)/2
    
    pde0_Values = U_Pred_x + V_Pred_y
    pde1_Values = U_Predicted*U_Pred_x + V_Predicted*U_Pred_y + P_Pred_x - (U_Pred_xx + U_Pred_yy)/500.0
    pde2_Values = U_Predicted*V_Pred_x + V_Predicted*V_Pred_y + P_Pred_y - (V_Pred_xx + V_Pred_yy)/500.0
    
    # Take out the boundaries
    pde0_IN = tf.keras.layers.Cropping2D(cropping=((1, 1), (1, 1)))(tf.expand_dims(pde0_Values, axis = -1))
    pde1_IN = tf.keras.layers.Cropping2D(cropping=((1, 1), (1, 1)))(tf.expand_dims(pde1_Values, axis = -1))
    pde2_IN = tf.keras.layers.Cropping2D(cropping=((1, 1), (1, 1)))(tf.expand_dims(pde2_Values, axis = -1))
    
    pde0 = tf.reduce_mean(tf.square(pde0_IN), axis = [1,2])
    pde1 = tf.reduce_mean(tf.square(pde1_IN), axis = [1,2])
    pde2 = tf.reduce_mean(tf.square(pde2_IN), axis = [1,2])
    
    return pde0, pde1, pde2
    

    
    
    
    

################# LAPLACE ######################

#%% =============================================================================
# Load Model
# =============================================================================
Data_Type = 'float32'
tf.keras.backend.set_floatx(Data_Type)
Path = os.path.join(os.path.dirname(__file__), "../models/")
modelName ="nsbc_nop_a1-1-0.5-400x6-3"
modelPath = Path + modelName

DON_Model = DON.NSModelHardBC(width=[400, 400, 400, 400, 400, 400, 3])
DON_Model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3))
void_input_BC = np.zeros((100,256))
void_input_xy = np.zeros((100,2))
void_input = (void_input_BC, void_input_xy)
Out_1 = DON_Model(void_input)
Path_res = os.path.join(os.path.dirname(__file__), "../../NS/models/")
#%%
DON_Model.load_weights(tf.train.latest_checkpoint(Path_res + modelName ))
Out_2 = DON_Model(void_input)
Out_Substract = tf.reduce_mean(tf.square(Out_1 - Out_2))
print("Loaded Weights Prediction = ", Out_Substract.numpy)
DON_Model.trainable = False
# Freeze the weights
for layer in DON_Model.layers:
    layer.trainable = False



#%% ################################################################
# configs
################################################################
DATA_PATH = '../../CNN/data/genomes_merged.h5'
Data_File = os.path.join(os.path.dirname(__file__), DATA_PATH)


# load data
dataSet = NS_D.NSDataSet()
dataSet.add_file(Data_File)
dataSet.load_data()
dataSet.extract_genome_bc()
dataSet.summary()
DATA = dataSet.var[:,:,:,:]

C_Samples = dataSet.var.shape[0]

Input_Variables = 2
Output_Variables = 3


np.random.shuffle(DATA)
Total_Samples = len(DATA)
nval = 100
ntest = 100
ntrain = 2000


batch_size = 20
learning_rate = 0.001

step_size = 100
gamma = 0.5

s = 33
################################################################
# load data and data normalization
################################################################
x_Data = np.ones_like(DATA[:ntrain + nval + ntest, :,:,:Input_Variables])*DATA[:ntrain + nval + ntest, :,:,:Input_Variables]
x_Data[:,1:-1,1:-1, :] = 0
x=  tf.convert_to_tensor(x_Data, dtype = tf.float32)
y=  tf.convert_to_tensor(DATA[:ntrain + nval + ntest, :,:,:], dtype = tf.float32)


x_train = x[:ntrain, :, :, : ]
y_train = y[:ntrain, :, :, : ]

x_test = x[ntrain:ntrain+ntest, :, :, :]
y_test = y[ntrain:ntrain+ntest, :, :, :]

x_val = x[ntrain + ntest:ntrain+ntest + nval, :, :, :]
y_val = y[ntrain + ntest:ntrain+ntest + nval, :, :, :]
#%% =============================================================================
# Create the inputs
# =============================================================================

width = 0.25/2
h = 1/32*2*width
x = np.linspace(0 + h, width - h, 33 -2 )
y = np.linspace(0 + h, width - h, 33 -2 )
xy_col = np.reshape(np.asarray(np.meshgrid(x,y)).T, (961,2))
x = np.linspace(0 , 0.5, int(33/width/2) )
y = np.linspace(0 , 0.5 , int(33/width/2) )
xy_dat = np.reshape(np.asarray(np.meshgrid(x,y)).T, (int(1/4*1089/width**2),2))
DON_Model.nCell = 33


#%%
validGen = dataSet.generator_bc_xy_label(\
               100, 2100, 1, nDataPoint1D=31,\
               omitP=True)



#%% =============================================================================
# Test the residual on the test data
# =============================================================================

pde_0_AD = []
pde_1_AD = []
pde_2_AD = []

pde_0_FD = []
pde_1_FD = []
pde_2_FD = []
validGen = dataSet.generator_bc_xy_label(\
               100, 2100, 1, nDataPoint1D=31,\
               omitP=True)
for i in range(100):
    print(i)
    IN_Out = next(validGen)
    Input_Sol = IN_Out[0][0][0:1,:]
    Label = np.reshape(IN_Out[1].T, (3,33,33))
    BC_col = DON_Model.Generate_BC(Input_Sol, 961)
    xy_col_mul = tf.cast(tf.tile(xy_col, multiples = [1, 1]), tf.float32)
    with tf.GradientTape(persistent=True, watch_accessed_variables=False) as tape2:
        tape2.watch(xy_col_mul)
        with tf.GradientTape(persistent=True,  watch_accessed_variables=False ) as tape1:
            tape1.watch(xy_col_mul)
            UVP_col = DON_Model((BC_col, xy_col_mul))
            U_col = UVP_col[:,0:1]
            V_col = UVP_col[:,1:2]
            P_col = UVP_col[:,2:3]
            
        U_col_grad = tape1.gradient(U_col, xy_col_mul)
        U_col_grad_x = U_col_grad[:,0]
        U_col_grad_y = U_col_grad[:,1]
        
        V_col_grad = tape1.gradient(V_col, xy_col_mul)
        V_col_grad_x = V_col_grad[:,0]
        V_col_grad_y = V_col_grad[:,1]
        
        P_col_grad = tape1.gradient(P_col, xy_col_mul)
        P_col_grad_x = P_col_grad[:,0]
        P_col_grad_y = P_col_grad[:,1]
    
    
    
    U_col_grad_xx = tape2.gradient(U_col_grad_x, xy_col_mul)[:,0]
    U_col_grad_yy = tape2.gradient(U_col_grad_y, xy_col_mul)[:,1]
    
    V_col_grad_xx = tape2.gradient(V_col_grad_x, xy_col_mul)[:,0]
    V_col_grad_yy = tape2.gradient(V_col_grad_y, xy_col_mul)[:,1]
    
    U_col = U_col[:,0]
    V_col = V_col[:,0]
    P_col = P_col[:,0]
    
    pde0_Values = U_col_grad_x + V_col_grad_y
    pde1_Values = U_col*U_col_grad_x + V_col*U_col_grad_y + P_col_grad_x - (U_col_grad_xx + U_col_grad_yy)/500.0
    pde2_Values = U_col*V_col_grad_x + V_col*V_col_grad_y + P_col_grad_y - (V_col_grad_xx + V_col_grad_yy)/500.0
    
    pde_0_AD.append(tf.reduce_mean(tf.square(pde0_Values)))
    pde_1_AD.append(tf.reduce_mean(tf.square(pde1_Values)))
    pde_2_AD.append(tf.reduce_mean(tf.square(pde2_Values)))
    
    BC_dat = DON_Model.Generate_BC(Input_Sol, int(1/4*1089/width**2))
    xy_dat_mul = tf.tile(xy_dat, multiples = [1, 1])
    UVP_dat = DON_Model((BC_dat, xy_dat_mul))
    U_Predicted = tf.transpose(tf.reshape(UVP_dat[:,0], (1,int(33/2/width),int(33/2/width))), perm = [0,2,1])
    V_Predicted = tf.transpose(tf.reshape(UVP_dat[:,1], (1,int(33/2/width),int(33/2/width))), perm = [0,2,1])
    P_Predicted = tf.transpose(tf.reshape(UVP_dat[:,2], (1,int(33/2/width),int(33/2/width))), perm = [0,2,1])
    pde_0_current, pde_1_current, pde_2_current = Compute_Residual_Whole(U_Predicted, V_Predicted, P_Predicted, Width = width)
    pde_0_FD.append(pde_0_current.numpy().flatten())
    pde_1_FD.append(pde_1_current.numpy().flatten())
    pde_2_FD.append(pde_2_current.numpy().flatten())
    
pde_0_AD= np.asarray(pde_0_AD)
pde_1_AD= np.asarray(pde_1_AD)
pde_2_AD= np.asarray(pde_2_AD)    

pde_0_FD= np.asarray(pde_0_FD)
pde_1_FD= np.asarray(pde_1_FD)
pde_2_FD= np.asarray(pde_2_FD)



pde_0_AD_MEAN = np.mean(pde_0_AD)
pde_1_AD_MEAN = np.mean(pde_1_AD)
pde_2_AD_MEAN = np.mean(pde_2_AD)


pde_0_FD_MEAN = np.mean(pde_0_FD)
pde_1_FD_MEAN = np.mean(pde_1_FD)
pde_2_FD_MEAN = np.mean(pde_2_FD)



print(f'{pde_0_FD_MEAN:.3e}')
print(f'{pde_1_FD_MEAN:.3e}')
print(f'{pde_2_FD_MEAN:.3e}')

print(f'{pde_0_AD_MEAN:.3e}')
print(f'{pde_1_AD_MEAN:.3e}')
print(f'{pde_2_AD_MEAN:.3e}')

