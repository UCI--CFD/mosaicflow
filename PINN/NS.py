GPU = None
if(GPU is not None):
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = "1"
    
    
import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-d', '--data', default="../NS/data/square_cavity.vtk",
                    help='Select a data file')
parser.add_argument('-l', '--arch', type=int, nargs='*', help='architecture',
                    default = [2, 50, 50, 50,50,50, 2])

parser.add_argument('-c', '--collocation', type=int, default=20000,
                    help ='Amount of collocation points in the domain 1x1')

parser.add_argument('-nIter', '--nIter', type=int, default=40000,
                    help ='Epochs of the ADAM optimizer')
parser.add_argument('-DataType', '--DataType', default="float32",
                    help='float32 or float64, must match the model')
parser.add_argument('-print', '--print', default=False,
                    help='Print to a file if true')
parser.add_argument('-plot', '--plot', default=False,
                    help='Plot to files')
parser.add_argument('-reproduce', '--reproduce', default=False, action='store_true',\
                    help='reproduce the comparison in paper')

args = parser.parse_args()


if(args.reproduce):
    args.data = "../NS/data/square_cavity.vtk"
    args.collocation = 20000
    args.arch = [2, 50, 50, 50,50,50, 2]
    args.nIter = 40000
    args.DataType = "float32"
    args.print = True
    args.plot = True


import os
import sys

sys.path.append(os.path.dirname(__file__))
#sys.path.append(os.path.join(os.path.dirname(__file__), "../NS_Genomes_src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../NS/src/"))
#sys.path.append(os.path.join(os.path.dirname(__file__), "../models/"))

import tensorflow as tf
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
import pandas as pd
from NS_model_tf import Sampler, Navier_Stokes2D, Sampler_Index
import vtk as vtk
from vtk.util.numpy_support import vtk_to_numpy
import time as time
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable


# =============================================================================
# Print File
# =============================================================================

def Print_vector_File(File_Name, Vector, Vector_Name):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt,"a") 
    file1.write(Vector_Name + " = [")
    for index,element in enumerate(Vector):
        if( index == len(Vector)-1):
            file1.write(str(element) + "]" + "\n")
        else:
            file1.write(str(element) + ',')
    file1.close()
    
def Print_String_File(File_Name, String):
    File_Name_txt = File_Name + ".txt"
    file1 = open(File_Name_txt,"a") 
    file1.write(String + " \n")
    file1.close()

# =============================================================================
# Function to read data
# =============================================================================
def Read_Data(vtkFile):
    Data_File_vtk = os.path.join(os.path.dirname(__file__), vtkFile)
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(Data_File_vtk)
    reader.Update()
    vtkData = reader.GetOutput()
    # check # points


    p = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
    U = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
    pntxyz =  vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPoints().GetData())

    Block = np.zeros(((129,129,3)))
    Coords_Block = np.zeros((129,129,2))
    
    s,  e  = 0, 129*129
    ni, nj = 129,129
    Coords_Block[...]  = np.reshape(pntxyz[s:e,:2], (ni,nj,2))
    Block[:,:,:2] = np.reshape(U[s:e,:2],(ni,nj,2))
    Block[:,:,2]  = np.reshape(p[s:e], (ni,nj))
    
    # You need to coarse the grid
    x0 = 0
    y0 = 0
    Width = 1
    Height = 1
    U = Block[x0:x0+int(Width*2*65):2,y0:y0+int(Height*2*65):2,:]
    Coords = Coords_Block[x0:x0+int(Width*2*65):2,y0:y0+int(Height*2*65):2,:]
    
    return U, Coords

# =============================================================================
# PINN CODES
# =============================================================================

if __name__ == '__main__':
    
    def f(x):
        num = x.shape[0]
        return np.zeros((num, 2))

    def operator(psi, p, x, y, Re, sigma_x=1.0, sigma_y=1.0):
        u = tf.gradients(psi, y)[0] / sigma_y
        v = - tf.gradients(psi, x)[0] / sigma_x

        u_x = tf.gradients(u, x)[0] / sigma_x
        u_y = tf.gradients(u, y)[0] / sigma_y

        v_x = tf.gradients(v, x)[0] / sigma_x
        v_y = tf.gradients(v, y)[0] / sigma_y

        p_x = tf.gradients(p, x)[0] / sigma_x
        p_y = tf.gradients(p, y)[0] / sigma_y

        u_xx = tf.gradients(u_x, x)[0] / sigma_x
        u_yy = tf.gradients(u_y, y)[0] / sigma_y

        v_xx = tf.gradients(v_x, x)[0] / sigma_x
        v_yy = tf.gradients(v_y, y)[0] / sigma_y

        Ru_momentum = u * u_x + v * u_y + p_x - (u_xx + u_yy) / Re
        Rv_momentum = u * v_x + v * v_y + p_y - (v_xx + v_yy) / Re

        return Ru_momentum, Rv_momentum

    # Parameters of equations
    Re = 500.0

    # Domain boundaries
    
    U, Coords = Read_Data(args.data)
    
    
    bc1_values = U[-1,:,:2]
    bc1_coords = Coords[-1,:,:]
    bc2_values = U[:,0,:2]
    bc2_coords = Coords[:,0,:]
    bc3_values = U[:,-1,:2]
    bc3_coords = Coords[:,-1,:]
    bc4_values = U[0,:,:2]
    bc4_coords = Coords[0,:,:]
    
    bc1= Sampler_Index(2, bc1_coords, bc1_values, name='Dirichlet BC1')
    bc2 = Sampler_Index(2, bc2_coords, bc2_values, name='Dirichlet BC2')
    bc3= Sampler_Index(2, bc3_coords, bc3_values, name='Dirichlet BC3')
    bc4= Sampler_Index(2, bc4_coords, bc4_values, name='Dirichlet BC4')

    bcs_sampler = [bc1, bc2, bc3, bc4]

    # Create residual sampler
    
    U_flatted = np.reshape(U[:,:,0:2], (65*65,2))
    Coords_Flatted = np.reshape(Coords[:,:,:], (65*65,2))
    
    dom_coords = np.array([[0.0, 0.0],
                           [1.0, 1.0]])

    
    res_sampler = Sampler(2, dom_coords, lambda x: f(x), name='Forcing')

    # Define model
    mode = 'M4'
    layers = args.arch

    model = Navier_Stokes2D(layers, operator, bcs_sampler, res_sampler, Re, mode)

    # Train model
    Start_Time = time.time()
    model.train(nIter=args.nIter, batch_size=128)
    Elapsed_Time = time.time() - Start_Time
    
    # Test Data
    nx = 65
    ny = 65  # change to 100
    x = np.linspace(0.0, 1.0, nx)
    y = np.linspace(0.0, 1.0, ny)
    X, Y = np.meshgrid(x, y)

    X_star = Coords_Flatted

    # Predictions
    psi_pred, p_pred = model.predict_psi_p(X_star)
    u_pred, v_pred = model.predict_uv(X_star)
    
    psi_star = griddata(X_star, psi_pred.flatten(), (X, Y), method='cubic')
    p_star = griddata(X_star, p_pred.flatten(), (X, Y), method='cubic')
    u_star = griddata(X_star, u_pred.flatten(), (X, Y), method='cubic')
    v_star = griddata(X_star, v_pred.flatten(), (X, Y), method='cubic')

    velocity = np.sqrt(u_pred**2 + v_pred**2)
    velocity_star = griddata(X_star, velocity.flatten(), (X, Y), method='cubic')
    
    # Reference
    u_ref= U[:,:,0]
    v_ref= U[:,:,1]
    velocity_ref = np.sqrt(u_ref**2 + v_ref**2)
    
    # Relative error
    error = np.linalg.norm(velocity_star - velocity_ref.T, 2) / np.linalg.norm(velocity_ref, 2)
    print('l2 error: {:.2e}'.format(error))
  
    # Errors
    MAE_u_x = np.mean(np.abs(u_star[:,:] - U[:,:,0]))
    MAE_u_y = np.mean(np.abs(v_star[:,:] - U[:,:,1]))

    print('MAE_U_x', MAE_u_x)
    print('MAE_U_y', MAE_u_y)
    print('Elapsed_Time', Elapsed_Time)
    
    vtkFile = args.data
    Name = vtkFile[25:-4] 
    res_Name = "res_PINN_" + Name
    Path_To_Save_Res = (os.path.join(os.path.dirname(__file__), res_Name))
    if(args.print):
        if not os.path.exists(Path_To_Save_Res):
            os.makedirs(Path_To_Save_Res)
        
        Path_File = Path_To_Save_Res + "/" + 'Test_PINN_NS_single_cavity'
        Print_String_File(Path_File, Name)
        Print_vector_File(Path_File, [MAE_u_x], "MAE_U_x")
        Print_vector_File(Path_File, [MAE_u_y], "MAE_U_y")
        Print_vector_File(Path_File, [Elapsed_Time], "Elapsed_Time")
        
    if(args.plot):
        if not os.path.exists(Path_To_Save_Res):
            os.makedirs(Path_To_Save_Res)
        Solution_Magnitude =  np.sqrt(np.square(u_star[:,:]) + np.square(v_star[:,:]))
        
        plt.figure()
        ax = plt.gca()
        im2 = plt.imshow(np.flip(np.flip(Solution_Magnitude, axis = 1)), cmap='rainbow', extent=[0,2,0,2], aspect=1)
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.1)
        cbar = plt.colorbar(im2, cax=cax, ticks = [0,0.5, 1])
        plt.clim(0,1)
        Path_File = Path_To_Save_Res + "/" + 'NS_Velocity_Magnitude_single_cavity_' + Name + '.pdf'
        plt.savefig(Path_File, format='pdf', bbox_inches='tight')  
        
        nx = 65
        ny= 65 
        x = np.linspace(0.0, 2.0, nx)
        y = np.linspace(0.0, 2.0, ny)
        X, Y = np.meshgrid(x, y)
        
        plt.figure()
        ax = plt.gca()
        plt.streamplot(x[:], y[:],u_star[:,:], v_star[:,:], color = Solution_Magnitude, cmap = 'rainbow', density=1.5)
        ax.set_aspect(aspect=1)
        plt.xlim(((0,2)))
        plt.ylim(((0,2))) 
        plt.clim(0,1)
        Path_File = Path_To_Save_Res + "/" + 'NS_Streamlines_single_cavity_' + Name + '.pdf'
        plt.savefig(Path_File, format='pdf', bbox_inches='tight') 
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
