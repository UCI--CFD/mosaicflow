#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 14:48:45 2021

@author: robertplanas
"""

import numpy as np
from matplotlib import pyplot as plt


def Check_Surroundings(i,j,Domain):
    if(np.isnan(Domain[i,j])):
        return True
    if(i == 0 or j == 0 or i == len(Domain[:,0]) - 1 or j == len(Domain[0,:]) -1 ):
        return True
    
    if(np.isnan(Domain[i-1,j]) or np.isnan(Domain[i+1,j]) or np.isnan(Domain[i,j+1]) or np.isnan(Domain[i,j-1])):
        return True
 
    return False 

def Extract_Border_Domain(Domain, Nan_or_Zero = "NaN"):
    if Nan_or_Zero == "NaN":
        Value = np.nan
    else:
        Value = 0
    Size_x = len(Domain[:,0, 0])
    Size_y = len(Domain[0,:, 0])
    Domain_Return = np.empty((Size_x, Size_y, 3))
    for i in range(Size_x):
        for j in range(Size_y):
            if(Check_Surroundings(i,j, Domain[:,:,0])):
                Domain_Return[i,j, :] = Domain[i,j, :]
            else: 
                Domain_Return[i,j, :] = Value
    return Domain_Return 



class Genome:
    def __init__(self, width = 0.5, Points_Border = 33, x0_global = 0, y0_global = 0):
        self.x0_global = x0_global
        self.y0_global = y0_global
        self.width = width
        self.Points_Border = Points_Border
        self.h = self.width/(self.Points_Border-1)
        X = np.linspace(0, self.width, self.Points_Border)
        Y = np.linspace(0, self.width, self.Points_Border)
        XY = np.reshape(np.meshgrid(X,Y), (2,self.Points_Border**2)).T
        self.Inner_Points_Grid = XY
        self.Inner_Borders = [] # (X_R, Y_R, Value, index_shared)
        self.BC_Values = np.zeros((2*4*(self.Points_Border -1) ))
        self.BC_Info = []
        self.i0 = int(self.x0_global*((self.Points_Border-1)/self.width))
        self.j0 = int(self.y0_global*((self.Points_Border-1)/self.width))
        self.I = np.zeros_like(self.BC_Values)
        self.J = np.zeros_like(self.BC_Values)
        
    def Set_Borders_New_Definition(self, Shared_Borders, p = np.zeros((33,33,2)), Coords = np.zeros((33,33,2))):
        Coords_R = np.ones((33,33,2))*Coords[self.i0: self.i0 + self.Points_Border, self.j0:self.j0 + self.Points_Border ,:]
        Coords_R -= [self.y0_global, self.x0_global]
        self.Inner_Points_Grid = np.reshape(Coords_R, ((self.Points_Border)**2,2))
        # Define Left Borders
        for i in range(0,self.Points_Border -1):
            self.BC_Values[2*i ] = p[self.i0 , self.j0  +i  ,0]
            self.BC_Values[2*i +1] = p[self.i0  , self.j0 + i ,1]
            self.I[2*i] = Coords[self.i0 , self.j0  +i,0]
            self.J[2*i] = Coords[self.i0 , self.j0  +i,1]
            self.I[2*i + 1] = Coords[self.i0 , self.j0  +i,0]            
            self.J[2*i + 1] = Coords[self.i0 , self.j0  +i,1]
            
        # Define Top Borders
        for i in range(0,self.Points_Border -1):
            self.BC_Values[2*i + 2*(self.Points_Border-1) ] = p[self.i0 + i , self.j0  + self.Points_Border -1,0]
            self.BC_Values[2*i +1 + 2*(self.Points_Border -1)] = p[self.i0 + i  , self.j0 + self.Points_Border -1,1]
            self.I[2*i + 2*(self.Points_Border-1) ] = Coords[self.i0 + i , self.j0  + self.Points_Border -1,0]
            self.J[2*i + 2*(self.Points_Border-1) ] = Coords[self.i0 + i , self.j0  + self.Points_Border -1,1]
            self.I[2*i + 1 + 2*(self.Points_Border -1)]  = Coords[self.i0 + i , self.j0  + self.Points_Border -1,0]
            self.J[2*i + 1 + 2*(self.Points_Border -1)]  = Coords[self.i0 + i , self.j0  + self.Points_Border -1,1]
        
        # Define Right Borders
        for i in range(0,self.Points_Border-1):
            self.BC_Values[2*i + 4*(self.Points_Border-1)] = p[self.i0 + self.Points_Border -1, self.j0 + self.Points_Border -1  -i ,0]
            self.BC_Values[2*i +1 + 4*(self.Points_Border-1)] = p[self.i0 + self.Points_Border -1, self.j0 + self.Points_Border -1  -i ,1]
            self.I[2*i + 4*(self.Points_Border-1)] = Coords[self.i0 + self.Points_Border -1, self.j0 + self.Points_Border -1  -i ,0]
            self.J[2*i + 4*(self.Points_Border-1)] = Coords[self.i0 + self.Points_Border -1, self.j0 + self.Points_Border -1  -i ,1]
            self.I[2*i + 1 + 4*(self.Points_Border-1)] = Coords[self.i0 + self.Points_Border -1, self.j0 + self.Points_Border -1  -i ,0]
            self.J[2*i + 1 + 4*(self.Points_Border-1)] = Coords[self.i0 + self.Points_Border -1, self.j0 + self.Points_Border -1  -i ,1]
        
        # Define Bootm Borders
        for i in range(0,self.Points_Border-1):
            self.BC_Values[2*i + 6*(self.Points_Border-1)] = p[self.i0 + self.Points_Border -1 -i, self.j0 ,0]
            self.BC_Values[2*i +1 + 6*(self.Points_Border-1)] = p[self.i0 + self.Points_Border -1 - i, self.j0 ,1]
            self.I[2*i + 6*(self.Points_Border-1)] = Coords[self.i0 + self.Points_Border -1 -i, self.j0 ,0]
            self.J[2*i + 6*(self.Points_Border-1)] = Coords[self.i0 + self.Points_Border -1 -i, self.j0 ,1]
            self.I[2*i + 1 + 6*(self.Points_Border-1)] = Coords[self.i0 + self.Points_Border -1 -i, self.j0 ,0]
            self.J[2*i + 1 + 6*(self.Points_Border-1)] = Coords[self.i0 + self.Points_Border -1 -i, self.j0 ,1]
        
        index_BC = np.linspace(0,8*(self.Points_Border-1) -1, 8*(self.Points_Border - 1))[:,None]
        for i in range(0,8*(self.Points_Border -1)):
            if np.isnan(self.BC_Values[i]):
                self.BC_Values[i] = 0
                Value = 0
                index_shared = -1
                Border_Info = np.hstack([self.I[i],self.J[i],index_BC[i],index_shared, Value])
                if(len(self.BC_Info) == 0):
                    self.BC_Info = Border_Info
                else:
                    self.BC_Info = np.vstack([self.BC_Info, Border_Info])
        
        index_shared = len(Shared_Borders)
        for index in range(len(self.BC_Info)):
            self.BC_Info[index,-2] = index_shared +index
            if(index_shared +index == 0):
                Shared_Borders = self.BC_Info[index,:]
            else:
                Shared_Borders = np.vstack([Shared_Borders, self.BC_Info[index,:]])
        
        return Shared_Borders
        
        
    def Set_Borders(self, Shared_Borders, p = np.zeros((33,33,2))):
        # Define Left Borders
        for i in range(0,self.Points_Border -1):
            self.BC_Values[2*i ] = p[self.i0 , self.j0  +i  ,0]
            self.BC_Values[2*i +1] = p[self.i0  , self.j0 + i ,1]
        
            
        I = np.linspace(self.x0_global, self.x0_global, self.Points_Border -1)[:,None]
        J = np.flip(np.linspace(self.y0_global, self.y0_global + self.width - self.h, self.Points_Border -1))[:,None]
        index_BC = np.linspace(0,2*(self.Points_Border-1) -1, 2*(self.Points_Border-1))[:,None]
        for i in range(0,self.Points_Border -1):
            if np.isnan(self.BC_Values[2*i]):
                self.BC_Values[2*i] = 0
                self.BC_Values[2*i+1] = 0
                Value = 0
                index_shared = -1
                Border_Info_x = np.hstack([I[i],J[i],index_BC[2*i],index_shared, Value])
                Border_Info_y = np.hstack([I[i],J[i],index_BC[2*i +1],index_shared, Value])
                Border_Info = np.vstack([Border_Info_x, Border_Info_y])
                if(len(self.BC_Info) == 0):
                    self.BC_Info = Border_Info
                else:
                    self.BC_Info = np.vstack([self.BC_Info, Border_Info])    
                    
        # Define Top Borders
        for i in range(0,self.Points_Border -1):
            self.BC_Values[2*i + 2*(self.Points_Border-1) ] = p[self.i0 + i , self.j0  + self.Points_Border -1,0]
            self.BC_Values[2*i +1 + 2*(self.Points_Border -1)] = p[self.i0 + i  , self.j0 + self.Points_Border -1,1]

            
        I = np.linspace(self.x0_global, self.x0_global + self.width - self.h, self.Points_Border -1)[:,None]
        J = np.linspace(self.y0_global + self.width, self.y0_global + self.width, self.Points_Border -1)[:,None]
        index_BC = np.linspace(2*(self.Points_Border -1),4*(self.Points_Border-1) -1, 2*(self.Points_Border - 1))[:,None]
        for i in range(0,self.Points_Border -1):
            if np.isnan(self.BC_Values[2*i + 2*(self.Points_Border-1)]):
                self.BC_Values[2*i + 2*(self.Points_Border-1)] = 0
                self.BC_Values[2*i+1 + 2*(self.Points_Border-1)] = 0
                Value = 0
                index_shared = -1
                Border_Info_x = np.hstack([I[i],J[i],index_BC[2*i],index_shared, Value])
                Border_Info_y = np.hstack([I[i],J[i],index_BC[2*i +1],index_shared, Value])
                Border_Info = np.vstack([Border_Info_x, Border_Info_y])
                if(len(self.BC_Info) == 0):
                    self.BC_Info = Border_Info
                else:
                    self.BC_Info = np.vstack([self.BC_Info, Border_Info])
                    
        # Define Right Borders
        for i in range(0,self.Points_Border-1):
            self.BC_Values[2*i + 4*(self.Points_Border-1)] = p[self.i0 + self.Points_Border -1, self.j0 + self.Points_Border -1  -i ,0]
            self.BC_Values[2*i +1 + 4*(self.Points_Border-1)] = p[self.i0 + self.Points_Border -1, self.j0 + self.Points_Border -1  -i ,1]

            
        I = np.linspace(self.x0_global + self.width, self.x0_global + self.width, self.Points_Border -1)[:,None]
        J = np.flip(np.linspace(self.y0_global + self.h, self.y0_global + self.width, self.Points_Border -1))[:,None]
        index_BC = np.linspace(4*(self.Points_Border -1),6*(self.Points_Border-1) -1, 2*(self.Points_Border - 1))[:,None]
        for i in range(0,self.Points_Border -1):
            if np.isnan(self.BC_Values[2*i + 4*(self.Points_Border-1)]):
                self.BC_Values[2*i + 4*(self.Points_Border-1)] = 0
                self.BC_Values[2*i+1 + 4*(self.Points_Border-1)] = 0
                Value = 0
                index_shared = -1
                Border_Info_x = np.hstack([I[i],J[i],index_BC[2*i],index_shared, Value])
                Border_Info_y = np.hstack([I[i],J[i],index_BC[2*i +1],index_shared, Value])
                Border_Info = np.vstack([Border_Info_x, Border_Info_y])
                if(len(self.BC_Info) == 0):
                    self.BC_Info = Border_Info
                else:
                    self.BC_Info = np.vstack([self.BC_Info, Border_Info])
        
        # Define Bootm Borders
        for i in range(0,self.Points_Border-1):
            self.BC_Values[2*i + 6*(self.Points_Border-1)] = p[self.i0 + self.Points_Border -1 -i, self.j0 ,0]
            self.BC_Values[2*i +1 + 6*(self.Points_Border-1)] = p[self.i0 + self.Points_Border -1 - i, self.j0 ,1]

            
        I = np.linspace(self.x0_global +self.h, self.x0_global + self.width, self.Points_Border -1)[:,None]
        J = np.flip(np.linspace(self.y0_global, self.y0_global, self.Points_Border -1))[:,None]
        index_BC = np.linspace(6*(self.Points_Border -1),8*(self.Points_Border-1) -1, 2*(self.Points_Border - 1))[:,None]
        for i in range(0,self.Points_Border -1):
            if np.isnan(self.BC_Values[2*i + 6*(self.Points_Border-1)]):
                self.BC_Values[2*i + 6*(self.Points_Border-1)] = 0
                self.BC_Values[2*i+1 + 6*(self.Points_Border-1)] = 0
                Value = 0
                index_shared = -1
                Border_Info_x = np.hstack([I[i],J[i],index_BC[2*i],index_shared, Value])
                Border_Info_y = np.hstack([I[i],J[i],index_BC[2*i +1],index_shared, Value])
                Border_Info = np.vstack([Border_Info_x, Border_Info_y])
                if(len(self.BC_Info) == 0):
                    self.BC_Info = Border_Info
                else:
                    self.BC_Info = np.vstack([self.BC_Info, Border_Info])
        
        
        index_shared = len(Shared_Borders)
        for index in range(len(self.BC_Info)):
            self.BC_Info[index,-2] = index_shared +index
            if(index_shared +index == 0):
                Shared_Borders = self.BC_Info[index,:]
            else:
                Shared_Borders = np.vstack([Shared_Borders, self.BC_Info[index,:]])
        
        return Shared_Borders
    
    def Update_BC(self,Shared_Borders):
        for index in range(len(self.BC_Info)):
            self.BC_Values[int(self.BC_Info[index,2])] = Shared_Borders[int(self.BC_Info[index,-2]), -1]
    
    def Update_Shared_Borders(self,model,Shared_Borders):
        Input_BC = np.zeros((int(len(self.Inner_Borders)/2), 8*(self.Points_Border -1)))
        Input_BC[:] = self.BC_Values
        Input = [Input_BC,self.Inner_Borders[::2,0:2]]
        Predictions = (model.predict(Input)[:,0:2]).flatten()
        Convergence = np.mean(np.square(self.Inner_Borders[:,2] - Predictions))
        self.Inner_Borders[:,2] = Predictions
        for index in range(len(self.Inner_Borders)):
            Shared_Borders[int(self.Inner_Borders[index,-1]), -1] = self.Inner_Borders[index,2]
        return Shared_Borders, Convergence
    
    def Update_Domain(self, model, Domain):
        Input_BC = np.zeros((self.Points_Border*self.Points_Border, 8*(self.Points_Border-1)))
        Input_BC[:] = self.BC_Values
        Input = [Input_BC,self.Inner_Points_Grid]
        Output = np.reshape((model.predict(Input)).flatten(), (self.Points_Border,self.Points_Border,3))
        Domain[self.i0:(self.i0+self.Points_Border), self.j0:(self.j0 + self.Points_Border)] = Output[:,:]
        return Domain
    
    def Update_Domain_Mean(self, model, Domain, Aux):
        Input_BC = np.zeros((self.Points_Border*self.Points_Border, 8*(self.Points_Border-1)))
        Input_BC[:] = self.BC_Values
        Input = [Input_BC,self.Inner_Points_Grid]
        Output = np.reshape((model.predict(Input)).flatten(), (self.Points_Border,self.Points_Border,3))
        Domain[self.i0:(self.i0+self.Points_Border), self.j0:(self.j0 + self.Points_Border)] += Output[:,:]
        Aux[self.i0:(self.i0+self.Points_Border), self.j0:(self.j0 + self.Points_Border)] += 1
        return Domain, Aux
        
    def Include_Inner_Border_Points(self,Shared_Borders):
        
        for element in Shared_Borders:
            Y = element[0]
            X = element[1]
            Value = 0
            index_shared = element[-2]
            if(X > self.x0_global and X < (self.x0_global + self.width) and Y > self.y0_global and Y < self.y0_global +self.width):
                x_r = X - self.x0_global
                y_r = Y - self.y0_global
                Point_Info = [y_r,x_r, Value, index_shared]
                if(len(self.Inner_Borders) == 0):
                    self.Inner_Borders = Point_Info
                else:
                    self.Inner_Borders = np.vstack([self.Inner_Borders, Point_Info])
    

def Iterative_Solution_Optimized(Shared_Borders, Gnomes, Model, True_Solution, Iterations = 300):
    Number_Gnomes_Optimize = len(Gnomes)
    Convergences = []
    if len(Gnomes) == 1:
        Solution = np.zeros_like(True_Solution)
        Solution = Gnomes[0].Update_Domain(Model, Solution)
    else:   
        for iteration in range(Iterations):
            print("Iteration = ", iteration, "--------" )
            Convergence = 0
            for index, Gnome in enumerate(Gnomes):
                if(index%10 == 0):
                    print("Predicting Genome ", index, "of ", len(Gnomes), "Total Genomes")
                
                Gnome.Update_BC(Shared_Borders)
                Shared_Borders, Convergence_1 = Gnome.Update_Shared_Borders(Model, Shared_Borders)            
                Convergence = max(Convergence,Convergence_1)
                
                
                
            #Convergence = Convergence/Number_Gnomes_Optimize
            Convergences.append(Convergence)
            if(Convergence < 1e-10):
                break
        #Final iteration for obtaining the solution
        Solution = np.zeros_like(True_Solution)
        Aux = np.zeros_like(True_Solution)
         
        for index, Gnome in enumerate(Gnomes):
            if(index%10 == 0):
                print("Solving Genome ", index, "of ", len(Gnomes), "Total Genomes")
            Gnome.Update_BC(Shared_Borders)
            Solution, Aux = Gnome.Update_Domain_Mean(Model, Solution, Aux)
            #Solution = Gnome.Update_Domain(Model, Solution)
        Solution = Solution/(Aux) 
    
    # Compute the metrics
    
    Number_of_Pixels = 0
    for i in range(len(Solution[1:-1,0,0])):
        for j in range(len(Solution[0,1:-1,0])):
            if(not np.isnan(Solution[i,j,0])):
                Number_of_Pixels +=1 
    MAE_U_x = np.sum(np.abs(np.nan_to_num(Solution[1:-1,1:-1,0])-np.nan_to_num(True_Solution[1:-1,1:-1,0]))) / Number_of_Pixels
    MAE_U_y = np.sum(np.abs(np.nan_to_num(Solution[1:-1,1:-1,1])-np.nan_to_num(True_Solution[1:-1,1:-1,1]))) / Number_of_Pixels
    MAE_P = np.sum(np.abs(np.nan_to_num(Solution[1:-1,1:-1,2])-np.nan_to_num(True_Solution[1:-1,1:-1,2]))) / Number_of_Pixels
    
    return MAE_U_x, MAE_U_y, MAE_P, Convergences, Solution

def Generate_Genomes_Automatically(Width, Height, Genome_width, Points_Borders, Middle_Overlapping, Domain_Interior_Ones,
                                   Domain_Int_Nan, Coords , Finner_Grid = []):
    Gnomes = []
    Shared_Borders = []
    Number_Gnomes_x = int(Width/Genome_width)
    Number_Gnomes_y = int(Height/Genome_width)
    #print(Number_Gnomes_x)
    #print(Number_Gnomes_y)
    
    #Set the regular gnomes    
    for x in range(Number_Gnomes_x):
        for y in range(Number_Gnomes_y):
            x0_global = x*Genome_width
            y0_global = y*Genome_width
            i0_global = int(x0_global*(Points_Borders-1)/Genome_width)
            j0_global = int(y0_global*(Points_Borders-1)/Genome_width)
            if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
               Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                Shared_Borders = Genome_element.Set_Borders_New_Definition(Shared_Borders,p = Domain_Int_Nan, Coords = Coords)
                Gnomes.append(Genome_element)

            
    # Set the overlapping Gnomes in the horizontal line
    for x in range(Number_Gnomes_x):
        for y in range(Number_Gnomes_y -1):
            x0_global = x*Genome_width
            y0_global = y*Genome_width + Genome_width/2
            i0_global = int(x0_global*(Points_Borders-1)/Genome_width)
            j0_global = int(y0_global*(Points_Borders-1)/Genome_width)
            if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
               Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                Shared_Borders = Genome_element.Set_Borders_New_Definition(Shared_Borders,p = Domain_Int_Nan, Coords = Coords)
                Gnomes.append(Genome_element)
                
    # Set the overlapping Gnomes in the vertical line
    for x in range(Number_Gnomes_x- 1):
        for y in range(Number_Gnomes_y):
            x0_global = x*Genome_width  + Genome_width/2
            y0_global = y*Genome_width
            i0_global = int(x0_global*(Points_Borders-1)/Genome_width)
            j0_global = int(y0_global*(Points_Borders-1)/Genome_width)
            if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
               Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                Shared_Borders = Genome_element.Set_Borders_New_Definition(Shared_Borders,p = Domain_Int_Nan, Coords = Coords)
                Gnomes.append(Genome_element)
                
   # Set the middle overlapping
    if(Middle_Overlapping):
        # Set the overlapping Gnomes in the vertical line
        for x in range(Number_Gnomes_x -1):
            for y in range(Number_Gnomes_y -1):
                x0_global = x*Genome_width  + Genome_width/2
                y0_global = y*Genome_width + Genome_width/2
                i0_global = int(x0_global*(Points_Borders-1)/Genome_width)
                j0_global = int(y0_global*(Points_Borders-1)/Genome_width)
                if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
                   Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
                   Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
                   Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                    Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                    Shared_Borders = Genome_element.Set_Borders_New_Definition(Shared_Borders,p = Domain_Int_Nan, Coords = Coords)
                    Gnomes.append(Genome_element)
    # Set the middle overlapping
    if(len(Finner_Grid) > 0 ):
        for element in Finner_Grid: 
            for x in range(Number_Gnomes_x):
                for y in range(Number_Gnomes_y -1):
                    x0_global = x*Genome_width
                    y0_global = y*Genome_width + Genome_width/element
                    i0_global = int(x0_global*(Points_Borders-1)/Genome_width)
                    j0_global = int(y0_global*(Points_Borders-1)/Genome_width)
                    if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
                       Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                        Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                        Shared_Borders = Genome_element.Set_Borders_New_Definition(Shared_Borders,p = Domain_Int_Nan, Coords = Coords)
                        Gnomes.append(Genome_element)
            # Set the overlapping Gnomes in the vertical line
            for x in range(Number_Gnomes_x- 1):
                for y in range(Number_Gnomes_y):
                    x0_global = x*Genome_width  + Genome_width/element
                    y0_global = y*Genome_width
                    i0_global = int(x0_global*(Points_Borders-1)/Genome_width)
                    j0_global = int(y0_global*(Points_Borders-1)/Genome_width)
                    if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
                       Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                        Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                        Shared_Borders = Genome_element.Set_Borders_New_Definition(Shared_Borders,p = Domain_Int_Nan, Coords = Coords)
                        Gnomes.append(Genome_element)
            # Set the overlapping Gnomes in the vertical line
            for x in range(Number_Gnomes_x -1):
                for y in range(Number_Gnomes_y -1):
                    x0_global = x*Genome_width  + Genome_width/element
                    y0_global = y*Genome_width + Genome_width/element
                    i0_global = int(x0_global*(Points_Borders-1)/Genome_width)
                    j0_global = int(y0_global*(Points_Borders-1)/Genome_width)
                    if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
                       Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                        Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                        Shared_Borders = Genome_element.Set_Borders_New_Definition(Shared_Borders,p = Domain_Int_Nan, Coords = Coords)
                        Gnomes.append(Genome_element)
    
        for element in Finner_Grid: 
            for x in range(Number_Gnomes_x):
                for y in range(1,Number_Gnomes_y):
                    x0_global = x*Genome_width
                    y0_global = y*Genome_width - Genome_width/element
                    print(y0_global)
                    i0_global = int(x0_global*(Points_Borders-1)/Genome_width)
                    j0_global = int(y0_global*(Points_Borders-1)/Genome_width)
                    if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
                       Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                        Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                        Shared_Borders = Genome_element.Set_Borders_New_Definition(Shared_Borders,p = Domain_Int_Nan, Coords = Coords)
                        Gnomes.append(Genome_element)
            # Set the overlapping Gnomes in the vertical line
            for x in range(1, Number_Gnomes_x):
                for y in range(Number_Gnomes_y):
                    x0_global = x*Genome_width  - Genome_width/element
                    y0_global = y*Genome_width
                    i0_global = int(x0_global*(Points_Borders-1)/Genome_width)
                    j0_global = int(y0_global*(Points_Borders-1)/Genome_width)
                    if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
                       Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                        Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                        Shared_Borders = Genome_element.Set_Borders_New_Definition(Shared_Borders,p = Domain_Int_Nan, Coords = Coords)
                        Gnomes.append(Genome_element)
            # Set the overlapping Gnomes in the vertical line
            for x in range(1,Number_Gnomes_x):
                for y in range(1, Number_Gnomes_y):
                    x0_global = x*Genome_width  - Genome_width/element
                    y0_global = y*Genome_width - Genome_width/element
                    i0_global = int(x0_global*(Points_Borders-1)/Genome_width)
                    j0_global = int(y0_global*(Points_Borders-1)/Genome_width)
                    if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
                       Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                        Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                        Shared_Borders = Genome_element.Set_Borders_New_Definition(Shared_Borders,p = Domain_Int_Nan, Coords = Coords)
                        Gnomes.append(Genome_element)
                        
    print("A total of ", len(Gnomes), " genomes are being used")
    for index,Gnome in enumerate(Gnomes):
        if(index%10 == 0):
            print("Gather information from borders inside the Genome", index + 1, "of ", len(Gnomes), "Total Genomes")
        Gnome.Include_Inner_Border_Points(Shared_Borders)
    return Gnomes, Shared_Borders




