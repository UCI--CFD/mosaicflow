import numpy as np
from block import *
import copy as cp

class GenomeMap:
  def __init__(self, block, blockRange, genomeRange):
    self.block = block
    self.bRng  = blockRange
    self.gRng  = genomeRange


class Genome:
  def __init__(self, shape=(32,32), gSizej=32, gSizei=32, \
               nVar=3, nCoord=2, Re=500):
    self.nCoord = nCoord
    self.nVar   = nVar
    # map between blocks' data range to genome's.
    self.maps      = []
    # map for inference ranges
    self.inferMaps = []
    # allocate data extracted from blocks
    self.shape     = shape
    nBcPoint       = 2*(shape[0] + shape[1] - 2)
    self.coords    = np.zeros((shape[0], shape[1], nCoord))
    self.vars      = np.zeros((shape[0], shape[1], nVar))
    self.coordsMin = np.zeros(nCoord)
    self.pMin      = 0.0
    self.pBase     = 0.0
    self.bc        = np.zeros(nBcPoint * 2) # only u, v
    # Reynolds
    self.Re = Re


  def add_map(self, block, blockRange, genomeRange):
    self.maps.append(GenomeMap(block, blockRange, genomeRange))


  def add_infer_map(self, blkID, bRng, gRng):
    self.inferMaps.append(GenomeMap(blkID, bRng, gRng))


  def create_nn_input(self, separateRe=False):
    assert len(self.inferMaps) == 1
    m = self.inferMaps[0]
    batchSize = (m.bRng[2]-m.bRng[0]) * (m.bRng[3]-m.bRng[1])
    nBcPoint  = 2 * (self.shape[0] + self.shape[1] - 2)
    bc = np.zeros((batchSize, nBcPoint*2))
    xy = np.zeros((batchSize, 2))

    # broadcast/duplicate the values
    for i in range(batchSize):
      bc[i, :] = self.bc
    # set data points
    xy[:, :] = np.reshape(\
                 self.coords[m.gRng[0]:m.gRng[2], m.gRng[1]:m.gRng[3], :],\
                 (batchSize, 2))

    return [bc, xy]


  def extract_from_block(self, blocks, s=1, shiftP=False, \
                         orderByVar=False):
    # flowVars and coordinates shall be list of 2D numpy arrays
    for m in self.maps:
      # copy variables
      self.vars[m.gRng[0]:m.gRng[2], m.gRng[1]:m.gRng[3], :] = \
        blocks[m.block].vars[m.bRng[0]:m.bRng[2]:s, \
                             m.bRng[1]:m.bRng[3]:s,:].copy()
      # copy coordinates
      self.coords[m.gRng[0]:m.gRng[2], m.gRng[1]:m.gRng[3], :] = \
        blocks[m.block].coords[m.bRng[0]:m.bRng[2]:s, \
                               m.bRng[1]:m.bRng[3]:s,:].copy()
    # shift coordinates to start from (0,0)
    for i in range(self.nCoord):
      self.coordsMin[i] = np.amin(self.coords[:,:,i])
      self.coords[:,:,i] -= self.coordsMin[i]
    # # save and substract min pressure
    # self.pMin = np.amin(self.vars[:,:,2])
    # self.vars[:,:,2] -= self.pMin
    # substract left corner pressure
    self.pBase = self.vars[0, 0, 2]
    if shiftP:
      self.vars[:,:,2] -= self.pBase
    # set bc
    s, e = 0, 0
    if orderByVar:
      for i in range(2):
        s, e = e, e + (self.shape[1]-1)
        self.bc[s:e]  = self.vars[0,:-1,i].flatten()
        s, e = e, e + (self.shape[0]-1)
        self.bc[s:e]  = self.vars[:-1, -1, i].flatten()
        s, e = e, e + (self.shape[1]-1)
        self.bc[s:e]  = self.vars[-1, -1:0:-1, i].flatten()
        s, e = e, e + (self.shape[0]-1)
        self.bc[s:e]  = self.vars[-1:0:-1, 0, i].flatten()
    else:
      s, e = e, e + 2*(self.shape[1]-1)
      self.bc[s:e]  = self.vars[0,:-1,:2].flatten()
      s, e = e, e + 2*(self.shape[0]-1)
      self.bc[s:e]  = self.vars[:-1, -1, :2].flatten()
      s, e = e, e + 2*(self.shape[1]-1)
      self.bc[s:e]  = self.vars[-1, -1:0:-1, :2].flatten()
      s, e = e, e + 2*(self.shape[0]-1)
      self.bc[s:e]  = self.vars[-1:0:-1, 0, :2].flatten()


  def predict(self, nn):
    inputs = self.create_nn_input()
    self.vars[1:-1,1:-1,:] = np.reshape(nn.predict(inputs),\
                                        (self.shape[0]-2, self.shape[1]-2, 3))


  def copy_to_block(self, blocks):
    assert len(self.inferMaps) == 1
    for m in self.inferMaps:
      blocks[m.block].vars[m.bRng[0]:m.bRng[2], m.bRng[1]:m.bRng[3],:2] = \
        self.vars[m.gRng[0]:m.gRng[2], m.gRng[1]:m.gRng[3], :2].copy()
      blocks[m.block].vars[m.bRng[0]:m.bRng[2], m.bRng[1]:m.bRng[3],2] = \
        self.vars[m.gRng[0]:m.gRng[2], m.gRng[1]:m.gRng[3], 2].copy() + self.pBase

  def inner_xy(self):
    xy = self.coords.copy()
    xy[:,:,0] += self.coordsMin[0]
    xy[:,:,1] += self.coordsMin[1]
    return xy[1:-1,1:-1,:]

  def inner_value(self):
    val = self.vars.copy()
    val[:,:,2] += self.pBase
    return val[1:-1,1:-1,:]

  def compute_mass_flow(self):
    # j- in, j+ out, i- in, i+ out
    flow = np.sum(self.vars[1:,0,0]  + self.vars[:-1,0,0]) \
         - np.sum(self.vars[1:,-1,0] + self.vars[:-1,-1,0]) \
         + np.sum(self.vars[0,1:,1]  + self.vars[0,:-1,1]) \
         - np.sum(self.vars[-1,1:,1] + self.vars[-1,:-1,1])
    return 0.5*flow
