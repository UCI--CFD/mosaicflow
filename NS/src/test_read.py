import numpy as np
import vtk
from vtk.util.numpy_support import vtk_to_numpy

#fName = '../cavity_vtk/re100_1493.vtk'
#nPointi, nPointj  = 129, 129
fName = '../channel_vtk/re1000_182.vtk'
nPointi, nPointj  = 65, 257
reader = vtk.vtkUnstructuredGridReader()
reader.SetFileName(fName)
reader.Update()
vtkData = reader.GetOutput()

pointData = vtkData.GetPointData()
arr = vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
print(arr.shape)
arr = arr[:nPointi*nPointj].reshape((nPointi, nPointj))
print(arr[0:2,0:2])
print(np.amin(arr), np.amax(arr))
#for i in range(pointData.GetNumberOfArrays()):
  #arrName = pointData.GetArrayName(i)
  #print(arrName, end=' ')
  #arr = vtk_to_numpy(vtkData.GetPointData().GetArray(arrName))
  #print(arr.shape)

#cellData = vtkData.GetCellData()
#for i in range(cellData.GetNumberOfArrays()):
  #arrName = cellData.GetArrayName(i)
  #print(arrName, end=' ')
  #arr = vtk_to_numpy(vtkData.GetCellData().GetArray(arrName))
  #print(arr.shape)
