import h5py as h5
import numpy as np
import tensorflow as tf
from tensorflow import keras
from NS_compute_block import *

strategy = tf.distribute.MirroredStrategy()


class NSModelDataOnly(keras.Model):
  def __init__(self, alpha=[1.0, 1.0, 1.0], saveGradStat=False, **kwargs):
    super(NSModelDataOnly, self).__init__(**kwargs)
    self.alpha = alpha

    # ---- dicts for metrics and statistics ---- #
    # save gradients' statistics per layer
    self.saveGradStat = saveGradStat
    # create metrics
    self.trainMetrics = {}
    self.validMetrics = {}
    # add metrics
    for key in ['loss', 'uMse', 'vMse', 'pMse']:
      self.trainMetrics[key] = keras.metrics.Mean(name='train_'+key)
      self.validMetrics[key] = keras.metrics.Mean(name='valid_'+key)
    self.trainMetrics['rMse'] = keras.metrics.Mean(name='train_rMse')
    for key in ['uMae', 'vMae', 'pMae']:
      self.trainMetrics[key] = keras.metrics.MeanAbsoluteError(name='train_'+key)
      self.validMetrics[key] = keras.metrics.MeanAbsoluteError(name='valid_'+key)
    ## add metrics for layers' weights, if save_grad_stat is required
    ## i even for weights, odd for bias
    if self.saveGradStat:
      for i in range(len(width)):
        for prefix in ['u_', 'v_', 'p_',]:
          for suffix in ['w_avg', 'w_std', 'b_avg', 'b_std']:
            key = prefix + repr(i) + suffix
            self.trainMetrics[key] = keras.metrics.Mean(name='train '+key)
    # statistics
    self.trainStat = {}
    self.validStat = {}


  def call(self, inputs):
    return inputs


  def train_step(self, data):
    inputs = data[0]
    uvp    = data[1]
    with tf.GradientTape(persistent=True) as tape0:
      uvpPred = self(inputs)
      uMse    = tf.reduce_mean(tf.square(uvpPred[:,0] - uvp[:,0]))
      vMse    = tf.reduce_mean(tf.square(uvpPred[:,1] - uvp[:,1]))
      pMse    = tf.reduce_mean(tf.square(uvpPred[:,2] - uvp[:,2]))
      rMse    = tf.add_n(self.losses)
      loss    = ( self.alpha[0]*uMse + self.alpha[1]*vMse
                + self.alpha[2]*pMse + rMse)
      loss    = loss / strategy.num_replicas_in_sync
    # update gradients and trainable variables
    if self.saveGradStat:
      uMseGrad    = tape0.gradient(uMse, self.trainable_variables)
      vMseGrad    = tape0.gradient(vMse, self.trainable_variables)
      pMseGrad    = tape0.gradient(pMse, self.trainable_variables)
    lossGrad = tape0.gradient(loss, self.trainable_variables)
    del tape0
    self.optimizer.apply_gradients(zip(lossGrad, self.trainable_variables))
    # track loss and mae
    self.trainMetrics['loss'].update_state(loss*strategy.num_replicas_in_sync)
    self.trainMetrics['uMse'].update_state(uMse)
    self.trainMetrics['vMse'].update_state(vMse)
    self.trainMetrics['pMse'].update_state(pMse)
    self.trainMetrics['rMse'].update_state(rMse)
    self.trainMetrics['uMae'].update_state(uvpPred[:,0], uvp[:,0])
    self.trainMetrics['vMae'].update_state(uvpPred[:,1], uvp[:,1])
    self.trainMetrics['pMae'].update_state(uvpPred[:,2], uvp[:,2])
    # track gradients coefficients
    if self.saveGradStat:
      self.record_layer_gradient(uMseGrad, 'u_')
      self.record_layer_gradient(vMseGrad, 'v_')
      self.record_layer_gradient(pMseGrad, 'p_')
    for key in self.trainMetrics:
      self.trainStat[key] = self.trainMetrics[key].result()

    return self.trainStat


  def test_step(self, data):
    inputs  = data[0]
    uvp     = data[1]
    # predict
    uvpPred = self(inputs)
    # update loss
    uMse    = tf.reduce_mean(tf.square(uvpPred[:,0] - uvp[:,0]))
    vMse    = tf.reduce_mean(tf.square(uvpPred[:,1] - uvp[:,1]))
    pMse    = tf.reduce_mean(tf.square(uvpPred[:,2] - uvp[:,2]))
    loss    = (self.alpha[0]*uMse + self.alpha[1]*vMse + self.alpha[2]*pMse)
    # track loss and mae
    self.validMetrics['loss'].update_state(loss)
    self.validMetrics['uMse'].update_state(uMse)
    self.validMetrics['vMse'].update_state(vMse)
    self.validMetrics['pMse'].update_state(pMse)
    self.validMetrics['uMae'].update_state(uvpPred[:,0], uvp[:,0])
    self.validMetrics['vMae'].update_state(uvpPred[:,1], uvp[:,1])
    self.validMetrics['pMae'].update_state(uvpPred[:,2], uvp[:,2])
    for key in self.validMetrics:
      self.validStat[key] = self.validMetrics[key].result()
    #
    return self.validStat


  def reset_metrics(self):
    for key in self.trainMetrics:
      self.trainMetrics[key].reset_states()
    for key in self.validMetrics:
      self.validMetrics[key].reset_states()


  def summary(self):
    nVar = 0
    for t in self.trainable_variables:
      print(t.name, t.shape)
      nVar += tf.reduce_prod(t.shape)
    print('{} trainalbe variables'.format(nVar))


  def record_layer_gradient(self, grads, baseName):
    '''
    record the average and standard deviation of each layer's
    weights and biases
    '''
    for i, g in enumerate(grads):
      if g != None:
        l = i // 2
        parameter = 'w' if i%2==0 else 'b'
        prefix = baseName + '_{:d}{}_'.format(l,parameter)
        gAbs = tf.abs(g)
        gAvg = tf.reduce_mean(gAbs)
        gStd = tf.reduce_mean(tf.square(gAbs - gAvg))
        self.trainMetrics[prefix+'avg'].update_state(gAvg)
        self.trainMetrics[prefix+'std'].update_state(gStd)


  def gradient_bc(self, bc, xy):
    '''
    bc - 1D array of all boundary values (u, v, p)
    xy - [batch, 2], including all points to infer
    '''
    xy      = tf.convert_to_tensor(xy)
    # bc      = tf.convert_to_tensor(bc)
    bcTiled = tf.convert_to_tensor(bc)
    bcTiled = tf.expand_dims(bcTiled, 0)
    bcTiled = tf.tile(bcTiled, (xy.shape[0], 1))

    with tf.GradientTape(watch_accessed_variables=False,\
                         persistent=True) as tape:
      tape.watch(bcTiled)
      uvp = self([bcTiled, xy])
      u, v, p = uvp[:,0], uvp[:,1], uvp[:,2]
    u_bc = tape.gradient(u, bcTiled)
    v_bc = tape.gradient(v, bcTiled)
    p_bc = tape.gradient(p, bcTiled)
    del tape
    uvp_bc = tf.stack([u_bc, v_bc, p_bc], 1)
    return uvp_bc.numpy()


class NSModelMLP(NSModelDataOnly):
  '''
  Feed-Forward Model takes in the coordinates and variables on boundary,
  Re, and the collocation point as input, outputs (u, v, p)
  '''
  def __init__(self, width=[256, 256, 256, 128, 128, 128, 64, 32, 3],\
               reg=None, lastLinear = False, **kwargs):
    super(NSModelMLP, self).__init__(**kwargs)
    self.width = width
    self.reg   = reg
    if reg == None:
      self.mlp = DenseLayers(width=width, prefix='bc', \
                             last_linear=lastLinear)
    else:
      self.mlp = DenseLayers(width=width, reg=reg, prefix='bc', \
                             last_linear=lastLinear)

  def call(self, inputs):
    '''
    inputs: [bc, xy]
    '''
    bcXybcReXy = tf.concat(inputs, axis=-1)
    return self.mlp(bcXybcReXy)


  def preview(self):
    print('--------------------------------')
    print('model preview')
    print('--------------------------------')
    print('fully connected network:')
    print(self.width)
    print('layer regularization')
    print(self.reg)
    print('coefficients for data loss {} {} {}'.format(\
          self.alpha[0], self.alpha[1], self.alpha[2]))
    print('--------------------------------')


#---------------------------------------------------------------
# DeepONet model
#---------------------------------------------------------------
class NSModelDeepONet(NSModelDataOnly):
  '''
  Using DeepONet model from https://arxiv.org/abs/2103.10974. 
  For now this is data only, PINN will be added later.
  '''
  def __init__(self, branch=[], trunk=[], branchReg=None, \
               trunkReg=None, LastLinear=False, **kwargs):
    super(NSModelDeepONet, self).__init__(**kwargs)
    # set # neurons for each layer
    assert len(branch) > 0
    self.branchWidth = branch
    if (len(trunk) == 0):
      self.trunkWidth = [i for i in branch]
    else:
      self.trunkWidth = trunk
    self.branchNet = DenseLayers(width=self.branchWidth, prefix='branch')
    self.trunkNet  = DenseLayers(width=self.trunkWidth,  prefix='trunk')

  def call(self, inputs):
    '''
    inputs: [bc, xy]
    '''
    # the output of branch net is u0, v0, p0, u1, v1, p1, ...
    # then reshaped to 3xN, 
    branchOutput = self.branchNet(inputs[0])
    branchOutput = tf.reshape(branchOutput, [tf.shape(branchOutput)[0], -1, 3])
    trunkOutput  = self.trunkNet(inputs[1])
    trunkOutput  = tf.reshape(trunkOutput, [tf.shape(trunkOutput)[0], -1, 3])
    return tf.reduce_sum(branchOutput * trunkOutput, axis=1)

  def preview(self):
    print('--------------------------------')
    print('model preview')
    print('--------------------------------')
    print('branch network:')
    print(self.branchWidth)
    print('trunk network:')
    print(self.trunkWidth)
    print('coefficients for data loss {} {} {}'.format(\
          self.alpha[0], self.alpha[1], self.alpha[2]))
    print('--------------------------------')


#---------------------------------------------------------------
# Model that imposes BC
#---------------------------------------------------------------
class NSModelHardBC(NSModelMLP):
  '''
  Separate the original MLP into,
    g + d*f,
  g - a function satisfies the BC accurately
  d - a function equals zero at boundary and stickly non-zero elsewhere
  u - a network which takes in bc + (x,y) predicts the solution
  '''
  def __init__(self, nBcPoint=128, edgeLen=0.5, omitP=True, **kwargs):
    super(NSModelHardBC, self).__init__(**kwargs)
    # inputs
    self.nBcVar = 2 if omitP else 3  # number of var of bc point
    self.nBcPnt = nBcPoint

    # set the coordinates of boundary
    nEPnt = nBcPoint // 4    # number of points per edge
    h     = edgeLen / nEPnt
    self.xyBc = np.zeros((nBcPoint, 2))
    for i in range(nEPnt):
      self.xyBc[i, :]         = [i*h, 0.0]              # lower edge
      self.xyBc[nEPnt+i, :]   = [edgeLen, i*h]          # right edge
      self.xyBc[2*nEPnt+i, :] = [edgeLen-i*h, edgeLen]  # upper edge
      self.xyBc[3*nEPnt+i, :] = [0.0, edgeLen-i*h]      # left  edge
    # convert to tensor with precision 
    precision = keras.backend.floatx()
    if precision == 'float64':
      self.xyBc = tf.convert_to_tensor(self.xyBc, dtype=tf.float64)
    elif precision == 'float32':
      self.xyBc = tf.convert_to_tensor(self.xyBc, dtype=tf.float32)


  def call(self, inputs):
    bc, xy = inputs[0], inputs[1]
    # compute the distance between xy to every bc point (xyBc)
    # match the number of dimensions
    xyBc = tf.expand_dims(self.xyBc, axis=0)
    xy   = tf.expand_dims(xy,        axis=1)
    # The substraction broadcasts them to the same shape
    # The "distance" has shape [batch, nBcPoint], it is without sqrt
    eps  = 1e-14
    dist = tf.reduce_sum(tf.square(xy - xyBc), axis=-1)
    # coefficients in averaing bc
    coef = 1.0 / (dist + eps)
    coef = coef / tf.reduce_sum(coef, axis=-1, keepdims=True)
    # compute the function g to impose bc
    # coef is broadcasted to bc' shape, g's shape [batch, nBcVar]
    bc   = tf.reshape(bc, (-1, self.nBcPnt, self.nBcVar))
    coef = tf.expand_dims(coef, axis=-1)
    g    = tf.reduce_sum(coef * bc, axis=1)
    # tf.print("g ", tf.reduce_sum(tf.square(g)))

    # function d filters the network's effect on boundary
    # d's shape [batch, 1]
    xy   = inputs[1]
    x, y = xy[:,0], xy[:,1]
    d    = tf.expand_dims(x*(0.5-x)*y*(0.5-y), axis=-1)

    # network, d is broadcasted to uvp's shape, [batch, 3]
    bcXy = tf.concat([inputs[0], inputs[1]], axis=-1)
    uvp  = self.mlp(bcXy)

    # g + d*f, notet that g and f may have different shape
    # when p is not input
    uv, p = uvp[:,:2], uvp[:,2]
    p     = tf.expand_dims(p, axis=-1)
    uv    = g + d*uv
    uvp   = tf.concat([uv, p], axis=-1)
    
    return uvp


#---------------------------------------------------------------
# PINN model
#---------------------------------------------------------------
class NSModelPinn(keras.Model):
  def __init__(self, width=[256, 256, 256, 128, 128, 128, 64, 32, 3],\
               alpha = [1.0, 1.0, 1.0], beta = [1e-4, 1e-4, 1e-4], \
               reg=None, saveGradStat=False, **kwargs):
    super(NSModelPinn, self).__init__(**kwargs)
    self.width = width
    self.reg   = reg
    if reg == None:
      self.mlp = DenseLayers(width=width, prefix='bc')
    else:
      self.mlp = DenseLayers(width=width, reg=reg, prefix='bc')
    # coefficient for data and pde loss
    self.alpha = alpha
    self.beta  = beta

    # ---- dicts for metrics and statistics ---- #
    # save gradients' statistics per layer
    self.saveGradStat = saveGradStat
    # create metrics
    self.trainMetrics = {}
    self.validMetrics = {}
    # add metrics
    names = ['loss', 'uMse', 'vMse', 'pMse', 'pde0', 'pde1', 'pde2', \
             'uMae', 'vMae', 'pMae']
    for key in names:
      self.trainMetrics[key] = keras.metrics.Mean(name='train_'+key)
      self.validMetrics[key] = keras.metrics.Mean(name='valid_'+key)
    ## add metrics for layers' weights, if save_grad_stat is required
    ## i even for weights, odd for bias
    if self.saveGradStat:
      for i in range(len(width)):
        for prefix in ['u_', 'v_', 'p_', 'pde0_', 'pde1_', 'pde2_']:
          for suffix in ['w_avg', 'w_std', 'b_avg', 'b_std']:
            key = prefix + repr(i) + suffix
            self.trainMetrics[key] = keras.metrics.Mean(name='train '+key)
    # statistics
    self.trainStat = {}
    self.validStat = {}


  def call(self, inputs):
    '''
    inputs: [bc, xy, w]
    '''
    bcXy = tf.concat([inputs[0], inputs[1]], axis=-1)
    return self.mlp(bcXy)


  def record_layer_gradient(self, grads, baseName):
    '''
    record the average and standard deviation of each layer's
    weights and biases
    '''
    for i, g in enumerate(grads):
      if g != None:
        l = i // 2
        parameter = 'w' if i%2==0 else 'b'
        prefix = baseName + '_{:d}{}_'.format(l,parameter)
        gAbs = tf.abs(g)
        gAvg = tf.reduce_mean(gAbs)
        gStd = tf.reduce_mean(tf.square(gAbs - gAvg))
        self.trainMetrics[prefix+'avg'].update_state(gAvg)
        self.trainMetrics[prefix+'std'].update_state(gStd)


  def compute_data_pde_losses(self, bc, xy, w, uvp):
    # track computation for 2nd derivatives for u, v, p
    with tf.GradientTape(watch_accessed_variables=False,persistent=True) as tape2:
      tape2.watch(xy)
      with tf.GradientTape(watch_accessed_variables=False,persistent=True) as tape1:
        tape1.watch(xy)
        uvpPred = self([bc, xy, w])
        u       = uvpPred[:,0]
        v       = uvpPred[:,1]
        p       = uvpPred[:,2]
      # 1st order derivatives
      u_grad   = tape1.gradient(u, xy)
      v_grad   = tape1.gradient(v, xy)
      p_grad   = tape1.gradient(p, xy)
      u_x, u_y = u_grad[:,0], u_grad[:,1]
      v_x, v_y = v_grad[:,0], v_grad[:,1]
      p_x, p_y = p_grad[:,0], p_grad[:,1]
      del tape1
    # 2nd order derivatives
    u_xx = tape2.gradient(u_x, xy)[:,0]
    u_yy = tape2.gradient(u_y, xy)[:,1]
    v_xx = tape2.gradient(v_x, xy)[:,0]
    v_yy = tape2.gradient(v_y, xy)[:,1]
    del tape2

    # compute data loss
    w = tf.squeeze(w)
    nDataPoint = tf.reduce_sum(w) + 1.0e-10
    uMse  = tf.reduce_sum(w * tf.square(u - uvp[:,0])) / nDataPoint
    vMse  = tf.reduce_sum(w * tf.square(v - uvp[:,1])) / nDataPoint
    pMse  = tf.reduce_sum(w * tf.square(p - uvp[:,2])) / nDataPoint
    # pde error, 0 continuity, 1-2 NS
    ww      = 1.0 - w
    nPdePoint = tf.reduce_sum(ww) + 1.0e-10
    pde0    = u_x + v_y
    pde1    = u*u_x + v*u_y + p_x - (u_xx + u_yy)/500.0
    pde2    = u*v_x + v*v_y + p_y - (v_xx + v_yy)/500.0
    pdeMse0 = tf.reduce_sum(tf.square(pde0) * ww) / nPdePoint
    pdeMse1 = tf.reduce_sum(tf.square(pde1) * ww) / nPdePoint
    pdeMse2 = tf.reduce_sum(tf.square(pde2) * ww) / nPdePoint

    return uvpPred, uMse, vMse, pMse, pdeMse0, pdeMse1, pdeMse2


  def train_step(self, data):
    bc  = data[0][0]
    xy  = data[0][1]
    w   = data[0][2]
    uvp = data[1]
    with tf.GradientTape(persistent=True) as tape0:
      # compute the data loss for u, v, p and pde losses for
      # continuity (0) and NS (1-2)
      uvpPred, uMse, vMse, pMse, pdeMse0, pdeMse1, pdeMse2 = \
        self.compute_data_pde_losses(bc, xy, w, uvp)
      # replica's loss, divided by global batch size
      loss  = ( self.alpha[0]*uMse   + self.alpha[1]*vMse   + self.alpha[2]*pMse \
              + self.beta[0]*pdeMse0 + self.beta[1]*pdeMse1 + self.beta[2]*pdeMse2)
      loss += tf.add_n(self.losses)
      loss  = loss / strategy.num_replicas_in_sync
    # update gradients
    if self.saveGradStat:
      uMseGrad    = tape0.gradient(uMse,    self.trainable_variables)
      vMseGrad    = tape0.gradient(vMse,    self.trainable_variables)
      pMseGrad    = tape0.gradient(pMse,    self.trainable_variables)
      pdeMse0Grad = tape0.gradient(pdeMse0, self.trainable_variables)
      pdeMse1Grad = tape0.gradient(pdeMse1, self.trainable_variables)
      pdeMse2Grad = tape0.gradient(pdeMse2, self.trainable_variables)
    lossGrad = tape0.gradient(loss, self.trainable_variables)
    del tape0

    # ---- update parameters ---- #
    self.optimizer.apply_gradients(zip(lossGrad, self.trainable_variables))

    # ---- update metrics and statistics ---- #
    # track loss and mae
    self.trainMetrics['loss'].update_state(loss*strategy.num_replicas_in_sync)
    self.trainMetrics['uMse'].update_state(uMse)
    self.trainMetrics['vMse'].update_state(vMse)
    self.trainMetrics['pMse'].update_state(pMse)
    self.trainMetrics['pde0'].update_state(pdeMse0)
    self.trainMetrics['pde1'].update_state(pdeMse1)
    self.trainMetrics['pde2'].update_state(pdeMse2)
    w = tf.squeeze(w)
    nDataPoint = tf.reduce_sum(w) + 1.0e-10
    uMae = tf.reduce_sum(tf.abs((uvpPred[:,0] - uvp[:,0]) * w))/nDataPoint
    vMae = tf.reduce_sum(tf.abs((uvpPred[:,1] - uvp[:,1]) * w))/nDataPoint
    pMae = tf.reduce_sum(tf.abs((uvpPred[:,2] - uvp[:,2]) * w))/nDataPoint
    self.trainMetrics['uMae'].update_state(uMae)
    self.trainMetrics['vMae'].update_state(vMae)
    self.trainMetrics['pMae'].update_state(pMae)
    # track gradients coefficients
    if self.saveGradStat:
      self.record_layer_gradient(uMseGrad, 'u_')
      self.record_layer_gradient(vMseGrad, 'v_')
      self.record_layer_gradient(pMseGrad, 'p_')
      self.record_layer_gradient(pdeMse0Grad, 'pde0_')
      self.record_layer_gradient(pdeMse1Grad, 'pde1_')
      self.record_layer_gradient(pdeMse2Grad, 'pde2_')
    for key in self.trainMetrics:
      self.trainStat[key] = self.trainMetrics[key].result()
    return self.trainStat


  def test_step(self, data):
    bc  = data[0][0]
    xy  = data[0][1]
    w   = data[0][2]
    uvp = data[1]

    # compuate the data and pde losses
    uvpPred, uMse, vMse, pMse, pdeMse0, pdeMse1, pdeMse2 = \
      self.compute_data_pde_losses(bc, xy, w, uvp)
    # replica's loss, divided by global batch size
    loss  = ( self.alpha[0]*uMse   + self.alpha[1]*vMse   + self.alpha[2]*pMse \
            + self.beta[0]*pdeMse0 + self.beta[1]*pdeMse1 + self.beta[2]*pdeMse2)

    # track loss and mae
    self.validMetrics['loss'].update_state(loss)
    self.validMetrics['uMse'].update_state(uMse)
    self.validMetrics['vMse'].update_state(vMse)
    self.validMetrics['pMse'].update_state(pMse)
    self.validMetrics['pde0'].update_state(pdeMse0)
    self.validMetrics['pde1'].update_state(pdeMse1)
    self.validMetrics['pde2'].update_state(pdeMse2)
    w = tf.squeeze(w)
    nDataPoint = tf.reduce_sum(w) + 1.0e-10
    uMae = tf.reduce_sum(tf.abs((uvpPred[:,0] - uvp[:,0]) * w))/nDataPoint
    vMae = tf.reduce_sum(tf.abs((uvpPred[:,1] - uvp[:,1]) * w))/nDataPoint
    pMae = tf.reduce_sum(tf.abs((uvpPred[:,2] - uvp[:,2]) * w))/nDataPoint
    self.validMetrics['uMae'].update_state(uMae)
    self.validMetrics['vMae'].update_state(vMae)
    self.validMetrics['pMae'].update_state(pMae)

    for key in self.validMetrics:
      self.validStat[key] = self.validMetrics[key].result()
    return self.validStat


  def reset_metrics(self):
    for key in self.trainMetrics:
      self.trainMetrics[key].reset_states()
    for key in self.validMetrics:
      self.validMetrics[key].reset_states()


  def summary(self):
    nVar = 0
    for t in self.trainable_variables:
      print(t.name, t.shape)
      nVar += tf.reduce_prod(t.shape)
    print('{} trainalbe variables'.format(nVar))


  def preview(self):
    print('--------------------------------')
    print('model preview')
    print('--------------------------------')
    print('fully connected network:')
    print(self.width)
    print('layer regularization')
    print(self.reg)
    print('coefficients for data loss {} {} {}'.format(\
          self.alpha[0], self.alpha[1], self.alpha[2]))
    print('coefficients for pde residual {} {} {}'.format(\
          self.beta[0], self.beta[1], self.beta[2]))
    print('--------------------------------')


def space_gradient(nn, bc, xy):
  # input bc should be a 1d tensor, expand it to 2D
  # to match xy, which is a 2D tensor [batch, 2]
  bc = tf.expand_dims(bc, 0)
  bc = tf.tile(bc, (xy.shape[0], 1))
  with tf.GradientTape(persistent=True) as tape2:
    tape2.watch(xy)
    tape2.watch(bc)
    with tf.GradientTape(persistent=True) as tape1:
      tape1.watch(xy)
      tape1.watch(bc)
      uvp = nn([bc, xy])
      u, v, p = uvp[:,0], uvp[:,1], uvp[:,2]
    u_1 = tape1.gradient(u, xy)
    v_1 = tape1.gradient(v, xy)
    del tape1
    u_x, u_y = u_1[:,0], u_1[:,1]
    v_x, v_y = v_1[:,0], v_1[:,1]
  u_xx = tape2.gradient(u_x, xy)[:,0]
  u_yy = tape2.gradient(u_y, xy)[:,1]
  v_xx = tape2.gradient(v_x, xy)[:,0]
  v_yy = tape2.gradient(v_y, xy)[:,1]
  del tape2
  u_2  = tf.stack([u_xx, u_yy], axis=-1)
  v_2  = tf.stack([v_xx, v_yy], axis=-1)

  return [u_1, u_2, v_1, v_2]


def infer_range(nn, bottom, right, top, left, xy):
  # concatenate four edges
  bc = [bottom, right, tf.reverse(top, [0]), tf.reverse(left, [0])]
  bc = tf.concat(bc, 0)
  # flatten bc as input
  bc  = tf.reshape(bc, [-1])
  # tile bc according to input xy
  shape3d = xy.shape
  xy = tf.reshape(xy, (shape3d[0]*shape3d[1], 2))
  bc = tf.expand_dims(bc, 0)
  bc = tf.tile(bc, (shape3d[0]*shape3d[1], 1))
  # infer
  uvp = nn([bc, xy])
  uvp  = tf.reshape(uvp, (shape3d[0], shape3d[1], 3))
  return uvp
