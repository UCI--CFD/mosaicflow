#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 10:51:22 2021

@author: robertplanas
"""

import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-m', '--model', default="FNO_Laplace_width-64_modes-18",
                    help='Select a model')
parser.add_argument('-d', '--data', default="../data_AMG/Sin_BC_1_AMG_5x5.h5",
                    help='Select a data file')
parser.add_argument('-DataType', '--DataType', default="float32",
                    help='float32 or float64, must match the model')
parser.add_argument('-n', '--name', default="BC = sin(x/Width)",
                    help='Name of the output file ')
parser.add_argument('-print', '--print', default=False,
                    help='Print to a file if true')
parser.add_argument('-reproduce', '--reproduce', default=False, action='store_true',\
                    help='reproduce the comparison in paper')
parser.add_argument('-it', '--iterations', type = int, default = 500,
                    help = 'Number of iterations')
parser.add_argument('-tol', '--tolerance', type = float, default = 1e-8,
                    help = 'MSE tolerance for the iterative approach')


args = parser.parse_args()


import sys
import os
import h5py as h5
import numpy as np
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../models/"))
sys.path.append('./../src/')
import Model as FNO
import Genomes as G
import time
import Print_to_File as P2F
import torch as torch
import tensorflow as tf
import utils
from utilities3 import *




#%% =============================================================================
# Load Model
# =============================================================================
Data_Type = args.DataType
#tf.keras.backend.set_floatx(Data_Type)
Path = os.path.join(os.path.dirname(__file__), "../res/")
modelName = args.model
modelPath = Path + modelName

#Get the structure of the model from the name: 
modes = int(modelName[int(modelName.find("_modes-") + 7): ])
width = int(modelName[int(modelName.find("_width-") + 7):int(modelName.find("_modes-")) ])
Model_FNO = FNO.FNO2d(modes, modes, width)
Model_FNO.load_state_dict(torch.load(modelPath, map_location=torch.device('cpu')))


#%% =============================================================================
# Load Encoder and decpders
# =============================================================================
DATA_PATH = '../../../CNN/data/Laplace_BC_AMG_ED_31.h5'
Data_File = os.path.join(os.path.dirname(__file__), DATA_PATH)
p, nSample, nCell = utils.Load_Data_File(Data_File, Scale= True)

ntrain = 1000
nval = 100
ntest = 100
x=  (torch.from_numpy(p[:ntrain + nval + ntest, :,:].astype(np.float32))).double()
y= (torch.from_numpy(p[:ntrain + nval + ntest, :,:].astype(np.float32))).double()
x[:,1:-1,1:-1] = 0

x_train = x[:ntrain, :, : ]
y_train = y[:ntrain, :, : ]

x_test = x[ntrain:ntrain+ntest, :, :]
y_test = y[ntrain:ntrain+ntest, :, :]

x_val = x[ntrain + ntest:ntrain+ntest + nval, :, :]
y_val = y[ntrain + ntest:ntrain+ntest + nval, :, :]


x_normalizer = UnitGaussianNormalizer(x_train)
y_normalizer = UnitGaussianNormalizer(y_train)



#%% ===========================================================================
# Set up the experiments
# =============================================================================

MAEs = []
TIMES = []
Strings = []

if args.reproduce:

    Data = ["../data_AMG/Sin_BC_1_AMG_1x1.h5",
            "../data_AMG/Sin_BC_1_AMG_2x2.h5",
            "../data_AMG/Sin_BC_1_AMG_3x3.h5",
            "../data_AMG/Sin_BC_1_AMG_4x4.h5",
            "../data_AMG/Sin_BC_1_AMG_5x5.h5",
            "../data_AMG/Sin_BC_1_AMG_6x6.h5",
            "../data_AMG/Sin_BC_1_AMG_8x8.h5",
            "../data_AMG/Sin_BC_2_AMG_1x1.h5",
            "../data_AMG/Sin_BC_2_AMG_2x2.h5",
            "../data_AMG/Sin_BC_2_AMG_3x3.h5",
            "../data_AMG/Sin_BC_2_AMG_4x4.h5",
            "../data_AMG/Sin_BC_2_AMG_5x5.h5",
            "../data_AMG/Sin_BC_2_AMG_6x6.h5",
            "../data_AMG/Sin_BC_2_AMG_8x8.h5"]
    
    Names = ["BC = sin(x/1)",
             "BC = sin(x/2)",
             "BC = sin(x/3)",
             "BC = sin(x/4)",
             "BC = sin(x/5)",
             "BC = sin(x/6)",
             "BC = sin(x/8)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)",
             "BC = sin(x)"]
    args.print = True 
else:
    Data = [args.data]
    Names = [args.name]
    
#%% ===========================================================================
# Run the experiments
# =============================================================================    

for index, data in enumerate(Data):    
    # =========================================================================
    # Load data, note that you have to recreate it 
    # =========================================================================
    
    File_AMG = os.path.join(os.path.dirname(__file__), data)
    psnFile = h5.File(File_AMG, 'r')
    nSample = psnFile.attrs['nSample']
    nCell = 31
    psnData = psnFile['LaplaceSolution']
    print('# Samples: {}'.format(nSample))
    print('#Genome Cells: {}*{}'.format(nCell +1, nCell +1))
    print('data shape: ', end='')
    print(psnData.shape)
    p = np.zeros(psnData.shape)
    p = psnData[:, :, :]
    pMin, pMax = np.amin(p[:, :, :]), np.amax(p[:, :, :])
    print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))
    psnFile.close()
    #%
    # Define the width of the domain
    Width = (len(p[0, :, :]) - 1) / nCell
    
    # Default max and min values used in training to normalize the inputs
    MIN = -1.199990063905716
    MAX = 1.1999822501681834
    
    
    # Normalize
    U = np.squeeze(p)
    U_N = U
    
    
    # Define the domain with ones, excluding the halo borders
    Domain_Ones = np.ones_like(U_N)
    Domain_Interior_Ones = np.zeros_like(U_N)
    Domain_Interior_Ones[1:-1,1:-1] = 1
    Domain_Borders = np.ones_like(U_N)*U_N
    Domain_Borders[1:-1, 1:-1] = 0
    
    # Generate the genomes
    Genomes_List= G.Generate_Genomes_Automatically(Width=Width, Height=Width, Genome_width=1,
                                                   Points_Borders=31, Domain_Ones=Domain_Ones,
                                                   DataType = tf.float32)
    
    # Solve the domain
    Solution, Time, MAE = G.Solve_Domain_Iterative(Genomes_List, Domain_Borders, Domain_Interior_Ones,
                 Model_FNO,  x_normalizer, y_normalizer,  Batch_Size = None, DataType = tf.float32, max_iter = args.iterations, min_tol = args.tolerance,
                 Real_Solution = U, Min = MIN, Max = MAX,)
    
    
    Current_Area = "FNO Results ------- " + Names[index] +  " Area " + str(Width**2) + " Data_Type = " + Data_Type + "  -------"
    MAEs.append(MAE)
    TIMES.append(Time)
    Strings.append(Current_Area)
    print(f"MAE = {MAE:.3e}, Time = {Time:.1e}")
    
    if(args.print):
        
        Name_File = os.path.join(os.path.dirname(__file__), "Test_Iterative_Square")
        P2F.Print_String_File(Name_File, Current_Area)
        P2F.Print_vector_File(Name_File, [MAE], "MAE")
        P2F.Print_vector_File(Name_File, [Time], "Times")

print("Final resutls -----------")
for index, MAE in enumerate(MAEs):
    Time = TIMES[index]
    print(Strings[index])
    print("       MAE = ", MAE)
    print("       Elapsed Time = ", Time)
