#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 11:07:04 2021

@author: robertplanas
"""

import numpy as np
import tensorflow as tf
import time as time 
from matplotlib import pyplot as plt
#import tensorflow_probability as tfp
import torch as torch 


def Check_Surroundings(i,j,Domain):
    if(np.isnan(Domain[i,j])):
        return True
    if(i == 0 or j == 0 or i == len(Domain[:,0]) - 1 or j == len(Domain[0,:]) -1 ):
        return True
    
    if(np.isnan(Domain[i-1,j]) or np.isnan(Domain[i+1,j]) or np.isnan(Domain[i,j+1]) or np.isnan(Domain[i,j-1])):
        return True
 
    return False 

def Extract_Border_Domain(Domain, Nan_or_Zero = "NaN"):
    if Nan_or_Zero == "NaN":
        Value = np.nan
    else:
        Value = 0
    Size_x = Domain.shape[0]
    Size_y = Domain.shape[1]
    Domain_Return = np.empty((Size_x, Size_y))
    for i in range(Size_x):
        for j in range(Size_y):
            if(Check_Surroundings(i,j, Domain[:,:])):
                Domain_Return[i,j] = Domain[i,j]
            else: 
                Domain_Return[i,j] = Value
    return Domain_Return 


# Normalize Domain
def Normalize_Domain(Domain, Min, Max):
    return (Domain -Min)/(Max - Min)*2 -1

# UnNormalize the Domain
def Un_Normalize_Domain(Domain, Min, Max):
    return (Domain +1)/2*(Max - Min) + Min 


def Predict_Batch(Genome_List, Model, Solution, Batch_Size, x_normalizer, y_normalizer, DataType = tf.float32, ):
    
    Genome_Batch = []
    Predictions_Batch = []
    Solution_Aux = tf.sparse.from_dense(tf.zeros_like(Solution))
    Auxiliar = tf.sparse.from_dense(tf.zeros_like(Solution))
    for index, Genome in enumerate(Genome_List):
        if(len(Genome_Batch) == 0 ):
            Genome_Batch = tf.expand_dims(Genome.Update_Inputs(Solution), axis = 0) 
        else:
            Genome_Batch = tf.concat([Genome_Batch, tf.expand_dims(Genome.Update_Inputs(Solution), axis = 0) ], axis = 0)
        if(Genome_Batch.shape[0] == Batch_Size):
            if(len(Predictions_Batch) == 0):
                Genome_Batch_numpy = Genome_Batch.numpy()
                Genome_Batch_numpy[:,1:-1,1:-1] = 0
                Batch = Genome_Batch_numpy.shape[0]
                Genome_Batch_torch = torch.from_numpy(Genome_Batch_numpy)
                Genome_Batch_torch = x_normalizer.encode(Genome_Batch_torch).reshape(Batch, 32,32,1)
                Predictions_Batch_torch = Model(Genome_Batch_torch).reshape(Batch, 32,32)
                Predictions_Batch_torch = y_normalizer.decode(Predictions_Batch_torch)
                Predictions_Batch_numpy = Predictions_Batch_torch.detach().numpy()
                Predictions_Batch = tf.convert_to_tensor(Predictions_Batch_numpy, dtype = DataType)

            else:
                Genome_Batch_numpy = Genome_List.numpy()
                Genome_Batch_numpy[:,1:-1,1:-1] = 0
                Batch = Genome_Batch_numpy.shape[0]
                Genome_Batch_torch = torch.from_numpy(Genome_Batch_numpy)
                Genome_Batch_torch = x_normalizer.encode(Genome_Batch_torch).reshape(Batch, 32,32,1)
                Predictions_Batch_torch = Model(Genome_Batch_torch).reshape(Batch, 32,32)
                Predictions_Batch_torch = y_normalizer.decode(Predictions_Batch_torch)
                Predictions_Batch_numpy = Predictions_Batch_torch.detach().numpy()
                Predictions_Batch_Current = tf.convert_to_tensor(Predictions_Batch_numpy, dtype = DataType)
                
                Predictions_Batch = tf.concat([Predictions_Batch, Predictions_Batch_Current], axis = 0 ) 
            Genome_Batch = []
    for index, Genome in enumerate(Genome_List):
        
        Output = tf.slice(Predictions_Batch, [index, 0,0], [1,Genome.Points_Border + 1 , Genome.Points_Border + 1])
        Solution_Aux, Auxiliar = Genome.Update_Solution(Output, Solution_Aux, Auxiliar)
    
    Solution_Aux = tf.sparse.to_dense(Solution_Aux)
    Auxiliar = tf.sparse.to_dense(Auxiliar)
   
    Updated_Domain = tf.cast(tf.cast(Auxiliar, bool), DataType)
    Non_Updated_Domain = tf.cast(tf.math.logical_not(tf.cast(Auxiliar, bool)), DataType)
    Solution_Batch = Non_Updated_Domain * Solution +  tf.math.multiply_no_nan((Solution_Aux/Auxiliar), Updated_Domain)
    
    return Solution_Batch

def Loss_Batch_Optimize(Genome_List, Model, Solution, Batch_Size, DataType = tf.float32):
    
    Genome_Batch = []
    Predictions_Batch = []
    Solution_Aux = tf.sparse.from_dense(tf.zeros_like(Solution))
    Auxiliar = tf.sparse.from_dense(tf.zeros_like(Solution))
    for index, Genome in enumerate(Genome_List):
        if(len(Genome_Batch) == 0 ):
            Genome_Batch = tf.expand_dims(Genome.Update_Inputs(Solution), axis = 0) 
        else:
            Genome_Batch = tf.concat([Genome_Batch, tf.expand_dims(Genome.Update_Inputs(Solution), axis = 0) ], axis = 0)
        if(Genome_Batch.shape[0] == Batch_Size):
            if(len(Predictions_Batch) == 0):
                Predictions_Batch = Model(Genome_Batch)
            else:
                Predictions_Batch = tf.concat([Predictions_Batch, Model(Genome_List)], axis = 0 ) 
            Genome_Batch = []
    for index, Genome in enumerate(Genome_List):
        
        Output = tf.slice(Predictions_Batch, [index, 0,0], [1,Genome.Points_Border + 1 , Genome.Points_Border + 1])
        Solution_Aux, Auxiliar = Genome.Update_Solution(Output, Solution_Aux, Auxiliar)
    
    Solution_Aux = tf.sparse.to_dense(Solution_Aux)
    Auxiliar = tf.sparse.to_dense(Auxiliar)
   
    Updated_Domain = tf.cast(tf.cast(Auxiliar, bool), DataType)
    
    return tf.reduce_mean(tf.math.square(tf.math.multiply(Solution, Updated_Domain)  - tf.math.multiply_no_nan((Solution_Aux/Auxiliar), Updated_Domain)))
    
    

class Genome:
    def __init__(self, width = 1, Points_Border = 31, x0_global = 0, y0_global = 0,
                 Domain_Width = 1, Domain_Height = 1, DataType = tf.float32):
        
        # Define parameters
        self.width = width 
        self.Points_Border = Points_Border
        self.x0_global = x0_global
        self.y0_global = y0_global
        self.i0 = int(x0_global/width*Points_Border)
        self.j0 = int(y0_global/width*Points_Border)
        self.Domain_Width = Domain_Width
        self.Domain_Height = Domain_Height
        self.Domain_Width_Pixels = int(Domain_Width*Points_Border + 1)
        self.Domain_Height_Pixels = int(Domain_Height*Points_Border + 1)
        self.Input = tf.zeros((Points_Border+1, Points_Border+1))
        Border_Values = np.ones((Points_Border+1, Points_Border+1))
        Border_Values[1:-1,1:-1] = np.zeros((Points_Border-1, Points_Border-1))
        self.Border_Values = tf.convert_to_tensor(Border_Values, dtype = DataType )
        self.Indices = []
        for i in range(self.i0, self.i0+Points_Border +1 ):
            for j in range(self.j0, self.j0+Points_Border +1 ):
                Indices =np.asarray([int(i),int(j)])[None, :]
                if(len(self.Indices) == 0):
                    self.Indices = Indices
                else:
                    self.Indices = np.vstack([self.Indices, Indices])   
    def Update_Inputs(self, Solution):
        self.Input = tf.multiply(self.Border_Values, tf.slice(Solution, [self.i0, self.j0],
                                                              [self.Points_Border + 1, self.Points_Border +1]))
        return self.Input
                
    
    def Update_Solution(self, Output, Solution, Auxiliar):
        # Output : Dense tensor of shape (Points_Border +1, Points_Border +1)
        # Solution: Sparse tensor
        # Auxiliar: Sparse tensor
        Output = tf.sparse.SparseTensor(self.Indices, tf.reshape(Output,((self.Points_Border +1)**2)), 
                                        dense_shape = [self.Domain_Width_Pixels, self.Domain_Height_Pixels])
        Ones = tf.sparse.SparseTensor(self.Indices, tf.ones(((self.Points_Border +1)**2,)), 
                                      dense_shape = [self.Domain_Width_Pixels, self.Domain_Height_Pixels])
        Solution = tf.sparse.add(Solution, Output)
        Auxiliar = tf.sparse.add(Auxiliar, Ones)    
        return Solution, Auxiliar
    
    
def Generate_Genomes_Automatically(Width, Height, Genome_width,  Points_Borders, Domain_Ones , DataType = tf.float32):
    
    Genomes = []
    Number_of_Genomes = 0
    Number_Gnomes_x = int(Width/Genome_width)
    Number_Gnomes_y = int(Height/Genome_width)
    print("Placing Genomes...")
    #Set the lists of gnomes   
    H_V = [[0,0], [0, Genome_width/2], [Genome_width/2, 0], [Genome_width/2, Genome_width/2]]
    for index, element in enumerate(H_V):
        print(f"Placing Layer of Genomes {index +1 }/4")
        Layer_Genomes = []
        for x in range(Number_Gnomes_x):
            for y in range(Number_Gnomes_y):
                x0_global = x*Genome_width + element[0]
                y0_global = y*Genome_width + element[1]
                i0_global = int(x0_global/Genome_width*Points_Borders)
                j0_global = int(y0_global/Genome_width*Points_Borders)
                if(x0_global + Genome_width <= Width and y0_global + Genome_width <= Height and 
                   (Domain_Ones[i0_global:i0_global + Points_Borders +1 ,j0_global:j0_global + Points_Borders +1] == 1).all()):
                    Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, 
                                            x0_global = x0_global, y0_global = y0_global,
                                            Domain_Width =Width, Domain_Height=Height, DataType = DataType)
                    Layer_Genomes.append(Genome_element)
        Number_of_Genomes += len(Layer_Genomes)
        Genomes.append(Layer_Genomes)
    
                
    print("A total of ", (Number_of_Genomes), " genomes are being used")
    return Genomes

def Reinitialize_Borders_Solution(Solution, Domain_Borders,Domain_Interior_Ones):
    return Domain_Borders + Domain_Interior_Ones*Solution

def Solve_Domain_Iterative(Genomes_List, Domain_Borders, Domain_Interior_Ones,
                 Model,x_normalizer, y_normalizer,  Batch_Size = None, DataType = tf.float32, max_iter = 500, min_tol = 1e-8,
                 Real_Solution = None, Min = -1, Max = +1):
    
    Start_Time = time.time()
    Iterations = 0
    Tolerance = np.inf
    Solution = tf.cast(tf.ones_like(Domain_Borders) * Domain_Borders, DataType)
    
    #Convert inputs to tensors
    Domain_Borders = tf.cast(tf.convert_to_tensor(Domain_Borders), DataType)
    Domain_Interior_Ones = tf.cast(tf.convert_to_tensor(Domain_Interior_Ones), DataType)
    while(Iterations < max_iter):
        Solution_P = Solution
        for Layer_Genomes in Genomes_List:
            # Reinitialize the borders of the solution:
            Solution = Reinitialize_Borders_Solution(Solution, Domain_Borders,Domain_Interior_Ones)
            if(Batch_Size == None ):
                Batch_Size_L = len(Layer_Genomes)
            else:
                Batch_Size_L = Batch_Size
            Solution = Predict_Batch(Layer_Genomes, Model, Solution, Batch_Size_L, x_normalizer, y_normalizer,  DataType = DataType)  
        Tolerance = tf.reduce_mean(tf.square(Solution_P - Solution))
        print(f"Iteration: {Iterations}, Tolerance = {Tolerance:.2e}")
        Solution_ii = Solution
        MAE = np.mean(np.abs(Solution_ii - Real_Solution ))
        print(f"Iteration: {Iterations}, Tolerance = {Tolerance:.2e}, MAE = {MAE:.3e}")
        if(Tolerance < min_tol):
            break
        Iterations += 1
        
    
    Solution = Solution.numpy()
    #Solution = Un_Normalize_Domain(Solution, Min, Max)
    MAE = None
    Time = time.time() - Start_Time
    if(Real_Solution is not None):
        MAE = np.mean(np.abs(Solution - Real_Solution ))
    
    return Solution, Time, MAE 
    
        
class Optimize:
    def __init__(self, Genomes_List, Domain_Borders, Domain_Interior_Ones,
                 Model, Batch_Size = None, DataType = tf.float32, max_iter = 500, min_tol = 1e-8,
                 Real_Solution = None, Min = -1, Max = +1, Random_Initilize = False,
                 Compute_Intermediate = False):
        
        # Parameters
        self.Genomes_List = Genomes_List
        self.Domain_Borders = Domain_Borders
        self.Domain_Interior_Ones = Domain_Interior_Ones
        self.Model = Model
        self.Batch_Size = Batch_Size
        self.DataType = DataType
        self.max_iter = max_iter
        self.min_tol = min_tol
        self.Real_Solution = Real_Solution
        self.Min = Min 
        self.Max = Max
        self.Compute_Intermediate = Compute_Intermediate
        # Initialize Domain 
        Shape_Domain = Domain_Interior_Ones.shape 
        self.Solution_Values = tf.reshape(Domain_Borders, (Shape_Domain[0]*Shape_Domain[1],))
        if(Random_Initilize):
            self.Solution_Values += tf.reshape(np.random.rand(Shape_Domain)*Domain_Interior_Ones, (Shape_Domain[0]*Shape_Domain[1],))
        self.Solution_Values = tf.convert_to_tensor(self.Solution_Values, dtype = self.DataType)
        
        
        # Dictionary of Losses
        self.History = {}
        # History of the total loss
        self.History['Loss'] = []
        if(self.Real_Solution is not None and self.Compute_Intermediate):
            self.History['MAE'] = []
        self.Iteration = 0  
        
        self.Optimize()

        
    def loss(self, x):
        
        x = tf.reshape(x, (self.Domain_Interior_Ones.shape))
        Loss = 0
        for Layer_Genomes in self.Genomes_List:
            # Reinitialize the borders of the solution:
            x = Reinitialize_Borders_Solution(x, self.Domain_Borders,self.Domain_Interior_Ones)
            if(self.Batch_Size == None ):
                Batch_Size_L = len(Layer_Genomes)
            else:
                Batch_Size_L = self.Batch_Size
            Loss += Loss_Batch_Optimize(Layer_Genomes, self.Model, x, Batch_Size_L, DataType = self.DataType)  
            
        self.History['Loss'].append(Loss.numpy()) 
        if(self.Real_Solution is not None and self.Compute_Intermediate):  
            Solution_ii = Un_Normalize_Domain(x, self.Min, self.Max)
            MAE = np.mean(np.abs(Solution_ii - self.Real_Solution ))
            self.History['MAE'].append(MAE)

        if(np.mod(self.Iteration,10)==0):
            print(f"Iteration ={self.Iteration},  Loss = {Loss}")
        self.Iteration += 1
        return Loss

    def Loss_and_Graidents(self, x):
        return tfp.math.value_and_gradient(
            lambda x: self.loss(x),
            x)
    
        
    def Optimize(self): 
        
        self.Time = time.time()
        start = self.Solution_Values
        optim_results = tfp.optimizer.lbfgs_minimize(
              self.Loss_and_Graidents,
              initial_position=start,
              num_correction_pairs=10,
              x_tolerance=self.min_tol,
              max_iterations = self.max_iter)
        
        self.Results = Un_Normalize_Domain(np.reshape(optim_results.position.numpy(), self.Domain_Interior_Ones.shape), self.Min, self.Max)
        self.MAE = None
        self.Time = time.time() - self.Time
        if(self.Real_Solution is not None):
            self.MAE = np.mean(np.abs(self.Results - self.Real_Solution ))



            
            
            
        
        
        
