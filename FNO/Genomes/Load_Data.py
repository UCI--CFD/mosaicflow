#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 24 08:17:31 2021

@author: robertplanas
"""

import os 
import vtk as vtk 
from vtk.util.numpy_support import vtk_to_numpy
import numpy as np

def Load_Single_Cavity(vtkFile):

    # =============================================================================
    # Load the data into a matrix
    # =============================================================================
    
    Data_File_vtk = os.path.join(os.path.dirname(__file__), vtkFile)
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(Data_File_vtk)
    reader.Update()
    vtkData = reader.GetOutput()
    # check # points
    
    
    p = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
    U = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
    pntxyz =  vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPoints().GetData())
    
    Block = np.zeros(((129,129,3)))
    Coords_Block = np.zeros((129,129,2))
    
    s,  e  = 0, 129*129
    ni, nj = 129,129
    Coords_Block[...]  = np.reshape(pntxyz[s:e,:2], (ni,nj,2))
    Block[:,:,:2] = np.reshape(U[s:e,:2],(ni,nj,2))
    Block[:,:,2]  = np.reshape(p[s:e], (ni,nj))
    
    # You need to coarse the grid
    x0 = 0
    y0 = 0
    Width = 1
    Height = 1
    U = Block[x0:x0+int(Width*2*65):2,y0:y0+int(Height*2*65):2,:]
    Coords = Coords_Block[x0:x0+int(Width*2*65):2,y0:y0+int(Height*2*65):2,:]
    
    return U, Coords

def Load_Single_Cavity_Dense(vtkFile):

    # =============================================================================
    # Load the data into a matrix
    # =============================================================================
    
    Data_File_vtk = os.path.join(os.path.dirname(__file__), vtkFile)
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(Data_File_vtk)
    reader.Update()
    vtkData = reader.GetOutput()
    # check # points
    
    
    p = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
    U = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
    pntxyz =  vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPoints().GetData())
    
    Block = np.zeros(((129,129,3)))
    Coords_Block = np.zeros((129,129,2))
    
    s,  e  = 0, 129*129
    ni, nj = 129,129
    Coords_Block[...]  = np.reshape(pntxyz[s:e,:2], (ni,nj,2))
    Block[:,:,:2] = np.reshape(U[s:e,:2],(ni,nj,2))
    Block[:,:,2]  = np.reshape(p[s:e], (ni,nj))
    
    # You need to coarse the grid
    U = Block
    Coords = Coords_Block
    
    return U, Coords



def Load_L_Cavity(vtkFile):

    # =============================================================================
    # Load the data into a matrix
    # =============================================================================
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(vtkFile)
    reader.Update()
    vtkData = reader.GetOutput()
    # check # points
    
    p = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
    U = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
    pntxyz = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPoints().GetData())
    
    
    Coords_Block = np.zeros((257, 257, 2)) * np.nan
    Block = np.zeros((257, 257, 3)) * np.nan
    
    for i in range(np.shape(pntxyz)[0]):
        x = pntxyz[i, 0]
        y = pntxyz[i, 1]
        I = y * 128 + 128
        J = x * 128
        Coords_Block[int(I), int(J), :] = [x, y + 1]
        Block[int(I), int(J), :2] = U[i, :2]
        Block[int(I), int(J), 2] = p[i]
    
    
    # coarse the grid
    x0 = 0
    y0 = 0
    Width = 2
    Height = 2
    U = Block[x0:x0 + int(Width * 2 * 65):2, y0:y0 + int(Height * 2 * 65):2, :]
    Coords = Coords_Block[x0:x0 +
                          int(Width * 2 * 65):2, y0:y0 + int(Height * 2 * 65):2, :]
    
    return U, Coords


def Load_L_Cavity_Dense(vtkFile):

    # =============================================================================
    # Load the data into a matrix
    # =============================================================================
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(vtkFile)
    reader.Update()
    vtkData = reader.GetOutput()
    # check # points
    
    p = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
    U = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
    pntxyz = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPoints().GetData())
    
    
    Coords_Block = np.zeros((257, 257, 2)) * np.nan
    Block = np.zeros((257, 257, 3)) * np.nan
    
    for i in range(np.shape(pntxyz)[0]):
        x = pntxyz[i, 0]
        y = pntxyz[i, 1]
        I = y * 128 + 128
        J = x * 128
        Coords_Block[int(I), int(J), :] = [x, y + 1]
        Block[int(I), int(J), :2] = U[i, :2]
        Block[int(I), int(J), 2] = p[i]
    
    
    # coarse the grid
    U = Block
    Coords = Coords_Block
    
    return U, Coords

def Load_Long_Cavity(vtkFile):

    # =============================================================================
    # Load the data into a matrix
    # =============================================================================
    
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(vtkFile)
    reader.Update()
    vtkData = reader.GetOutput()
    # check # points
    
    
    p = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
    U = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
    pntxyz =  vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPoints().GetData())
    
    # Get the dimensions 
    X = np.max(pntxyz,axis = 0)[0]
    Y = np.max(pntxyz, axis = 0)[1]
    
    Step_x = min(i for i in pntxyz[:,0] if i > 0)
    Step_y = min(i for i in pntxyz[:,1] if i > 0)
    Y_Samples = int(Y/Step_y + 1 )
    X_Samples = int(X/Step_x + 1 )
    
    Block = np.zeros(((Y_Samples,X_Samples,3)))
    Coords_Block = np.zeros((Y_Samples,X_Samples,2))

    
    s,  e  = 0, X_Samples*Y_Samples
    ni, nj = X_Samples,Y_Samples
    Coords_Block[...]  = np.reshape(pntxyz[s:e,:2], (nj,ni,2))
    Block[:,:,:2] = np.reshape(U[s:e,:2],(nj,ni,2))
    Block[:,:,2]  = np.reshape(p[s:e], (nj,ni))
    
        
    for i in range(int(np.shape(pntxyz)[0]/2)):
        x = pntxyz[i, 0]
        y = pntxyz[i, 1]
        I = y / Step_y
        J = x / Step_x
        Coords_Block[int(I), int(J), :] = [x, y]
        Block[int(I), int(J), :2] = U[i, :2]
        Block[int(I), int(J), 2] = p[i]
    
    # You need to coarse the grid
    U = Block[::2,::2,:]
    Coords = Coords_Block[::2,::2,:]
    
    return U, Coords


def Load_Long_Cavity_Dense(vtkFile):

    # =============================================================================
    # Load the data into a matrix
    # =============================================================================
    
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(vtkFile)
    reader.Update()
    vtkData = reader.GetOutput()
    # check # points
    
    
    p = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('p'))
    U = vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPointData().GetArray('U'))
    pntxyz =  vtk.util.numpy_support.vtk_to_numpy(vtkData.GetPoints().GetData())
    
    # Get the dimensions 
    X = np.max(pntxyz,axis = 0)[0]
    Y = np.max(pntxyz, axis = 0)[1]
    
    Step_x = min(i for i in pntxyz[:,0] if i > 0)
    Step_y = min(i for i in pntxyz[:,1] if i > 0)
    Y_Samples = int(Y/Step_y + 1 )
    X_Samples = int(X/Step_x + 1 )
    
    Block = np.zeros(((Y_Samples,X_Samples,3)))
    Coords_Block = np.zeros((Y_Samples,X_Samples,2))

    
    s,  e  = 0, X_Samples*Y_Samples
    ni, nj = X_Samples,Y_Samples
    Coords_Block[...]  = np.reshape(pntxyz[s:e,:2], (nj,ni,2))
    Block[:,:,:2] = np.reshape(U[s:e,:2],(nj,ni,2))
    Block[:,:,2]  = np.reshape(p[s:e], (nj,ni))
    
        
    for i in range(int(np.shape(pntxyz)[0]/2)):
        x = pntxyz[i, 0]
        y = pntxyz[i, 1]
        I = y / Step_y
        J = x / Step_x
        Coords_Block[int(I), int(J), :] = [x, y]
        Block[int(I), int(J), :2] = U[i, :2]
        Block[int(I), int(J), 2] = p[i]
    
    # You need to coarse the grid
    U = Block
    Coords = Coords_Block
    
    return U, Coords
