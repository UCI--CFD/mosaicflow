# Fourier Neural Operator

This repository contains the codes adapted from the paper:
- [(FNO) Fourier Neural Operator for Parametric Partial Differential Equations](https://arxiv.org/abs/2010.08895)

To train the models, for both the NS and the Laplace equations, use the codes: 
- ./src/train_FNO_Laplace.py and ./src/train_FNO_Laplace.py

To use the MF Predictor on different domains use the codes found on ./src/Genomes/
- Mosaic_Flow_Domain_FNO_Operator.py: 
	Solves the Laplace equation on the "Mosaic Flow" domain shape- NS_L_Cavity.py: 
	Solves the NS equation for the step-shaped cavity- Square_Domain_FNO_Operator_8000.py: 
	Solves the Laplace equation on square domain


## Citations

```
@misc{li2020fourier,
      title={Fourier Neural Operator for Parametric Partial Differential Equations}, 
      author={Zongyi Li and Nikola Kovachki and Kamyar Azizzadenesheli and Burigede Liu and Kaushik Bhattacharya and Andrew Stuart and Anima Anandkumar},
      year={2020},
      eprint={2010.08895},
      archivePrefix={arXiv},
      primaryClass={cs.LG}
}

```
