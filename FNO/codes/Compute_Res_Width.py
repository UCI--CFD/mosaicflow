#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 14 10:44:23 2021

@author: robertplanas
"""


import sys
import os
import h5py as h5
import numpy as np
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../models/"))
sys.path.append('./../src/')
sys.path.append(os.path.join(os.path.dirname(__file__), "../Genomes/"))
import Model as FNO
import Genomes as G
import time
import Print_to_File as P2F
import torch as torch
import tensorflow as tf
import utils
from utilities3 import *
from scipy import interpolate
from matplotlib import pyplot as plt


def Compute_Res_Laplace(Solution, h = 1/31):
    Shape_Solution = Solution.shape
    RHS = np.zeros((Shape_Solution[0] -2, Shape_Solution[1]- 2))  
    for k in range( Shape_Solution[0] -2 ):
        for l in range(Shape_Solution[1] -2 ):
            RHS[k,l] = (4*Solution[k+1,l+1] - Solution[k+1, l] - Solution[k,l+1] - Solution[k+2, l+1] - Solution[k+1, l+2])/(h**2)
        
    mean_RHS = np.mean(np.square(RHS))
    max_RHS = np.max(np.abs(RHS))
    print('Max Absolute Error RHS ',max_RHS )
    return mean_RHS
    
    
    
    

################# LAPLACE ######################

#%% =============================================================================
# Load Model
# =============================================================================
Data_Type = 'float32'
tf.keras.backend.set_floatx(Data_Type)
Path = os.path.join(os.path.dirname(__file__), "../res/")
modelName = "FNO_Laplace_width-64_modes-18"
modelPath = Path + modelName

#Get the structure of the model from the name: 
modes = int(modelName[int(modelName.find("_modes-") + 7): ])
width = int(modelName[int(modelName.find("_width-") + 7):int(modelName.find("_modes-")) ])
Model_FNO = FNO.FNO2d(modes, modes, width)
Model_FNO.load_state_dict(torch.load(modelPath, map_location=torch.device('cpu')))


#%% =============================================================================
# Load Encoder and decpders
# =============================================================================
DATA_PATH = '../../../CNN/data/Laplace_BC_AMG_ED_31.h5'
Data_File = os.path.join(os.path.dirname(__file__), DATA_PATH)
p, nSample, nCell = utils.Load_Data_File(Data_File, Scale= True)

ntrain = 8000
nval = 100
ntest = 100
x=  (torch.from_numpy(p[:ntrain + nval + ntest, :,:].astype(np.float32))).double()
y= (torch.from_numpy(p[:ntrain + nval + ntest, :,:].astype(np.float32))).double()
x[:,1:-1,1:-1] = 0

x_train = x[:ntrain, :, : ]
y_train = y[:ntrain, :, : ]

x_test = x[ntrain:ntrain+ntest, :, :]
y_test = y[ntrain:ntrain+ntest, :, :]

x_val = x[ntrain + ntest:ntrain+ntest + nval, :, :]
y_val = y[ntrain + ntest:ntrain+ntest + nval, :, :]


x_normalizer = UnitGaussianNormalizer(x_train)
y_normalizer = UnitGaussianNormalizer(y_train)


#%% =============================================================================
# Test the MAE on test
# =============================================================================
MAEs = []
x_train_n = x_normalizer.encode(x_train).reshape(ntrain,32,32,1)

AA = 100
for i in range(int(ntrain/AA)):
    print(i)
    Y_train_Pred = y_normalizer.decode(Model_FNO(x_train_n[i*AA :i*AA + AA, :, :, :]).reshape(AA,32,32))
    for j in range(AA):
        MAE = np.mean(np.abs(y_train[i*AA + j,:,:].detach().numpy() - Y_train_Pred[j,:,:].detach().numpy()))
        MAEs.append(MAE)

print(f'MAE = {np.mean(MAEs) :.3e}')




#%% =============================================================================
# Test the residual on the test data
# =============================================================================



Width = 1/8
y_val_width = np.zeros_like(y_val)
x_val_width =  np.zeros_like(x_val)
x = np.arange(0, 1 + 1/31, 1/31)
y = np.arange(0, 1 + 1/31, 1/31)
x_width = np.arange(0, 1*Width + 1/31*Width, 1/31*Width)
y_width = np.arange(0, 1*Width + 1/31*Width, 1/31*Width)

xx, yy = np.meshgrid(x, y)
for i in range(nval):
    f = interpolate.interp2d(x, y, y_val[i,:,:], kind='quintic')
    y_val_width[i,:,:] = f(x_width,y_width )
    x_val_width[i,:,:] = np.ones_like(y_val_width[i,:,:])*y_val_width[i,:,:]
    x_val_width[i,1:-1,1:-1] = 0 
    
x_val_width = torch.Tensor(x_val_width)
x_test_n = x_normalizer.encode(x_val_width).reshape(nval,32,32,1)
Y_Test_1 = Model_FNO.Predict(x_test_n, width = Width)


Y_test_p = y_normalizer.decode(Model_FNO(x_test_n).reshape(nval,32,32))
mean_RHS = []
for i in range(nval) :
    Solution = Y_test_p[i,:,:]
    mean_RHS_current = Compute_Res_Laplace(Solution, h = 1/31*Width)
    mean_RHS.append(mean_RHS_current)
    
mean_RHS = np.asarray(mean_RHS)
RHS = np.mean(mean_RHS)

print(RHS)

# ============================================================================= 
#%% Compute the AD
# =============================================================================

def nth_derivative(f, wrt, n):

    for i in range(n):

        grads = grad(f, wrt, create_graph=True)[0]
        f = grads.sum()

    return grads

def f(x, grid ):
    U_n = Model_FNO.Compute_Res(x_test_n, grid)
    U = y_normalizer.decode(U_n.reshape(nval,32,32))
    return U.sum()

gird = torch.autograd.Variable(Model_FNO.get_grid(x_test_n.shape, x_test_n.device), requires_grad = True)
grid.retain_grad()


grads_x = torch.autograd.grad(f(x,grid), grid, create_graph=True)
grads_xx = torch.autograd.grad(grads_x[0][:,:,:,0].sum(), grid, create_graph=True)
grads_yy = torch.autograd.grad(grads_x[0][:,:,:,1].sum(), grid, create_graph=True)

    
#%%

U_xx = grads_xx[0][:,:,:,0]
U_yy = grads_yy[0][:,:,:,1]

Res = U_xx + U_yy










