#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 14 10:44:23 2021

@author: robertplanas
"""


import sys
import os
import h5py as h5
import numpy as np
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../src/"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../models/"))
sys.path.append('./../src/')
sys.path.append(os.path.join(os.path.dirname(__file__), "../Genomes/"))
import Model as FNO
import Genomes as G
import time
import Print_to_File as P2F
import torch as torch
import tensorflow as tf
import utils
from utilities3 import *
import NS_dataset as NS_D 
import pickle5 as pickle5
from scipy import interpolate



def Compute_Residual_Whole(U_Predicted, V_Predicted, P_Predicted,
                     Width = 0.5, nCell = 33) :
    h = 1/Width*(nCell -1)
              
    #Compute the derivatives for U 
    U_Pred_R =  tf.roll(U_Predicted, shift = 1, axis = -1)
    U_Pred_U = tf.roll(U_Predicted, shift = -1, axis = -2)
    U_Pred_L = tf.roll(U_Predicted, shift = -1, axis = -1)
    U_Pred_D = tf.roll(U_Predicted, shift = 1, axis = -2)
    
    U_Pred_L_s = (U_Pred_L - U_Predicted)*h
    U_Pred_R_s = (U_Predicted - U_Pred_R)*h
    U_Pred_D_s = ( U_Predicted - U_Pred_D )*h
    U_Pred_U_s = (U_Pred_U - U_Predicted)*h
    
    U_Pred_x = (U_Pred_L_s + U_Pred_R_s)/2
    U_Pred_y = (U_Pred_U_s + U_Pred_D_s)/2
    U_Pred_xx = (U_Pred_L_s -U_Pred_R_s )*h
    U_Pred_yy = (U_Pred_U_s - U_Pred_D_s )*h
    
    #Compute the derivatives for V 
    V_Pred_R =  tf.roll(V_Predicted, shift = 1, axis = -1)
    V_Pred_U = tf.roll(V_Predicted, shift = -1, axis = -2)
    V_Pred_L = tf.roll(V_Predicted, shift = -1, axis = -1)
    V_Pred_D = tf.roll(V_Predicted, shift = 1, axis = -2)
    
    V_Pred_L_s = (V_Pred_L - V_Predicted )*h
    V_Pred_R_s = (V_Predicted - V_Pred_R)*h
    V_Pred_D_s = (V_Predicted- V_Pred_D)*h
    V_Pred_U_s = (V_Pred_U- V_Predicted)*h
    
    V_Pred_x = (V_Pred_L_s + V_Pred_R_s)/2
    V_Pred_y = (V_Pred_U_s + V_Pred_D_s)/2
    V_Pred_xx = (V_Pred_L_s - V_Pred_R_s)*h
    V_Pred_yy = (V_Pred_U_s - V_Pred_D_s)*h
    
    #Compute the derivatives for P
    P_Pred_R =  tf.roll(P_Predicted, shift = 1, axis = -1)
    P_Pred_U = tf.roll(P_Predicted, shift = -1, axis = -2)
    P_Pred_L = tf.roll(P_Predicted, shift = -1, axis = -1)
    P_Pred_D = tf.roll(P_Predicted, shift = 1, axis = -2)
    
    P_Pred_L_s = (P_Pred_L - P_Predicted )*h
    P_Pred_R_s = ( P_Predicted - P_Pred_R)*h
    P_Pred_D_s = (P_Predicted- P_Pred_D)*h
    P_Pred_U_s = (P_Pred_U- P_Predicted)*h
    
    P_Pred_x = (P_Pred_L_s + P_Pred_R_s)/2
    P_Pred_y = (P_Pred_U_s + P_Pred_D_s)/2
    
    pde0_Values = U_Pred_x + V_Pred_y
    pde1_Values = U_Predicted*U_Pred_x + V_Predicted*U_Pred_y + P_Pred_x - (U_Pred_xx + U_Pred_yy)/500.0
    pde2_Values = U_Predicted*V_Pred_x + V_Predicted*V_Pred_y + P_Pred_y - (V_Pred_xx + V_Pred_yy)/500.0
    
    # Take out the boundaries
    pde0_IN = tf.keras.layers.Cropping2D(cropping=((1, 1), (1, 1)))(tf.expand_dims(pde0_Values, axis = -1))
    pde1_IN = tf.keras.layers.Cropping2D(cropping=((1, 1), (1, 1)))(tf.expand_dims(pde1_Values, axis = -1))
    pde2_IN = tf.keras.layers.Cropping2D(cropping=((1, 1), (1, 1)))(tf.expand_dims(pde2_Values, axis = -1))
    
    pde0 = tf.reduce_mean(tf.square(pde0_IN), axis = [1,2])
    pde1 = tf.reduce_mean(tf.square(pde1_IN), axis = [1,2])
    pde2 = tf.reduce_mean(tf.square(pde2_IN), axis = [1,2])
    
    return pde0, pde1, pde2
    

################# NS ######################

#%% =============================================================================
# Load Model
# =============================================================================
Data_Type = 'float32'
tf.keras.backend.set_floatx(Data_Type)
Path = os.path.join(os.path.dirname(__file__), "../res/")
modelName = "FNO_NS_width-64_modes-16"
modelPath = Path + modelName

#Get the structure of the model from the name: 
modes = int(modelName[int(modelName.find("_modes-") + 7): ])
width = int(modelName[int(modelName.find("_width-") + 7):int(modelName.find("_modes-")) ])
Model_FNO = FNO.FNO2d_ns(modes, modes, width)
Model_FNO.load_state_dict(torch.load(modelPath, map_location=torch.device('cpu')))



################################################################
# configs
################################################################
DATA_PATH = '../../../CNN/data/genomes_merged.h5'
Data_File = os.path.join(os.path.dirname(__file__), DATA_PATH)


# load data
dataSet = NS_D.NSDataSet()
dataSet.add_file(Data_File)
dataSet.load_data()
dataSet.extract_genome_bc()
dataSet.summary()
DATA = dataSet.var[:,:,:,:]

C_Samples = dataSet.var.shape[0]

Input_Variables = 2
Output_Variables = 3


np.random.shuffle(DATA)
Total_Samples = len(DATA)
nval = 100
ntest = 100
ntrain = 2000


batch_size = 20
learning_rate = 0.001

step_size = 100
gamma = 0.5

s = 33
################################################################
# load data and data normalization
################################################################
x=  (torch.from_numpy(DATA[:ntrain + nval + ntest, :,:,:Input_Variables].astype(np.float32))).double()
y= (torch.from_numpy(DATA[:ntrain + nval + ntest, :,:,:].astype(np.float32))).double()
x[:,1:-1,1:-1, :] = 0

x_train = x[:ntrain, :, :, : ]
y_train = y[:ntrain, :, :, : ]

x_test = x[ntrain:ntrain+ntest, :, :, :]
y_test = y[ntrain:ntrain+ntest, :, :, :]

x_val = x[ntrain + ntest:ntrain+ntest + nval, :, :, :]
y_val = y[ntrain + ntest:ntrain+ntest + nval, :, :, :]


Norm_File = os.path.join(os.path.dirname(__file__), '../res/normalizers.pkl')
with open(Norm_File, 'rb') as inp:
    x_normalizer = pickle5.load(inp)
    y_normalizer = pickle5.load(inp)


#%% =============================================================================
# Test the residual on the test data
# =============================================================================


Width = 0.25
y_val_width = np.zeros_like(y_val)
x_val_width =  np.zeros_like(x_val)
x = np.arange(0, 0.5 + 1/64, 1/64)
y = np.arange(0, 0.5 + 1/64, 1/64)
x_width = np.arange(0, 1*Width + 1/32*Width, 1/32*Width)
y_width = np.arange(0, 1*Width + 1/32*Width, 1/32*Width)

xx, yy = np.meshgrid(x, y)
for i in range(nval):
    f_u = interpolate.interp2d(x, y, y_val[i,:,:, 0], kind='quintic')
    f_v = interpolate.interp2d(x, y, y_val[i,:,:, 1], kind='quintic')
    y_val_width[i,:,:, 0] = f_u(x_width,y_width )
    y_val_width[i,:,:,1] = f_v(x_width,y_width )
    x_val_width[i,:,:] = np.ones_like(y_val_width[i,:,:,:2])*y_val_width[i,:,:,:2]
    x_val_width[i,1:-1,1:-1,:] = 0 
    
x_val_width = torch.Tensor(x_val_width)

x_test_n = x_normalizer.encode(x_val_width).reshape(nval,33,33,2)
Y_test_p = y_normalizer.decode(Model_FNO(x_test_n).reshape(nval,33,33, 3))
y_val_p = y_val.detach().numpy()
pde_0 = []
pde_1 = []
pde_2 = []

for i in range(nval) :
    Solution = Y_test_p[i:i+1,:,:,:].detach().numpy()
    U_Predicted = tf.convert_to_tensor(Solution[:,:,:,0], dtype = tf.float32)
    V_Predicted = tf.convert_to_tensor(Solution[:,:,:,1], dtype = tf.float32)
    P_Predicted = tf.convert_to_tensor(Solution[:,:,:,2], dtype = tf.float32)
    pde_0_current, pde_1_current, pde_2_current = Compute_Residual_Whole(U_Predicted, V_Predicted,P_Predicted, Width =Width)
    pde_0.append(pde_0_current)
    pde_1.append(pde_1_current)
    pde_2.append(pde_2_current)
    
pde_0 = np.asarray(pde_0)
pde_1 = np.asarray(pde_1)
pde_2 = np.asarray(pde_2)

pde_0_mean = np.mean(pde_0)
pde_1_mean = np.mean(pde_1)
pde_2_mean = np.mean(pde_2)



print(f'{pde_0_mean:.3e}')
print(f'{pde_1_mean:.3e}')
print(f'{pde_2_mean:.3e}')

