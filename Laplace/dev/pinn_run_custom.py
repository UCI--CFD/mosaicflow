import sys
sys.path.append('../src')
import h5py as h5
import numpy as np
import argparse
import time
import scipy.io


parser = argparse.ArgumentParser()
parser.add_argument('-l', '--arch', type=int, nargs='*', help='architecture')
parser.add_argument('-r', '--reg', type=float, nargs='*', default=None,
                    help='l2 regularization')
parser.add_argument('-Epochs', '--TrainEpochs', type=int, default=10000,
                    help='Training epochs')
parser.add_argument('-d', '--nDataPoint', type=int, default=0,
                    help='number of data points in training')
parser.add_argument('-c', '--nCollocPoint', type=int, default=400,
                    help='number of collocation points in training')
parser.add_argument('-bc', '--nBC_Solution', type=int, default=32,
                    help='number of boundary conditions in training')
parser.add_argument('-m', '--model', type=int, default=0,
                    help='0 - psnfc, 1 - psnxyfc')
parser.add_argument('-alpha', '--alpha', type=float, default=0.01,
                    help='regularization of pde')
parser.add_argument('-s', '--nSolPerBatch', type=int, default=20,
                    help='number of solution per batch')
parser.add_argument('-f', '--file', default='../data/Laplace_2_64_Diff_Non_Unif.mat',
                    help='data file')
parser.add_argument('-g', '--saveGradStat', default=False, action='store_true',
                    help='save gradient statistics')
parser.add_argument('-gpu', '--GPU', default=None, help='Specify GPU to use')
args = parser.parse_args()

if(args.GPU is not None):
  import os
  os.environ["CUDA_VISIBLE_DEVICES"] = str(args.GPU)

import tensorflow as tf
from tensorflow import keras
import pinn_model_custom as psnModel
import pinn_utils_custom as PUC
keras.backend.set_floatx('float32')

# --------------------------------------------

# Define architecture using the inputs
if args.arch:
  fcLayers = args.arch
  for i in range(len(fcLayers)):
    fcLayers[i] = int(fcLayers[i])
else:
  fcLayers = [128, 128, 128, 1]
archStr = ''
for l in fcLayers:
  archStr += ('-' + repr(l))
print('arch ' + archStr)

# load mat data
Data_mat = scipy.io.loadmat(args.file)
BCs = np.asarray(Data_mat['BCs_save_non_uniform_homo'])
Us = np.asarray(Data_mat['u_save_non_uniform_homo'])
height = Data_mat['height']
width = Data_mat['width']
nSolutions = Us.shape[0]
nSamples_Solution = Us.shape[-1]
nBC_Solution = BCs.shape[-1]
print('# Different Solutions: {}'.format(nSolutions))
print('# Samples per Solution: {}'.format(nSamples_Solution))
print('# Boundary conditions per Solution: {}'.format(nBC_Solution))

# Print the upper and lower bound of the data
print(np.min(BCs[:, :, :]))
pMin = min(np.min(Us[:, :, :]), np.min(BCs[:, :, :]))
pMax = max(np.max(Us[:, :, :]), np.max(BCs[:, :, :]))
print('lower, upper bound {:6f} {:6f}'.format(pMin, pMax))

# split traning and validation
nUsed = 2000
if (nUsed > Us.shape[0]):
  nUsed = Us.shape[0]
nValid = 200  # int(0.1 * nUsed)
nTrain = nUsed - nValid
nSolPerBatch = args.nSolPerBatch

# define generator for training and validation
#Points = np.zeros((nTrain, 2, args.nCollocPoint))
trainGen = PUC.bc_xyBC_xy_w__label(Us, BCs, args.nCollocPoint, args.nDataPoint,
                                   args.nBC_Solution, 0, nTrain, nSolPerBatch,
                                   normalize=True, Untrained_Model=True,
                                   Total_Avialble_Points_BC=4 * args.nBC_Solution)
validGen = PUC.bc_xyBC_xy_w__label(Us, BCs, args.nCollocPoint, args.nDataPoint,
                                   args.nBC_Solution, nTrain, nUsed, nSolPerBatch,
                                   normalize=True, Untrained_Model=True,
                                   Total_Avialble_Points_BC=4 * args.nBC_Solution)


# Compute the normalized regularization:
N_reg = None
if (args.reg is not None):
  Normalizing_Factor = np.prod(fcLayers)
  N_reg = np.ones_like(fcLayers) * args.reg[0] / Normalizing_Factor
  print('Using a regularization factor of {:.1e}'.format(
      args.reg[0] / Normalizing_Factor))
  modelName = 'pinn' + archStr + 'r{:.1e}_a{}_t{}_a{}_Dat{}_Coll{}'.format(
      args.reg[0], nSolPerBatch, nTrain, str(args.alpha), int(args.nDataPoint), int(args.nCollocPoint))
else:
  modelName = 'pinn' + archStr + 'a{}_t{}_a{}_Dat{}_Coll{}'.format(
      nSolPerBatch, nTrain, str(args.alpha), int(args.nDataPoint), int(args.nCollocPoint))


if args.model == 0:
  with psnModel.strategy.scope():
    psnNet = psnModel.PsnFC_xy(nBC=args.nBC_Solution, arch=fcLayers, reg=N_reg,
                               alpha=args.alpha, last_linear=True,
                               save_grad_stat=args.saveGradStat)
    psnNet.compile(optimizer=keras.optimizers.Adam(learning_rate=5.0e-4))

  auxNet = psnModel.PsnFC_xy(nBC=args.nBC_Solution, arch=fcLayers, reg=N_reg,
                             alpha=args.alpha, last_linear=True)
  modelName = modelName + '_Model_' + str(args.model)
elif(args.model == 1):
  with psnModel.strategy.scope():
    psnNet = psnModel.PsnXyFC_xy(nBC=args.nBC_Solution, arch=fcLayers, reg=N_reg,
                                 alpha=args.alpha, last_linear=True,
                                 save_grad_stat=args.saveGradStat)
    psnNet.compile(optimizer=keras.optimizers.Adam(learning_rate=5.0e-4))
  auxNet = psnModel.PsnXyFC_xy(nBC=args.nBC_Solution, arch=fcLayers, reg=N_reg,
                               alpha=args.alpha, last_linear=True,
                               save_grad_stat=args.saveGradStat)
  modelName = modelName + '_Model_' + str(args.model)
elif(args.model == 2):
  with psnModel.strategy.scope():
    psnNet = psnModel.PsnFC_xy_residual(nBC=args.nBC_Solution, arch=fcLayers, reg=N_reg,
                                        alpha=args.alpha, last_linear=True,
                                        save_grad_stat=args.saveGradStat)
    psnNet.compile(optimizer=keras.optimizers.Adam(learning_rate=5.0e-4))
  auxNet = psnModel.PsnFC_xy_residual(nBC=args.nBC_Solution, arch=fcLayers, reg=N_reg,
                                      alpha=args.alpha, last_linear=True,
                                      save_grad_stat=args.saveGradStat)
  modelName = modelName + '_Model_' + str(args.model)
elif(args.model == 3):
  with psnModel.strategy.scope():
    psnNet = psnModel.PsnXyFC_xy_residual(nBC=args.nBC_Solution, arch=fcLayers, reg=N_reg,
                                          alpha=args.alpha, last_linear=True,
                                          save_grad_stat=args.saveGradStat)
    psnNet.compile(optimizer=keras.optimizers.Adam(learning_rate=5.0e-4))
  auxNet = psnModel.PsnXyFC_xy_residual(nBC=args.nBC_Solution, arch=fcLayers, reg=N_reg,
                                        alpha=args.alpha, last_linear=True,
                                        save_grad_stat=args.saveGradStat)
  modelName = modelName + '_Model_' + str(args.model)

# callbacks
psnCallbacks = [keras.callbacks.ModelCheckpoint(filepath='../res/' + modelName + '/checkpoint',
                                                monitor='val_loss', save_best_only=True,
                                                save_weights_only=True, verbose=1),
                keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.8, patience=400,
                                                  min_lr=1.0e-6, min_delta=0.01),
                keras.callbacks.CSVLogger('../res/' + modelName + '.log', append=True)]

# initial training, using grid cells as collocation points
# psnNet.load_weights(tf.train.latest_checkpoint(modelName))
#keras.backend.set_value(psnNet.optimizer.learning_rate, 5.0e-4)

psnNet.fit(trainGen, validation_data=validGen, epochs=args.TrainEpochs,
           steps_per_epoch=nTrain // nSolPerBatch,
           validation_steps=nValid // nSolPerBatch,
           verbose=2, callbacks=psnCallbacks)
psnNet.summary()
