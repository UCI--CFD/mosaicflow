import sys
sys.path.append('../src')
import numpy as np
import argparse
from scipy.io import loadmat
import math as math


parser = argparse.ArgumentParser()
parser.add_argument('-r', '--reg', type=float, nargs='*', default=None,
                    help='l2 regularization')
parser.add_argument('-Epochs', '--TrainEpochs', type=int, default=10000,
                    help='Training epochs')
parser.add_argument('-s', '--nSolPerBatch', type=int, default=20,
                    help='number of solution per batch')
parser.add_argument('-f', '--file', default='../data/Laplace_2_64_Diff_Unif.mat',
                    help='data file')
parser.add_argument('-gpu', '--GPU', default=None, help='Specify GPU to use')
parser.add_argument('-dt', '--data_type', default='float64',
                    help='Select data type: float32 or float64')
parser.add_argument('-n', '--name', default='ConvNN',
                    help='Define name of the model')
parser.add_argument('-l', '--load_model',
                    default='../res/res_HPC3/ConvNN_Laplace_Model_Predict')
args = parser.parse_args()


if(args.GPU is not None):
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = str(args.GPU)

import tensorflow as tf
from tensorflow import keras
import pinn_convNN as CNN

assert args.data_type == 'float32' or 'float64', "Invalid spceified data type"

keras.backend.set_floatx(args.data_type)
if(args.data_type == 'float32'):
    data_type = tf.float32
else:
    data_type = tf.float64


# %%===========================================================================
# Compute the residuals
# =============================================================================

def Laplace_PDE(Samples):
    PDE = np.zeros((1, Samples, Samples))
    PDE = tf.convert_to_tensor(PDE)
    return PDE


def Poisson_PDE(Samples, width, height, Range):
    PDE = np.zeros((1, Samples, Samples))
    X = np.linspace(0, width, num=Samples)
    Y = np.linspace(0, height, num=Samples)
    for i_x, x in enumerate(X):
        for i_y, y in enumerate(Y):
            PDE[0, i_x, i_y] = (20 * math.pi**2 * np.sin(2 * math.pi * x)
                                * np.sin(4 * math.pi * y)) * (2 / Range)
    PDE[0, 0, :] = 0
    PDE[0, -1, :] = 0
    PDE[0, :, 0] = 0
    PDE[0, :, -1] = 0
    PDE = tf.convert_to_tensor(PDE)
    return PDE


def Extract_Border(Image, Samples):
    Border = np.zeros((1, np.shape(Image)[0], np.shape(Image)[1]))
    Border[0, 0, :] = Image[0, :]
    Border[0, -1, :] = Image[-1, :]
    Border[0, :, 0] = Image[:, 0]
    Border[0, :, -1] = Image[:, -1]
    return Border

# %%==========================================================================
# Load and preprocess the Data
# =============================================================================


Data = loadmat(args.file)

if "Laplace" in args.file:
    U = Data['u_save_uniform_homo']
    if(args.data_type == 'float32'):
        U = np.float32(U)
else:
    U = Data['u_save_uniform']
    if(args.data_type == 'float32'):
        U = np.float32(U)

Range = np.max(U) - np.min(U)
U_N = 2 * (U - np.min(U)) / Range - 1

# Extract interios
X = np.zeros((np.shape(U_N)[0], np.shape(U_N)[1], np.shape(U_N)[2], 1))
for i in range(np.shape(U_N)[0]):
    X[i, :, :, 0] = Extract_Border(U_N[i, :, :], np.shape(U_N)[-1])

Samples_Train = int(np.shape(U_N)[0] * 0.9)
Samples_Val = np.shape(U_N)[0] - Samples_Train
X_Train = X[0:Samples_Train, :, :, :]
X_Val = X[Samples_Train:Samples_Train + Samples_Val, :, :, :]

U_N = np.expand_dims(U_N, axis=-1)
Y_Train = U_N[0:Samples_Train, :, :, :]
Y_Val = U_N[Samples_Train:Samples_Train + Samples_Val, :, :, :]


# %%===========================================================================
# Compute the Residuals
# =============================================================================

if "Laplace" in args.file:
    RHS = Laplace_PDE(np.shape(U)[-1])
    if(args.data_type == 'float32'):
        RHS = np.float32(RHS)
else:
    RHS = Poisson_PDE(np.shape(U)[-1], 1, 1, Range)
    if(args.data_type == 'float32'):
        RHS = np.float32(RHS)
# %%===========================================================================
# Define Model Name
# =============================================================================

if "Laplace" in args.file:
    modelName = 'ConvNN_Laplace'
else:
    modelName = 'ConvNN_Poisson'

modelName = modelName + args.name

# %%===========================================================================
# Predefine the callbacks and the regularizer
# =============================================================================

# callbacks
CNNCallbacks = [keras.callbacks.ModelCheckpoint(filepath='../res/' + modelName + '/checkpoint',
                                                monitor='val_loss', save_best_only=True,
                                                save_weights_only=True, verbose=1),
                keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.8, patience=300,
                                                  min_lr=1.0e-6, min_delta=0.01),
                keras.callbacks.CSVLogger('../res/' + modelName + '.log', append=True)]

# Regularizer
if args.reg != None:
    Reg_N = args.reg[0] / 1e7
    Regularizer = tf.keras.regularizers.l2(Reg_N)
else:
    Regularizer = None


# %%===========================================================================
# Initiate saved model
# =============================================================================
keras.backend.set_floatx('float32')
CNN_Model_aux = CNN.ConvolutionalNN_64(Loss_Selected='Loss_1', RHS=np.float32(RHS),
                                       activation='tanh', Range=1, Regularizer=Regularizer,
                                       N1=64, N2=64, data_type=tf.float32, Compute_Residual=True,
                                       beta=0)
# Load the float32 Model
CNN_Model_aux.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=5.0e-4))
CNN_Model_aux.load_weights(tf.train.latest_checkpoint(args.load_model))
CNN_Model_aux.evaluate(np.float32(np.zeros((1, 64, 64, 1))),
                       np.float32(np.zeros((1, 64, 64, 1))), verbose=2)
# Retireve the weights from it
weights = CNN_Model_aux.get_weights()
# print(weights)

# %%===========================================================================
# Initiate saved model
# =============================================================================
keras.backend.set_floatx(args.data_type)
CNN_Model = CNN.ConvolutionalNN_64(Loss_Selected='Loss_1', RHS=RHS,
                                   activation='tanh', Range=1, Regularizer=Regularizer,
                                   N1=64, N2=64, data_type=data_type, Compute_Residual=True,
                                   beta=0)
# Load the desired model Model
CNN_Model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=5.0e-4))

CNN_Model.evaluate(np.zeros((1, 64, 64, 1)),
                   np.zeros((1, 64, 64, 1)), verbose=2)
# Change the weights format
weights = [w.astype(tf.keras.backend.floatx()) for w in weights]

# Load the weights into the model
CNN_Model.set_weights(weights)
CNN_Model.evaluate(np.zeros((1, 64, 64, 1)),
                   np.zeros((1, 64, 64, 1)), verbose=2)

# %%===========================================================================
# Train the model
# =============================================================================

CNN_Model.fit(X_Train, Y_Train, validation_data=(X_Val, Y_Val), epochs=args.TrainEpochs,
              verbose=1, callbacks=CNNCallbacks, batch_size=125)
print(CNN_Model.summary())
