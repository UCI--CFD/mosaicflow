import tensorflow as tf
import numpy as np


def bc_xyBC_xy_w__label(U, BCs, Coll_points, Dat_points, BC_points, solBegin, solEnd,
                        nSolPerBatch, normalize=True, Untrained_Model=True, Points=None,
                        Total_Avialble_Points_BC=None):
    '''
    inputs:
            U = Matrix where the data is stored [x,y,U] shape(Sols, 3, Dat)
            BCs = Matrix where the boundary conditions are stored [xBC,yBC,U_BC]
            Dat_points = number of points in the inner domain, dat
            Coll_points = number of collocation points in the inner domain
            BC_points = number of points in the boundary 
            solBegin = Initial index for the solutions
            solEnd = Final index for the solutions
            nSolPerBatch = Number of solutions per batch
            nDataPOint1d = ---------- To be determined
            normalize = Bool. If the data needs to be normalized
            Untrained_Model = Bool. 
                True:   First iteration of the training
                    Points = None
                False:  Not the first iteration. The collocation points
                                                can be found stored at Points
                    Points = Collocation points [x,y] shape(solBatch,2,Coll_points)
    outputs: 
            [bc, xyBc, xy,w], label

    '''
    # Determine normalizing parameters
    pMax = max(np.max(U), np.max(BCs))
    pMin = min(np.min(U), np.min(BCs))

    # Determine the total points per solution -----------
    if (Dat_points > U.shape[-1]):
        Dat_points = U.shape[-1]
    if (BC_points > BCs.shape[-1]):
        BC_points = BCs.shape[-1]
    if(Untrained_Model):
        if(Coll_points > U.shape[-1]):
            Coll_points = U.shape[-1]
    else:
        if(Coll_points > Points.shape[-1]):
            Coll_points = Points.shape[-1]

    # Determine the total available BC per solution -------------
    if (Total_Avialble_Points_BC is None):
        Total_Avialble_Points_BC = BCs.shape[-1]
    Total_Avialble_Points_U = U.shape[-1]

    # Determine the number of points per solution
    nPntPerSol = Total_Avialble_Points_BC + Dat_points + Coll_points

    # Determine the batch size ---------------------------
    batchSize = nSolPerBatch * nPntPerSol

    # allocate inputs for network -----------------------
    # bc   - values at boundary points
    # xyBc - (x, y) of boundary points
    bc = np.zeros((batchSize, BC_points))
    xyBc = np.zeros((batchSize, BC_points, 2))
    # xy   - (x, y) of the input points, including data and collocation
    xy = np.zeros((batchSize, 2))
    # w    - marker to distinguish data and collocation
    w = np.zeros((batchSize, 2))
    # label - Outputs of the model
    label = np.zeros((batchSize))

    # Determine the w marker -------------------------------
    for m in range(0, batchSize, nPntPerSol):
        s, e = m, m + Total_Avialble_Points_BC + Dat_points
        w[s:e, 0] = 1.0
        w[s:e, 1] = 0.0
        s, e = e, m + nPntPerSol
        w[s:e, 0] = 0.0
        w[s:e, 1] = 1.0

    # Print Basic Information ---------------------------
    print('--------------------------------')
    print('generator:')
    print('{} boundary points'.format(BC_points))
    print('{} data points'.format(Dat_points))
    print('{} collocation points'.format(Coll_points))
    print('output [bc, xyBc, xy, w], label')
    print('--------------------------------')

    solBatchBegin = solBegin
    index_BC = 0
    index_Dat = 0
    index_Coll = 0
    while 1:
        if solBatchBegin + nSolPerBatch > solEnd:
            solBatchBegin = solBegin
        for s in range(solBatchBegin, solBatchBegin + nSolPerBatch):
            ss = s - solBatchBegin

            # bc and xyBC for this solution
            for m in range(ss * nPntPerSol, (ss + 1) * nPntPerSol):
                if (index_BC + BC_points <= Total_Avialble_Points_BC):
                    bc[m, :] = BCs[s, 2, index_BC: index_BC + BC_points]
                    xyBc[m, :, :] = np.transpose(
                        BCs[s, 0:2, index_BC:(index_BC + BC_points)])
                else:
                    index_bookmark = np.mod(
                        index_BC + BC_points, Total_Avialble_Points_BC)
                    Sample_Division = BC_points - index_bookmark
                    bc[m, 0:Sample_Division] = BCs[s, 2,
                                                   index_BC: Total_Avialble_Points_BC]
                    bc[m, Sample_Division:BC_points] = BCs[s,
                                                           2, 0: index_bookmark]
                    xyBc[m, 0:Sample_Division, :] = np.transpose(
                        BCs[s, 0:2, index_BC: Total_Avialble_Points_BC])
                    xyBc[m, Sample_Division:BC_points, :] = np.transpose(
                        BCs[s, 0:2, 0: index_bookmark])

                index_BC = np.mod(index_BC + BC_points,
                                  Total_Avialble_Points_BC)

            # label's and xy's boundary part
            start = ss * nPntPerSol
            label[start:start + Total_Avialble_Points_BC] = BCs[s,
                                                                2, 0:Total_Avialble_Points_BC]
            xy[start:start + Total_Avialble_Points_BC,
                :] = np.transpose(BCs[s, 0:2, 0:Total_Avialble_Points_BC])

            # label's and xy's data part if any
            if Dat_points != 0:
                start = ss * nPntPerSol + Total_Avialble_Points_BC
                if(index_Dat + Dat_points <= Total_Avialble_Points_U):
                    label[start:start + Dat_points] = U[s,
                                                        2, index_Dat: index_Dat + Dat_points]
                    xy[start:start + Dat_points,
                        :] = np.transpose(U[s, 0:2, index_Dat: index_Dat + Dat_points])
                else:
                    index_bookmark = np.mod(
                        index_Dat + Dat_points, Total_Avialble_Points_U)
                    Sample_Division = Dat_points - index_bookmark
                    label[start:start + Sample_Division] = U[s,
                                                             2, index_Dat: Total_Avialble_Points_U]
                    label[start + Sample_Division:start +
                          Dat_points] = U[s, 2, 0: index_bookmark]
                    xy[start:start + Sample_Division,
                        :] = np.transpose(U[s, 0:2, index_Dat: Total_Avialble_Points_U])
                    xy[start + Sample_Division:start +
                        Dat_points] = np.transpose(U[s, 0:2, 0: index_bookmark])
                index_Dat = np.mod(index_Dat + Dat_points,
                                   Total_Avialble_Points_U)

            # label's collocation part, no solution them
            start = ss * nPntPerSol + Total_Avialble_Points_BC + Dat_points
            label[start:start + Coll_points] = 0.0
            if(Untrained_Model):
                if(index_Coll + Coll_points <= Total_Avialble_Points_U):
                    xy[start:start + Coll_points,
                        :] = np.transpose(U[s, 0:2, index_Coll:index_Coll + Coll_points])
                else:
                    index_bookmark = np.mod(
                        index_Coll + Coll_points, Total_Avialble_Points_U)
                    Sample_Division = Coll_points - index_bookmark
                    xy[start:start + Sample_Division,
                        :] = np.transpose(U[s, 0:2, index_Coll:Total_Avialble_Points_U])
                    xy[start + Sample_Division:start + Coll_points,
                        :] = np.transpose(U[s, 0:2, 0:index_bookmark])

                index_Coll = np.mod(index_Coll + Coll_points,
                                    Total_Avialble_Points_U)
            else:
                xy[start:start + Coll_points,
                    :] = np.transpose(Points[s, 2, 0:Coll_points])
        # normalization
        if normalize:
            bc = (bc - pMin) / (pMax - pMin) * 2 - 1
            label = (label - pMin) / (pMax - pMin) * 2 - 1

        # move to next batch
        solBatchBegin += nSolPerBatch
        # return based on model type
        yield [bc, xyBc, xy, w], label


@tf.function
def max_abs_grad(grads):
    gMaxList = [tf.reduce_max(tf.abs(g)) for g in grads]
    return tf.reduce_max(tf.stack(gMaxList))


@tf.function
def avg_abs_grad(grads):
    gSumList = [tf.reduce_sum(tf.abs(g)) for g in grads]
    ngList = [tf.reduce_sum(tf.size(g)) for g in grads]
    gSum = tf.add_n(gSumList)
    ng = tf.add_n(ngList)
    return gSum / tf.cast(ng, tf.float32)


def BcXybcXydupGen(p, solBegin, solEnd, nSolPerBatch, bcXyOnly=True, length=1.0):
    nCell = p.shape[1] - 2
    nBcCell = 4 * nCell
    nInCell = nCell * nCell
    nCellPerBatch = nInCell
    h = length / nCell
    pMax = np.amax(p)
    pMin = np.amin(p)
    batchSize = nSolPerBatch * nCellPerBatch
    bc = np.zeros((batchSize, nBcCell))
    solBc = np.zeros(nBcCell)
    xy = np.zeros((batchSize, nBcCell, 2))
    xyIn = np.zeros((nInCell, 2))
    xyBc = np.zeros((batchSize, nBcCell, 2))
    label = np.zeros((batchSize))
    # weights, xyBc, and xy does not change with solutions.
    # Those data duplicate for each solution, even each cell, which are highly
    # ineffecient for memory allocation and needs to be optimized asap.
    # boundary coordinates
    for m in range(batchSize):
        xyBc[m, :nCell, 0] = 0.0
        xyBc[m, :nCell, 1] = (np.arange(nCell) + 0.5) * h
        xyBc[m, nCell:2 * nCell, 0] = (np.arange(nCell) + 0.5) * h
        xyBc[m, nCell:2 * nCell, 1] = 1.0
        xyBc[m, 2 * nCell:3 * nCell, 0] = 1.0
        xyBc[m, 2 * nCell:3 * nCell, 1] = (np.arange(nCell, 0, -1) - 0.5) * h
        xyBc[m, 3 * nCell:, 0] = (np.arange(nCell, 0, -1) - 0.5) * h
        xyBc[m, 3 * nCell:, 1] = 0.0
    # inner coordinates
    m = 0
    for i in range(1, nCell + 1):
        for j in range(1, nCell + 1):
            xyIn[m, :] = [(i - 0.5) * h, (j - 0.5) * h]
            m += 1
    # colloction points' cooridates
    for m in range(0, batchSize,  nCellPerBatch):
        for mm in range(nBcCell):
            # inner
            xy[m:m + nCellPerBatch, mm, :] = xyIn[:, :]
    # set bc and label per solution
    solBatchBegin = solBegin
    while 1:
        if solBatchBegin + nSolPerBatch > solEnd:
            solBatchBegin = solBegin
        for s in range(solBatchBegin, solBatchBegin + nSolPerBatch):
            ss = s - solBatchBegin
            # bc for this solution
            solBc[:nCell] = 0.5 * (p[s, 0, 1:-1] + p[s, 1, 1:-1])
            solBc[nCell:2 * nCell] = 0.5 * (p[s, 1:-1, -1] + p[s, 1:-1, -2])
            solBc[2 * nCell:3 * nCell] = 0.5 * \
                (p[s, -1, -2:0:-1] + p[s, -2, -2:0:-1])
            solBc[3 * nCell:] = 0.5 * (p[s, -2:0:-1, 0] + p[s, -2:0:-1, 1])
            for m in range(ss * nCellPerBatch, (ss + 1) * nCellPerBatch):
                bc[m, :] = solBc[:]
            # label
            start = ss * nCellPerBatch
            label[start:start + nInCell] = p[s, 1:-1, 1:-1].reshape((nInCell,))
        bc = (bc - pMin) / (pMax - pMin)
        label = (label - pMin) / (pMax - pMin)
        solBatchBegin += nSolPerBatch

        # return based on model type
        if bcXyOnly:
            yield [bc, np.squeeze(xy[:, 0, :])], label
        else:
            yield [bc, xyBc, xy], label


# distribute collocation points
def adapt_collocation(gradNorm, xy, solBegin, stride=2, height=1.0,
                      width=1.0, nearBoundary=None):
    '''
    inputs:
        gradNorm: Information about the gradient 
        xy = Points where the information is available, shape(Batchsize, 2, nCollocation)
        solBegin = Index to start choosing the solutions
        stride = jump across the solutions
        height = height of the rectangular volume
        width = width of the regtangular volume
        nearBoundary = Place more points near the boundaries

    '''
    nPoint = xy.shape[-1]
    nSol = gradNorm.shape[0]
    nCell = gradNorm.shape[1]
    h1 = width / nCell
    h2 = height / nCell

    for s in range(solBegin, solBegin + nSol):
        ss = s - solBegin
        # get number of points in each cell
        nSampleCell = (nCell // stride) * (nCell // stride)
        nPntInCell = np.zeros(nSampleCell, dtype=int)
        density = gradNorm[ss, :, :] / np.sum(gradNorm[ss, :, :])
        if nearBoundary != None:
            density = np.power(density, nearBoundary)
            density = density / np.sum(density)
        m = 0
        for i in range(0, nCell, stride):
            for j in range(0, nCell, stride):
                # number of points in current cell: stride x stride
                cellDensity = np.sum(density[i:i + stride, j:j + stride])
                nPntInCell[m] = int(np.rint(cellDensity * nPoint))
                m += 1
        # adjust number of points to fit nPoint
        diff = nPoint - np.sum(nPntInCell)
        m = 0
        if diff > 0:
            while diff > 0:
                nPntInCell[m] += 1
                diff -= 1
                m = (m + 1) % nSampleCell
        else:
            while diff < 0:
                if nPntInCell[m] > 1:
                    nPntInCell[m] -= 1
                    diff += 1
            m = (m + 1) % nSampleCell
        # generate points
        m = 0
        pntStart, pntEnd = 0, 0
        for i in range(0, nCell, stride):
            for j in range(0, nCell, stride):
                pntStart, pntEnd = pntEnd, pntEnd + nPntInCell[m]
                xy[s, 0, pntStart:pntEnd] = np.random.rand(
                    nPntInCell[m], 1) * stride * h1 + i * h1
                xy[s, 1, pntStart:pntEnd] = np.random.rand(
                    nPntInCell[m], 1) * stride * h2 + j * h2
                m += 1
