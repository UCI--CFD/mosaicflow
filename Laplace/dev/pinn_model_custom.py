import tensorflow as tf
from tensorflow import keras
import tensorflow.keras.layers as KL
import tensorflow.keras.regularizers as KR
import numpy as np
import pinn_utils_custom as PCU

keras.backend.set_floatx('float32')

strategy = tf.distribute.MirroredStrategy()

#-------------------------------------------------------------------------------
# model subclasses, fully connected
# Input:  bc   - values at boundary points
#         xyBc - (x, y) of boundary points
#         xy   - (x, y) of the input points, including data and collocation
#         w    - marker to distinguish data and collocation
# Output: p @ xy
#-------------------------------------------------------------------------------

class PsnFC_xy(keras.Model):

    def __init__(self, nBC=256, arch=[256, 256, 128, 128, 1], reg=None,
                 act='tanh', alpha=0.01, last_linear=True,
                 save_grad_stat=False, **kwargs):

        super(PsnFC_xy, self).__init__(**kwargs)

        # check inputs
        assert len(arch) > 0
        if reg is not None:
            assert len(reg) == len(arch)

        # save inputs
        self.nBC = nBC
        self.alpha = alpha
        self.act = act
        self.arch = arch
        self.reg = np.zeros(len(arch)) if reg is None else np.array(reg)
        self.save_grad_stat = save_grad_stat
        self.last_linear = last_linear

        # Set up mlp
        self.mlp = []
        for i, w in enumerate(self.arch[:-1]):
            self.mlp.append(KL.Dense(w, name='mlp' + repr(i), activation=act,
                                     kernel_regularizer=KR.l2(self.reg[i])))
        if last_linear:
            self.mlp.append(KL.Dense(arch[-1], name='mlp' + repr(len(self.arch) - 1), activation='linear',
                                     kernel_regularizer=KR.l2(self.reg[-1])))
        else:
            self.mlp.append(KL.Dense(arch[-1], name='mlp' + repr(len(self.arch) - 1),
                                     kernel_regularizer=KR.l2(self.reg[i]),
                                     activation=act))

        # ---- dicts for metrics and statistics ---- #
        self.trainMetrics = {}
        self.validMetrics = {}
        # add metrics for layers' weights, if save_grad_stat is required
        # i even for weights, odd for bias
        if self.save_grad_stat:
            for i in range(len(arch)):
                names = ['dat_' + repr(i) + 'w_avg', 'dat_' + repr(i) + 'w_std',
                         'dat_' + repr(i) + 'b_avg', 'dat_' +
                         repr(i) + 'b_std',
                         'pde_' + repr(i) + 'w_avg', 'pde_' +
                         repr(i) + 'w_std',
                         'pde_' + repr(i) + 'b_avg', 'pde_' + repr(i) + 'b_std']

                for name in names:
                    self.trainMetrics[name] = keras.metrics.Mean(
                        name='train ' + name)

        ## loss, mae
        names = ['loss', 'dat', 'pde']
        for key in names:
            self.trainMetrics[key] = keras.metrics.Mean(
                name='train ' + key)
            self.validMetrics[key] = keras.metrics.Mean(
                name='valid ' + key)
            self.trainMetrics['mae'] = keras.metrics.MeanAbsoluteError(
                name='mae')
            self.validMetrics['mae'] = keras.metrics.MeanAbsoluteError(
                name='mae')
        # dict to save training and validation statuse
        self.trainStat = {}
        self.validStat = {}

    def call(self, inputs, training=None):
        '''
        inputs: [p[batch,nBcCell], xy[batch,2]]
        [bc[batchsize,BCpoints], xyBc[batchSize, BCpoints,2], xy[batchSize,2],w[batchSize,2]]
        '''
        bc = inputs[0]
        xyBC = inputs[1]
        xy = inputs[2]
        bc_xyBC_xy = tf.concat([bc, xyBC[:, :, 0], xyBC[:, :, 1], xy], axis=-1)
        for layer in self.mlp:
            bc_xyBC_xy = layer(bc_xyBC_xy)
        return bc_xyBC_xy

    def record_layer_gradient(self, grads, baseName):
        '''
        record the average and standard deviation of each layer's 
        weights and biases
        '''
        for i, g in enumerate(grads):
            if g != None:
                l = i // 2
                parameter = 'w' if i % 2 == 0 else 'b'
                prefix = baseName + '_{:d}{}_'.format(l, parameter)
                gAbs = tf.abs(g)
                gAvg = tf.reduce_mean(gAbs)
                gStd = tf.reduce_mean(tf.square(gAbs - gAvg))
                self.trainMetrics[prefix + 'avg'].update_state(gAvg)
                self.trainMetrics[prefix + 'std'].update_state(gStd)

    def train_step(self, data):

        bc_xyBC_xy_w, p = data
        bc, xyBC, xy, w = bc_xyBC_xy_w
        with tf.GradientTape(persistent=True) as tape0:
                        # tape2: 2nd order derivative
            with tf.GradientTape(watch_accessed_variables=False, persistent=True) as tape2:
                tape2.watch(xy)
                # tape1: 1st order derivatives
                with tf.GradientTape(watch_accessed_variables=False) as tape1:
                    tape1.watch(xy)
                    pPred = self([bc, xyBC, xy])
                    pPred = tf.squeeze(pPred)
                g = tape1.gradient(pPred, xy)
                p_x = g[:, 0]
                p_y = g[:, 1]
            p_xx = tape2.gradient(p_x, xy)[:, 0]
            p_yy = tape2.gradient(p_y, xy)[:, 1]
            del tape2
            # bc mse + pde mse
            datMse = tf.reduce_sum(tf.multiply(w[:, 0], tf.square(p - pPred)))\
                / (tf.reduce_sum(w[:, 0]) + 1.0e-10)
            pdeMse = tf.reduce_sum(tf.multiply(w[:, 1], tf.square(p_xx + p_yy)))\
                / (tf.reduce_sum(w[:, 1]) + 1.0e-10)
            # replica's loss, divided by global batch size
            loss = datMse + self.alpha * pdeMse
            loss += tf.add_n(self.losses)
            loss = loss / strategy.num_replicas_in_sync

            # update gradients and trainable variables
        if self.save_grad_stat:
            datGrad = tape0.gradient(datMse, self.trainable_variables)
            pdeGrad = tape0.gradient(pdeMse, self.trainable_variables)
        lossGrad = tape0.gradient(loss, self.trainable_variables)
        del tape0
        # ---- update parameters ---- #
        self.optimizer.apply_gradients(zip(lossGrad, self.trainable_variables))

        # Update the metrics
        self.trainMetrics['loss'].update_state(
            loss * strategy.num_replicas_in_sync)
        self.trainMetrics['dat'].update_state(datMse)
        self.trainMetrics['pde'].update_state(pdeMse)
        self.trainMetrics['mae'].update_state(p, pPred)

        # Track gradient coefficients
        if self.save_grad_stat:
            self.record_layer_gradient(datGrad, 'dat')
            self.record_layer_gradient(pdeGrad, 'pde')

        for key in self.trainMetrics:
            self.trainStat[key] = self.trainMetrics[key].result()

        return self.trainStat

    def test_step(self, data):
        bc_xyBC_xy_w, p = data
        bc, xyBC, xy, w = bc_xyBC_xy_w

        # ---- compute data, pde mse ---- #
        with tf.GradientTape(watch_accessed_variables=False, persistent=True) as tape2:
            tape2.watch(xy)
            with tf.GradientTape(watch_accessed_variables=False) as tape1:
                tape1.watch(xy)
                pPred = self([bc, xyBC, xy])
                pPred = tf.squeeze(pPred)
            g = tape1.gradient(pPred, xy)
            p_x = g[:, 0]
            p_y = g[:, 1]
        p_xx = tape2.gradient(p_x, xy)[:, 0]
        p_yy = tape2.gradient(p_y, xy)[:, 1]
        del tape2
        # bc mse + pde mse
        datMse = tf.reduce_sum(tf.multiply(w[:, 0], tf.square(p - pPred)))\
            / (tf.reduce_sum(w[:, 0]) + 1.0e-10)
        pdeMse = tf.reduce_sum(tf.multiply(w[:, 1], tf.square(p_xx + p_yy)))\
            / (tf.reduce_sum(w[:, 1]) + 1.0e-10)

        # ---- update metrics and statistics ---- #
        self.validMetrics['loss'].update_state(datMse + self.alpha * pdeMse)
        self.validMetrics['pde'].update_state(pdeMse)
        self.validMetrics['dat'].update_state(datMse)
        self.validMetrics['mae'].update_state(p, pPred)
        for key in self.validMetrics:
            self.validStat[key] = self.validMetrics[key].result()

        return self.validStat

    def reset_metrics(self):
        '''
        Overwrite base class's function, this will be called at 
        every epoch's end
        '''
        for key in self.trainMetrics:
            self.trainMetrics[key].reset_states()
        for key in self.validMetrics:
            self.validMetrics[key].reset_states()

    def summary(self):
        nVar = 0
        for t in self.trainable_variables:
            print(t.name, t.shape)
            nVar += tf.reduce_prod(t.shape)
            print('{} trainalbe variables'.format(nVar))

    def preview(self):
        print('--------------------------------')
        print('model preview')
        print('--------------------------------')
        print('fully connected network:')
        print(self.width)
        print('last layer linear: {}'.format(self.last_linear))
        print('activation: ' + self.act)
        print('layer regularization')
        print(self.reg)
        print('regularize pde: {}'.format(self.alpha))
        print('collect gradient statistics: {}'.format(self.save_grad_stat))
        print('--------------------------------')

    def grad_xy_norm(self, inputs, nCell, nBc):
        '''
        inputs [bc[batch, nBcCell], xy[batch, 2]], label
        '''
        bc = tf.convert_to_tensor(inputs[0][0])
        xyBC = tf.convert_to_tensor(inputs[0][1])
        xy = tf.convert_to_tensor(inputs[0][2])
        # 1st ordre derivatives
        with tf.GradientTape(watch_accessed_variables=False) as tape1:
            tape1.watch(xy)
            pPred = self([bc, xyBC, xy])
            pPred = tf.squeeze(pPred)
            g = tape1.gradient(pPred, xy)
        # sum the derivatives from boundary points
        gNorm = tf.sqrt(tf.square(g[:, 0]) + tf.square(g[:, 1]))

        # convert to numpy array of shape nBc*nCell*nCell
        return np.reshape(gNorm.numpy(), (nBc, nCell, nCell))


#-------------------------------------------------------------------------------
# model subclasses, fully connected
# Input:  bc   - values at boundary points
#         xyBc - (x, y) of boundary points
#         xy   - (x, y) of the input points, including data and collocation
#         w    - marker to distinguish data and collocation
# Output: p @ xy
# Note that I assume bc's effect is linear, so it is not passed to the MLP
# Only xy is the input to the MLP, output are the coefficients timed with bc
# elementwisely.
#-------------------------------------------------------------------------------

class PsnXyFC_xy(PsnFC_xy):
    def __init__(self, **kwargs):
        super(PsnXyFC_xy, self).__init__(**kwargs)

        assert self.arch[-1] == self.nBC
        assert self.arch[0] == self.nBC * 2

    def call(self, inputs, training=None):
        bc = inputs[0]
        xyBC = inputs[1]
        xy = inputs[2]
        xyBC_xy = tf.concat([xyBC[:, :, 0], xyBC[:, :, 1], xy], axis=-1)
        for layer in self.mlp:
            xyBC_xy = layer(xyBC_xy)
        return tf.reduce_sum(tf.multiply(bc, xyBC_xy), axis=-1)


#-------------------------------------------------------------------------------
# model subclasses, fully connected
# Input:  bc   - values at boundary points
#         xyBc - (x, y) of boundary points
#         xy   - (x, y) of the input points, including data and collocation
#         w    - marker to distinguish data and collocation
# Output: p @ xy
# Note that I assume bc's effect is linear, so it is not passed to the MLP
# Only xy is the input to the MLP, output are the coefficients timed with bc
# elementwisely.
#-------------------------------------------------------------------------------

class PsnFC_xy_residual(PsnFC_xy):
    def __init__(self, **kwargs):
        super(PsnFC_xy_residual, self).__init__(**kwargs)

        assert self.arch[-1] == 1

    def call(self, inputs, training=None):

        bc = inputs[0]
        xyBC = inputs[1]
        xy = inputs[2]
        bc_xyBC_xy = tf.concat([bc, xyBC[:, :, 0], xyBC[:, :, 1]], axis=-1)
        for layer in self.mlp:
            bc_xyBC_xy = layer(tf.concat([bc_xyBC_xy, xy], axis=-1))
        return bc_xyBC_xy


#-------------------------------------------------------------------------------
# model subclasses, fully connected with residual connections
# Input:  bc   - values at boundary points
#         xyBc - (x, y) of boundary points
#         xy   - (x, y) of the input points, including data and collocation
#         w    - marker to distinguish data and collocation
# Output: p @ xy
# Note that I assume bc's effect is linear, so it is not passed to the MLP
# Only xy is the input to the MLP, output are the coefficients timed with bc
# elementwisely.
#-------------------------------------------------------------------------------


class PsnXyFC_xy_residual(PsnFC_xy):
    def __init__(self, **kwargs):
        super(PsnXyFC_xy_residual, self).__init__(**kwargs)

        assert self.arch[-1] == self.nBC
        assert self.arch[0] == self.nBC * 2

    def call(self, inputs, training=None):
        '''
        inputs: [p[batch,nBcCell], xy[batch,2]]
        [bc[batchsize,BCpoints], xyBc[batchSize, BCpoints,2], xy[batchSize,2],w[batchSize,2]]
        '''
        bc = inputs[0]
        xyBC = inputs[1]
        xy = inputs[2]
        bc_xyBC_xy = tf.concat([bc, xyBC[:, :, 0], xyBC[:, :, 1]], axis=-1)
        for layer in self.mlp:
            bc_xyBC_xy = layer(tf.concat([bc_xyBC_xy, xy], axis=-1))
        return tf.reduce_sum(tf.multiply(bc, bc_xyBC_xy), axis=-1)
