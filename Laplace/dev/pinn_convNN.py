import tensorflow as tf
import math as math
from tensorflow import keras
import numpy as np


# =============================================================================
# Metrics
# =============================================================================


# Usual metrics computed at the end of the model
Mae = keras.metrics.MeanAbsoluteError(name='mae')
Loss = keras.metrics.Mean(name='loss')
Predict_mse = keras.metrics.Mean(name='Pred_Mse')
Residual_mse = keras.metrics.Mean(name='Res_Mse')
BC_mse = keras.metrics.Mean(name='BC_Mse')

# Extra information to take into account
Predict_GradNorm = keras.metrics.Mean(name='Predict_GradNorm')
Residual_GradNorm = keras.metrics.Mean(name='Residual_GradNorm')
Cos = keras.metrics.Mean(name='cos_Pred_Res')
Predict_Middle_GradNorm = keras.metrics.Mean(name='Predict_GradNorm_Middle')
Residual_Middle_GradNorm = keras.metrics.Mean(name='Residual_GradNorm_Middle')
Cos_Middle = keras.metrics.Mean(name='cos_Middle_Pred_Res')


# =============================================================================
# Convolutional NN 64x64 Images
# =============================================================================


class ConvolutionalNN_64(keras.Model):

    def __init__(self, Loss_Selected='Loss_0', RHS=None,
                 activation='tanh', Range=1, Regularizer=None,
                 N1=64, N2=64, HL=10, N=60, Compute_Residual=False,
                 beta=1e-6, data_type=tf.float32, **kwargs):
        super(ConvolutionalNN_64, self).__init__(**kwargs)

        # UNet Structure
        self.N1 = N1
        self.N2 = N2
        self.activation = activation
        self.Loss_Selected = Loss_Selected
        self.RHS = RHS
        self.Range = Range
        self.Regularizer = Regularizer
        self.HL = HL
        self.N = N
        self.Compute_Residual = Compute_Residual
        if (self.Loss_Selected != 'Loss_0'):
            self.Compute_Residual = True
        self.beta = beta
        self.data_type = data_type

        self.Layer_Conv2D_1 = tf.keras.layers.Conv2D(16, 3, activation=self.activation,
                                                     kernel_regularizer=self.Regularizer,
                                                     bias_regularizer=self.Regularizer)
        self.Layer_Conv2D_11 = tf.keras.layers.Conv2D(16, 3, activation=self.activation,
                                                      kernel_regularizer=self.Regularizer,
                                                      bias_regularizer=self.Regularizer)
        self.Layer_Conv2D_2 = tf.keras.layers.Conv2D(32, 5, activation=self.activation,
                                                     kernel_regularizer=self.Regularizer,
                                                     bias_regularizer=self.Regularizer,
                                                     )
        self.Layer_Conv2D_22 = tf.keras.layers.Conv2D(32, 3, activation=self.activation,
                                                      kernel_regularizer=self.Regularizer,
                                                      bias_regularizer=self.Regularizer)
        self.Layer_Conv2D_3 = tf.keras.layers.Conv2D(64, 3, activation=self.activation,
                                                     kernel_regularizer=self.Regularizer,
                                                     bias_regularizer=self.Regularizer,
                                                     )
        self.Layer_Conv2D_32 = tf.keras.layers.Conv2D(64, 3, activation=self.activation,
                                                      kernel_regularizer=self.Regularizer,
                                                      bias_regularizer=self.Regularizer)
        self.Layer_Conv2D_4 = tf.keras.layers.Conv2D(128, 3, activation=self.activation,
                                                     kernel_regularizer=self.Regularizer,
                                                     bias_regularizer=self.Regularizer)

        self.Layer_Conv2DTrans_4 = tf.keras.layers.Conv2DTranspose(128, 3, activation=self.activation,
                                                                   kernel_regularizer=self.Regularizer,
                                                                   bias_regularizer=self.Regularizer)
        self.Layer_Conv2DTrans_3 = tf.keras.layers.Conv2DTranspose(64, 3, activation=self.activation,
                                                                   kernel_regularizer=self.Regularizer,
                                                                   bias_regularizer=self.Regularizer)
        self.Layer_Conv2DTrans_32 = tf.keras.layers.Conv2DTranspose(64, 3, activation=self.activation,
                                                                    kernel_regularizer=self.Regularizer,
                                                                    bias_regularizer=self.Regularizer)
        self.Layer_Conv2DTrans_2 = tf.keras.layers.Conv2DTranspose(32, 3, activation=self.activation,
                                                                   kernel_regularizer=self.Regularizer,
                                                                   bias_regularizer=self.Regularizer)
        self.Layer_Conv2DTrans_22 = tf.keras.layers.Conv2DTranspose(32, 5, activation=self.activation,
                                                                    kernel_regularizer=self.Regularizer,
                                                                    bias_regularizer=self.Regularizer)
        self.Layer_Conv2DTrans_1 = tf.keras.layers.Conv2DTranspose(16, 3, activation=self.activation,
                                                                   kernel_regularizer=self.Regularizer,
                                                                   bias_regularizer=self.Regularizer)
        self.Layer_Conv2DTrans_11 = tf.keras.layers.Conv2DTranspose(16, 3, activation=self.activation,
                                                                    kernel_regularizer=self.Regularizer,
                                                                    bias_regularizer=self.Regularizer)
        self.Layer_Conv2DTrans_Output = tf.keras.layers.Conv2DTranspose(1, 1, activation=self.activation,
                                                                        kernel_regularizer=self.Regularizer,
                                                                        bias_regularizer=self.Regularizer)
        self.Pooling_Layer = tf.keras.layers.AveragePooling2D((2, 2))
        self.UpSampling_Layer = tf.keras.layers.UpSampling2D(
            (2, 2), interpolation='bilinear')

        self.UpSampling_Layer = tf.keras.layers.UpSampling2D(
            (2, 2), interpolation='bilinear')
        self.Flatten = tf.keras.layers.Flatten()
        self.Reshape = tf.keras.layers.Reshape((2, 2, 128))

        self.Block_Structure = []
        for i in range(self.HL):
            Aux_Layer = tf.keras.layers.Dense(self.N, activation=self.activation,
                                              kernel_regularizer=self.Regularizer,
                                              bias_regularizer=self.Regularizer
                                              )

            self.Block_Structure.append(Aux_Layer)
        self.Block_Structure.append(tf.keras.layers.Dense(2 * 2 * 128, activation=self.activation,
                                                          kernel_regularizer=self.Regularizer,
                                                          bias_regularizer=self.Regularizer
                                                          ))

        Displace_up_right = np.zeros((self.N1, self.N2))
        Displace_down_left = np.zeros((self.N1, self.N2))
        Inner_points = np.zeros((self.N1, self.N2))

        for i in range(self.N1):
            for j in range(self.N2):
                if (i == np.mod(j - 1, self.N1)):
                    Displace_up_right[i, j] = 1
                if(np.mod(i - 1, self.N1) == j):
                    Displace_down_left[i, j] = 1
                if(i == j and i > 0 and i < self.N1 - 1):
                    Inner_points[i, j] = 1
        self.Displace_up_right = tf.convert_to_tensor(
            Displace_up_right, dtype=self.data_type)
        self.Displace_down_left = tf.convert_to_tensor(
            Displace_down_left, dtype=self.data_type)
        self.Inner_points = tf.convert_to_tensor(
            Inner_points, dtype=self.data_type)

        self.LOG = None

    def call(self, inputs):
        # Intial Shape = (Batch, 64,64, 1)
        Conv2D_1 = self.Layer_Conv2D_1(inputs)  # Shape = (Batch, 62,62,16)
        Conv2D_11 = self.Layer_Conv2D_11(
            Conv2D_1)  # Shape = (Batch, 60, 60, 16)
        Average_Pooling_1 = self.Pooling_Layer(
            Conv2D_11)  # Shape = (Batch, 30, 30, 16)
        Conv2D_2 = self.Layer_Conv2D_2(
            Average_Pooling_1)  # Shape = (Batch, 28,28,32)
        Conv2D_22 = self.Layer_Conv2D_22(Conv2D_2)  # Shape = (Batch, 26,26,32)
        Average_Pooling_2 = self.Pooling_Layer(
            Conv2D_22)  # Shape = (Batch, 18, 18 ,32)

        Conv2D_3 = self.Layer_Conv2D_3(
            Average_Pooling_2)  # Shape = (Batch, 16, 16 ,64)
        Conv2D_32 = self.Layer_Conv2D_32(
            Conv2D_3)
        Average_Pooling_3 = self.Pooling_Layer(
            Conv2D_32)  # Shape = (Batch, 8, 8 ,64)

        Conv2D_4 = self.Layer_Conv2D_4(
            Average_Pooling_3)  # Shape = (Batch, 2, 2 ,128)

        # Implementation of the FF
        FF = self.Flatten(Conv2D_4)
        for Layer in self.Block_Structure:
            FF = Layer(FF)
        Output_Reshaped = self.Reshape(FF)

        # Deconvolution
        Conv2DTrans_4 = self.Layer_Conv2DTrans_4(Output_Reshaped)
        Up_Sampled_3 = self.UpSampling_Layer(Conv2DTrans_4)

        Concatenated_3 = tf.concat(
            [Conv2D_32, tf.cast(Up_Sampled_3, dtype=self.data_type)], axis=-1)
        Conv2DTrans_32 = self.Layer_Conv2DTrans_32(Concatenated_3)

        Conv2DTrans_3 = self.Layer_Conv2DTrans_3(Conv2DTrans_32)
        Up_Sampled_2 = self.UpSampling_Layer(Conv2DTrans_3)
        Concatenated_2 = tf.concat(
            [Conv2D_22, tf.cast(Up_Sampled_2, dtype=self.data_type)], axis=-1)
        Conv2DTrans_22 = self.Layer_Conv2DTrans_22(Concatenated_2)
        Conv2DTrans_2 = self.Layer_Conv2DTrans_2(Conv2DTrans_22)
        Up_Sampled_1 = self.UpSampling_Layer(Conv2DTrans_2)

        Concatenated_1 = tf.concat(
            [Conv2D_11, tf.cast(Up_Sampled_1, dtype=self.data_type)], axis=-1)
        Conv2DTrans_11 = self.Layer_Conv2DTrans_11(Concatenated_1)

        Conv2DTrans_1 = self.Layer_Conv2DTrans_1(Conv2DTrans_11)

        Output = self.Layer_Conv2DTrans_Output(Conv2DTrans_1)

        return Output

    def train_step(self, data):
        BC, Output = data
        with tf.GradientTape(persistent=True) as tape0:
            FPhi_Pred = self(BC)
            loss_predict = keras.losses.mean_squared_error(FPhi_Pred, Output)
            if(self.Compute_Residual):

                # Compute the residual in 2D
                FPhi_Pred_R = tf.linalg.matmul(tf.transpose(
                    FPhi_Pred, perm=[0, 3, 1, 2]), self.Displace_up_right)

                FPhi_Pred_U = tf.linalg.matmul(
                    self.Displace_up_right, tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]))
                FPhi_Pred_L = tf.linalg.matmul(tf.transpose(
                    FPhi_Pred, perm=[0, 3, 1, 2]), self.Displace_down_left)
                FPhi_Pred_D = tf.linalg.matmul(
                    self.Displace_down_left, tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]))

                Residual = -(FPhi_Pred_U - 2 * tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]) + FPhi_Pred_D) * (self.N1) * (
                    self.N1) - (FPhi_Pred_L - 2 * tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]) + FPhi_Pred_R) * (self.N2) * (self.N2)

                # Take out the boundaries
                Inner_Residual = tf.linalg.matmul(
                    self.Inner_points, tf.linalg.matmul(Residual, self.Inner_points))

                # Compute the loss
                loss_residual = keras.losses.mean_squared_error(
                    Inner_Residual, self.RHS)
            else:
                loss_residual = tf.zeros_like(loss_predict)
            if(self.Loss_Selected != 'Loss_0'):
                loss = loss_predict + self.beta * loss_residual
            else:
                loss = loss_predict
            loss += sum(self.losses)
        # Predict_Grad = tape0.gradient(loss_predict, self.trainable_variables)
        # Residual_Grad = tape0.gradient(loss_residual, self.trainable_variables)
        lossGrad = tape0.gradient(loss, self.trainable_variables)

        del tape0
        self.optimizer.apply_gradients(zip(lossGrad, self.trainable_variables))

        # update loss and mae, tf internally handles distribution
        Loss.update_state(loss)
        Mae.update_state(FPhi_Pred, Output)
        Predict_mse.update_state(loss_predict)
        Residual_mse.update_state(loss_residual)
        # if(self.Compute_Residual):
        #     normList = [tf.reduce_sum(tf.square(Predict_Grad[i]))
        #                 for i in range(len(Predict_Grad))]
        #     Predict_Grad_Norm = tf.sqrt(tf.add_n(normList))
        #     normList = [tf.reduce_sum(tf.square(Residual_Grad[i]))
        #                 for i in range(len(Residual_Grad) - 1)]
        #     Residual_Grad_Norm = tf.sqrt(tf.add_n(normList))
        #     Predict_GradNorm.update_state(Predict_Grad_Norm)
        #     Residual_GradNorm.update_state(Residual_Grad_Norm)
        #     dotList = [tf.reduce_sum(tf.multiply(
        #         Predict_Grad[i], Residual_Grad[i])) for i in range(len(Residual_Grad))]
        #     dot = tf.add_n(dotList)
        #     cos = tf.divide(dot, tf.multiply(
        #         Predict_Grad_Norm, Residual_Grad_Norm))
        #     Cos.update_state(cos)
        #     # metric should handle the reducation across replicas automatically
        #     return {'loss': Loss.result(), 'mae': Mae.result(), 'Pred_Mse': Predict_mse.result(),
        #             'Res_Mse': Residual_mse.result(), 'Predict_GradNorm': Predict_GradNorm.result(),
        #             'Residual_GradNorm': Residual_GradNorm.result(), 'cos': Cos.result()}
        # else:
        return {'loss': Loss.result(), 'mae': Mae.result(), 'Pred_Mse': Predict_mse.result(),
                'Res_Mse': Residual_mse.result()}

    def test_step(self, data):
        BC, Output = data
        FPhi_Pred = self(BC)
        loss_predict = keras.losses.mean_squared_error(FPhi_Pred, Output)
        if(self.Compute_Residual):
            # Compute the residual in 2D
            FPhi_Pred_R = tf.linalg.matmul(tf.transpose(
                FPhi_Pred, perm=[0, 3, 1, 2]), self.Displace_up_right)

            FPhi_Pred_U = tf.linalg.matmul(
                self.Displace_up_right, tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]))
            FPhi_Pred_L = tf.linalg.matmul(tf.transpose(
                FPhi_Pred, perm=[0, 3, 1, 2]), self.Displace_down_left)
            FPhi_Pred_D = tf.linalg.matmul(
                self.Displace_down_left, tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]))

            Residual = -(FPhi_Pred_U - 2 * tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]) + FPhi_Pred_D) * (self.N1) * (
                self.N1) - (FPhi_Pred_L - 2 * tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]) + FPhi_Pred_R) * (self.N2) * (self.N2)

            # Take out the boundaries
            Inner_Residual = tf.linalg.matmul(
                self.Inner_points, tf.linalg.matmul(Residual, self.Inner_points))

            # Compute the loss
            loss_residual = keras.losses.mean_squared_error(
                Inner_Residual, self.RHS)
        else:
            loss_residual = tf.zeros_like(loss_predict)
        if(self.Loss_Selected != 'Loss_0'):
            loss = loss_predict + self.beta * loss_residual
        else:
            loss = loss_predict
        loss += sum(self.losses)

        # update loss and mae, tf internally handles distribution
        Loss.update_state(loss)
        Mae.update_state(FPhi_Pred, Output)
        Predict_mse.update_state(loss_predict)
        Residual_mse.update_state(loss_residual)
        # metric should handle the reducation across replicas automatically
        return {'loss': Loss.result(), 'mae': Mae.result(), 'Pred_Mse': Predict_mse.result(),
                'Res_Mse': Residual_mse.result()}

    @property
    def metrics(self):
        return [Loss, Predict_mse, Residual_mse, Predict_GradNorm, Residual_GradNorm, Cos, Mae]


# =============================================================================
# Convolutional NN 64x64 Images - Border

# The trining of this model is done using only the BC and the residual
# =============================================================================


class ConvolutionalNN_64_BC(ConvolutionalNN_64):

    def __init__(self, **kwargs):
        super(ConvolutionalNN_64_BC, self).__init__(**kwargs)

    def train_step(self, data):
        BC, Output = data
        print('Compute_Residual', self.Compute_Residual)
        with tf.GradientTape(persistent=True) as tape0:
            FPhi_Pred = self(BC)
            BC_Output = FPhi_Pred - tf.transpose(tf.linalg.matmul(
                self.Inner_points, tf.linalg.matmul(tf.transpose(
                    FPhi_Pred, perm=[0, 3, 1, 2]), self.Inner_points)), perm=[0, 2, 3, 1])

            loss_predict = keras.losses.mean_squared_error(FPhi_Pred, Output)
            loss_BC = keras.losses.mean_squared_error(BC_Output, BC)

            if(self.Compute_Residual):

                # Compute the residual in 2D
                FPhi_Pred_R = tf.linalg.matmul(tf.transpose(
                    FPhi_Pred, perm=[0, 3, 1, 2]), self.Displace_up_right)

                FPhi_Pred_U = tf.linalg.matmul(
                    self.Displace_up_right, tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]))
                FPhi_Pred_L = tf.linalg.matmul(tf.transpose(
                    FPhi_Pred, perm=[0, 3, 1, 2]), self.Displace_down_left)
                FPhi_Pred_D = tf.linalg.matmul(
                    self.Displace_down_left, tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]))

                Residual = -(FPhi_Pred_U - 2 * tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]) + FPhi_Pred_D) * (self.N1) * (
                    self.N1) - (FPhi_Pred_L - 2 * tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]) + FPhi_Pred_R) * (self.N2) * (self.N2)

                # Take out the boundaries
                Inner_Residual = tf.linalg.matmul(
                    self.Inner_points, tf.linalg.matmul(Residual, self.Inner_points))

                # Compute the loss
                loss_residual = keras.losses.mean_squared_error(
                    Inner_Residual, self.RHS)
            else:
                loss_residual = tf.zeros_like(loss_predict)
            if(self.Loss_Selected != 'Loss_0'):
                print('hello')
                loss = loss_BC + self.beta * loss_residual
            else:
                loss = loss_predict
            loss += sum(self.losses)
        # Predict_Grad = tape0.gradient(loss_predict, self.trainable_variables)
        # Residual_Grad = tape0.gradient(loss_residual, self.trainable_variables)
        lossGrad = tape0.gradient(loss, self.trainable_variables)

        del tape0
        self.optimizer.apply_gradients(zip(lossGrad, self.trainable_variables))

        # update loss and mae, tf internally handles distribution
        Loss.update_state(loss)
        Mae.update_state(FPhi_Pred, Output)
        Predict_mse.update_state(loss_predict)
        Residual_mse.update_state(loss_residual)
        BC_mse .update_state(loss_BC)

        return {'loss': Loss.result(), 'mae': Mae.result(), 'Pred_Mse': Predict_mse.result(),
                'Res_Mse': Residual_mse.result(), 'BC_Mse': BC_mse.result()}

    def test_step(self, data):
        BC, Output = data
        FPhi_Pred = self(BC)
        BC_Output = FPhi_Pred - tf.linalg.matmul(
            self.Inner_points, tf.linalg.matmul(tf.transpose(
                FPhi_Pred, perm=[0, 3, 1, 2]), self.Inner_points))

        loss_predict = keras.losses.mean_squared_error(FPhi_Pred, Output)
        loss_BC = keras.losses.mean_squared_error(BC_Output, BC)
        if(self.Compute_Residual):
            print('hello')
            # Compute the residual in 2D
            FPhi_Pred_R = tf.linalg.matmul(tf.transpose(
                FPhi_Pred, perm=[0, 3, 1, 2]), self.Displace_up_right)

            FPhi_Pred_U = tf.linalg.matmul(
                self.Displace_up_right, tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]))
            FPhi_Pred_L = tf.linalg.matmul(tf.transpose(
                FPhi_Pred, perm=[0, 3, 1, 2]), self.Displace_down_left)
            FPhi_Pred_D = tf.linalg.matmul(
                self.Displace_down_left, tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]))

            Residual = -(FPhi_Pred_U - 2 * tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]) + FPhi_Pred_D) * (self.N1) * (
                self.N1) - (FPhi_Pred_L - 2 * tf.transpose(FPhi_Pred, perm=[0, 3, 1, 2]) + FPhi_Pred_R) * (self.N2) * (self.N2)

            # Take out the boundaries
            Inner_Residual = tf.linalg.matmul(
                self.Inner_points, tf.linalg.matmul(Residual, self.Inner_points))

            # Compute the loss
            loss_residual = keras.losses.mean_squared_error(
                Inner_Residual, self.RHS)
        else:
            loss_residual = tf.zeros_like(loss_predict)
        if(self.Loss_Selected != 'Loss_0'):
            print(loss_residual)
            loss = loss_BC + self.beta * loss_residual
        else:
            loss = loss_predict
        loss += sum(self.losses)

        # update loss and mae, tf internally handles distribution
        Loss.update_state(loss)
        Mae.update_state(FPhi_Pred, Output)
        Predict_mse.update_state(loss_predict)
        Residual_mse.update_state(loss_residual)
        BC_mse.update_state(loss_BC)
        # metric should handle the reducation across replicas automatically
        return {'loss': Loss.result(), 'mae': Mae.result(), 'Pred_Mse': Predict_mse.result(),
                'Res_Mse': Residual_mse.result(), 'BC_Mse': BC_mse.result()}

    @property
    def metrics(self):
        return [Loss, Predict_mse, Residual_mse, BC_mse, Predict_GradNorm, Residual_GradNorm, Cos, Mae]
