import sys
sys.path.append('../src')
import os
import h5py as h5
import numpy as np
import tensorflow as tf
from tensorflow import keras
import argparse
import pinn_model as psnModel
from pinn_utils import *

parser = argparse.ArgumentParser()

# architecture
parser.add_argument('-m', '--model', type=int, default=1, \
                    help='0 - psnfc, 1 - psnxyfc')
parser.add_argument('-l', '--arch', type=int, nargs='*', help='architecture')
parser.add_argument('-last', '--lastLayerAct', default='tanh',\
                    help='activation function for last layer')

# coefficient for pde error
parser.add_argument('-alpha', '--alpha', type=float, default=0.01, \
                    help='regularization of pde')
parser.add_argument('-r', '--reg', type=float, nargs='*', default=None,\
                    help='l2 regularization')

# control training
parser.add_argument('-from', '--fromModel', default='', help='source model')
parser.add_argument('-to', '--toModel', default='', help='destination model')
parser.add_argument('-p', '--precision', default='32to64', \
                    help='precision convertion')

args = parser.parse_args()

dummyInput = [np.zeros((1,32*4)), np.zeros((1,2)), np.zeros((1,1))]

if args.precision == '32to64':
  keras.backend.set_floatx('float32')
else:
  keras.backend.set_floatx('float64')

if args.model == 1
  psnNet_aux = psnModel.PsnXyFC(width=args.arch, alpha=args.alpha, reg=N_reg)
  psnNet_aux.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=5.0e-4))
elif args.model == 0:
  psnNet_aux = psnModel.PsnFC(width=args.arch, alpha=args.alpha, reg=N_reg)
  psnNet_aux.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=5.0e-4))

psnNet_aux.load_weights(tf.train.latest_checkpoint(args.fromModel))
psnNet_aux.predict(void_input)
weights = psnNet_aux.get_weights()

if args.precision == '32to64':
  keras.backend.set_floatx('float64')
else:
  keras.backend.set_floatx('float32')

if args.model == 1:
  psnNet = psnModel.PsnXyFC(width=args.arch)
elif args.model == 0:
  psnNet = psnModel.PsnFC(width=args.arch)

psnNet.compile(optimizer=keras.optimizers.Adam(learning_rate=args.lr0))
psnNet.predict(dummyInput)
weights = [w.astype(tf.keras.backend.floatx()) for w in weights]
psnNet.set_weights(weights)
tf.keras.backend.set_value(psnNet.optimizer.lr,
                           lr.astype(tf.keras.backend.floatx()))
psnNet.predict(dummyInput) 
pnsNet.save_weights(args.toModel)


#bc_void = np.zeros((1, 32 * 4))
#xyBc_void = np.zeros((1, 32 * 4, 2))
#xy = np.zeros((1, 32 * 4, 2))
#w = np.zeros((1, 1))
#void_input = [bc_void, xyBc_void, xy]
# Load the float64 Model
#lr = tf.keras.backend.get_value(
    #psnNet_aux.optimizer.lr).astype(tf.keras.backend.floatx()) / 100
#psnNet_aux.predict(void_input)
## Retireve the weights from it
#weights = psnNet_aux.get_weights()

#keras.backend.set_floatx(‘float32’)
#psnNet = psnModel.PsnXyFC(width=args.arch, alpha=args.alpha, reg=N_reg,
                          #save_grad_stat=args.saveGradStat,
                          #last_act=args.lastLayerAct)
#psnNet.compile(optimizer=keras.optimizers.Adam(learning_rate=args.lr0))
#psnNet.predict(void_input)
#psnNet.evaluate(validGen, verbose=2)
# Change the weights format
weights = [w.astype(tf.keras.backend.floatx()) for w in weights]
# Load the weights into the model
psnNet.set_weights(weights)
tf.keras.backend.set_value(psnNet.optimizer.lr,
                           lr.astype(tf.keras.backend.floatx()))
psnNet.predict(void_input) 
