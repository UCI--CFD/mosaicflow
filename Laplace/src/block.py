import numpy as np

BOUNDARY_PHYSICAL = 0
BOUNDARY_TO_BLOCK = 1

#----------------------------------------------------------------
# class for block's boundary
#----------------------------------------------------------------

class BlockBoundary:
  def __init__(self, shape, edge, bType = BOUNDARY_PHYSICAL):
    '''
    a boundary can be 'physical', not changed during iterations, or 
    'to_block', mapped to an edge of another block
    edge has four poissbilities: i-, j-, i+, j+
    '''
    assert edge in ['i-', 'j-', 'i+', 'j+']
    self.bType   = bType
    self.toBlock = -1
    self.toEdge  = -1
    self.shape   = shape
    # data will be copied from one block's rng1 to self's rng0
    self.rng0    = None
    self.rng1    = None
    # base on the edge set data range0, 
    # which is the source for future copy
    if edge   == 'i-':
      self.rng0 = [0, 1, 1, shape[1]+1]
    elif edge == 'j-':
      self.rng0 = [1, 0, shape[0]+1, 1]
    elif edge == 'i+':
      self.rng0 = [shape[0]+1, 1, shape[0]+2, shape[1]+1]
    elif edge == 'j+':
      self.rng0 = [1, shape[1]+1, shape[0]+1, shape[1]+2]


  def set_connection(self, toBlock, toEdge):
    self.bType   = BOUNDARY_TO_BLOCK
    self.toBlock = toBlock
    self.toEdge  = toEdge
    # will copy from rng1 to rng0
    if   toEdge == 'i-':
      self.rng1 = [1, 1, 2, self.shape[1]+1]
    elif toEdge == 'j-':
      self.rng1 = [1, 1, self.shape[0]+1, 2]
    elif toEdge == 'i+':
      self.rng1 = [self.shape[0], 1, self.shape[0]+1, self.shape[1]+1]
    elif toEdge == 'j+':
      self.rng1 = [1, self.shape[1], self.shape[0]+1, self.shape[1]+1]


#----------------------------------------------------------------
# class for block's boundary
#----------------------------------------------------------------


class Block:
  def __init__(self, shape, nVar=1, nCoord=2):
    self.shape  = shape
    self.nVar   = nVar
    self.nCoord = nCoord
    self.coords = np.zeros((self.shape[0]+2, self.shape[1]+2, nCoord))
    self.vars   = np.zeros((self.shape[0]+2, self.shape[1]+2, nVar))
    self.nCell  = shape[0] * shape[1]
    self.nPoint = (shape[0]+1) * (shape[1]+1)
    self.bcs    = {'i-' : BlockBoundary(shape, 'i-'), \
                   'j-' : BlockBoundary(shape, 'j-'), \
                   'i+' : BlockBoundary(shape, 'i+'), \
                   'j+' : BlockBoundary(shape, 'j+')  }


  def add_connection(self, edge, toBlock, toEdge):
    self.bcs[edge].set_connection(toBlock, toEdge)

  def nullify(self):
    self.vars[1:-1,1:-1,:] = 0.0


#----------------------------------------------------------------
# fucntions work on block objects
#----------------------------------------------------------------


def nullify_block(blocks):
  for b in blocks:
    b.nullify()


def exchange_halo(blocks):
  for i, blk in enumerate(blocks):
    for key in blk.bcs:
      if blk.bcs[key].bType == BOUNDARY_TO_BLOCK:
        rng0  = blk.bcs[key].rng0
        rng1  = blk.bcs[key].rng1
        toBlk = blocks[blk.bcs[key].toBlock] 
        blk.vars[rng0[0]:rng0[2], rng0[1]:rng0[3], :] = \
          toBlk.vars[rng1[0]:rng1[2], rng1[1]:rng1[3] ,:]
        #print('copy block {} range'.format(i), end=' ')
        #print(rng0[:], end='  ')
        #print('from block {} range'.format(blk.bcs[key].toBlock), end=' ')
        #print(rng1[:])


def copy_block(fromBlocks, toBlocks):
  assert len(fromBlocks) == len(toBlocks)
  for b in range(len(fromBlocks)):
    assert fromBlocks[b].shape[0] == toBlocks[b].shape[0]
    assert fromBlocks[b].shape[1] == toBlocks[b].shape[1]
    toBlocks[b].vars[...]   = np.copy(fromBlocks[b].vars[...])
    toBlocks[b].coords[...] = np.copy(fromBlocks[b].coords[...])


def compute_residual(blocks0, blocks, verbose=False):
  assert len(blocks0) == len(blocks)

  resLi = np.zeros((blocks[0].nVar))
  resL1 = np.zeros((blocks[0].nVar))
  totCell = 0
  for v in range(blocks[0].nVar):
    for b in range(len(blocks)):
      for i in range(1, 1+blocks[b].shape[0]):
        for j in range(1, 1+blocks[b].shape[1]):
          resL1[v] += abs(blocks[b].vars[j,i,v] - blocks0[b].vars[j,i,v])
          resLi[v]  = max(abs(blocks[b].vars[j,i,v] - blocks0[b].vars[j,i,v]), resLi[v])
      totCell += blocks[b].nCell
    resL1[v] = resL1[v] / totCell

  if (verbose):
    for i in range(blocks[0].nVar):
      print('var {} residual Linf {:e} L1 {:e}'.format(i, resLi[i], resL1[i]))

  return [resLi, resL1]
