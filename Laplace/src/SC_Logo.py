#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 27 11:08:14 2021

@author: robertplanas
"""

GPU = None
if(GPU is not None):
    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = GPU

import argparse
parser = argparse.ArgumentParser()

parser.add_argument('-m', '--model', default="lplc-32x2-64x2-96x5-128x5t18000_a0.001_d400_c400_s20",
                    help='Select a model')
parser.add_argument('-l', '--arch', type=int, nargs='*', help='architecture',
                    default = [32, 32, 64, 64, 96, 96, 96, 96, 96, 128, 128, 128, 128, 128])
parser.add_argument('-DataType', '--DataType', default="float32",
                    help='float32 or float64, must match the model')
parser.add_argument('-plots', '--plots', default=True,
                    help='Plot the final solutions')
parser.add_argument('-f', '--File', default='../data/SC2021_Logo.png',
                    help='.png containing the logo')
parser.add_argument('-reproduce', '--reproduce', default=False, action='store_true',\
                    help='reproduce the comparison in paper')

args = parser.parse_args()

if(args.reproduce):
    args.model = "Laplace_FF-32x2-64x2-96x5-128x5t18000_a0.001_d400_c400_s20_float32"
    args.arch = [32, 32, 64, 64, 96, 96, 96, 96, 96, 128, 128, 128, 128, 128]
    args.DataType = "float32"
    args.plot = True
    args.print = True
    args.File = '../data/SC2021_Logo.png'


import tensorflow as tf
import numpy as np
from PIL import Image
from matplotlib import image
from matplotlib import pyplot as plt
import pyamg as amg
import time

import sys
sys.path.append('../src/')
import pinn_model as psnModel
sys.path.append('../Laplace_Genomes_src/')
import Genomes_FF as G_FF
from mpl_toolkits.axes_grid1 import make_axes_locatable

# =============================================================================
# Generate the data
# =============================================================================

# Load the file 
image = Image.open(args.File)
width, height = image.size
Resize_factor = 4
resized = image.resize((Resize_factor*width, Resize_factor*height))
data = np.asarray(resized)[:,:,0]
Workspace = np.copy(data).astype(args.DataType)

#Resize the workspace to have an integer number of genomes
#Note: No halo layer is included in this workspace as the logo is fully inside the domain!
Workspace = Workspace[4:-4,4:-4]
Pixels_I = np.shape(Workspace)[0]
Pixels_J = np.shape(Workspace)[1]


#Define a Genome
Gnome = np.ones((32,32))

#Paint the image: 
Canvas = np.zeros_like(Workspace)

#Place Gnomes Regular
for i in range(int(Pixels_I/32)):
    for j in range(int(Pixels_J/32)):
        if(Workspace[32*i,32*j] != 41 ):
            Canvas[32*i:32*i+32, 32*j:32*j+32] = Gnome[:,:]
#Place Horizontal  Auxiliar
for i in range(int(Pixels_I/32)-1):
    for j in range(int(Pixels_J/32)):
        if(Workspace[32*i+16,32*j] != 41 ):
            Canvas[32*i +16:32*i + 16 +32, 32*j:32*j+32] = Gnome[:,:]     

#Place Vertical Auxiliar
for i in range(int(Pixels_I/32)):
    for j in range(int(Pixels_J/32)-1):
        if(Workspace[32*i,32*j+16] != 41 ):
            Canvas[32*i:32*i +32, 32*j+16:32*j+32+16] = Gnome[:,:]   

#Set Other Pixels to Nan
for i in range(Pixels_I):
    for j in range(Pixels_J):
        if(Canvas[i,j] == 0 ):
            Canvas[i,j] = np.nan

plt.figure()
plt.imshow(Canvas)
Total_Pixels = np.sum(np.nan_to_num(Canvas))


Canvas_Borders = G_FF.Extract_Border_Domain(Canvas)

# Place the boundary conditions on the borders

Domain_Width_I = 6
Domain_Heigth_J = 12
x = np.linspace(1/64,Domain_Width_I - 1/64, num = np.shape(Canvas_Borders)[0])
y = np.linspace(1/64,Domain_Heigth_J - 1/64, num = np.shape(Canvas_Borders)[1])
X,Y = np.meshgrid(x,y)
Sin = np.sin(2*np.pi *(X + Y)).T
Canvas_BC = Canvas_Borders*Sin

#The figure may appear empty as one pixel is difficult to plot
plt.figure()
plt.imshow(Canvas_BC)

#Compute the length 
I_Width = np.shape(Canvas_BC)[0]
J_Width = np.shape(Canvas_BC)[1]

#%% Define the matrix
A = amg.gallery.poisson((I_Width, J_Width), format='csr')
# Define the RHS
RHS = np.zeros((I_Width, J_Width))
Max = 0
for i in range(len(Canvas_BC[:,0])):
    for j in range(len(Canvas_BC[0,:])):
        if(not np.isnan(Canvas_BC[i,j]) ):
            RHS[i,j] += Canvas_BC[i,j]
            
# transform rhs to a vector
RHS = np.reshape(RHS, ((I_Width)*(J_Width)), order='C')
Canvas_BC_reshaped = np.reshape(Canvas_BC, ((I_Width)*(J_Width)), order='C')
# Modify the A Values
for index in range(A.shape[0]):
    if(not np.isnan(Canvas_BC_reshaped[index])):
        for index_i in A[index].indices:
            if(A[index,index_i]  ==4 ):
                A[index,index_i] = 1
            else: 
                 A[index,index_i] = 0

print("Solving the domain with pyAMG....")
strat_time = time.time()
x = amg.solve(A, RHS, verb=False, tol=1e-12)
Solution = np.reshape(x, (I_Width, J_Width), order='C') 
Final_Solution = np.zeros((I_Width+2, J_Width+2))
Final_Solution[1:-1, 1:-1] = Solution[:,:]
print("Elapsed Time for solving with AMG", time.time() - strat_time)
plt.figure()
im = plt.imshow(Final_Solution)    
plt.colorbar(im)


#%% Domain interior and borders Ones
Domain_Interior_Ones = np.zeros((I_Width+2,J_Width+2))
Domain_Interior_Ones[1:-1, 1:-1] = Canvas[:,:]
#%% Domain_Interior_Nan
Canvas_Borders_Interor_Zero = G_FF.Extract_Border_Domain(Canvas, "Zeros")
Canvas_Interior = Canvas - Canvas_Borders_Interor_Zero
Canvas_Interior_Nan_Exterior_Ones = np.ones((I_Width, J_Width))
for i in range(I_Width):
    for j in range(J_Width):
        if (Canvas_Interior[i,j] == 1):
            Canvas_Interior_Nan_Exterior_Ones[i,j] = np.nan
            
Domain_Interior_Nan = np.ones((I_Width+2, J_Width+2))
Domain_Interior_Nan[1:-1,1:-1] = Canvas_Interior_Nan_Exterior_Ones
Domain_Interior_Nan = Domain_Interior_Nan*Final_Solution
Domain_Interior_Values = Domain_Interior_Ones*Final_Solution
plt.figure()
plt.imshow(Domain_Interior_Ones)
plt.figure()
plt.imshow(Domain_Interior_Nan)
plt.figure()
im = plt.imshow(Domain_Interior_Values)
plt.colorbar(im)

#%% =============================================================================
# LOAD THE MODEL
# =============================================================================

tf.keras.backend.set_floatx(args.DataType)
Data_Type = args.DataType
modelPath = '../models/'
modelName = args.model
Model_FF_BC = psnModel.PsnXyFC(width=args.arch)
Model_FF_BC.load_weights(tf.train.latest_checkpoint(modelPath + modelName))
Model_FF_BC.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3))
bc_zeros = np.zeros((2, 128))
xy_zeros = np.zeros((2, 2))
output_zeros = np.zeros((2, 1))
w_zeros = np.zeros((2, 2))
bc_xy_zeros = [bc_zeros, xy_zeros, w_zeros]
Model_FF_BC.evaluate(bc_xy_zeros, output_zeros, verbose=2)
Model_FF_BC.trainable = False
# Freeze the weights
for layer in Model_FF_BC.layers:
    layer.trainable = False


MIN = -1.785187
MAX = 2.011001
U_N = G_FF.Normalize_Domain(np.squeeze(Domain_Interior_Nan), Min=-1.785187, Max=2.011001).astype(Data_Type)
Domain_Interior_Values_N = G_FF.Normalize_Domain(np.squeeze(Domain_Interior_Values), Min=-1.785187, Max=2.011001).astype(Data_Type)


Width = 30
Heigth = 72
Gnomes, Shared_Borders = G_FF.Generate_Genomes_Automatically(Width = Width, Height = Heigth, Genome_width = 1,
                                                             Points_Borders = 32,
                                                             Domain_Interior_Ones = Domain_Interior_Ones,
                                                             Domain_Int_Nan = U_N)

#%% =============================================================================
# Iterate the genomes
# =============================================================================

start_time = time.time()
MAE, MSE, RMSE, Convergences, Solved_Solution = G_FF.Iterative_Solution_Optimized(Shared_Borders, Gnomes, Model_FF_BC, 
                                                                     Domain_Interior_Values_N, Max = MAX, Min = MIN, Iterations = 500 , Un_normalize = True)
Solved_Solution = Solved_Solution*Domain_Interior_Ones
Time = time.time() - start_time

print("Final resutls -----------")
print("MAE = ", MAE)
print("Elapsed Time = ", Time)

if(args.plots):
    plt.figure()
    ax = plt.gca()
    im2 = plt.imshow(Solved_Solution, cmap='rainbow', extent=[0,72,0,30], aspect=1)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cbar = plt.colorbar(im2, cax=cax, ticks = [-1,0, 1])
    plt.clim(-1,1)
    plt.savefig('Solved_Solution_SC_Logo.pdf', format='pdf', bbox_inches='tight')    
    
    
    plt.figure()
    ax = plt.gca()
    im2 = plt.imshow(Domain_Interior_Values, cmap='rainbow', extent=[0,72,0,30], aspect=1)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cbar = plt.colorbar(im2, cax=cax, ticks = [-1,0, 1])
    plt.clim(-1,1)
    plt.savefig('Exact_Solution_SC_Logo.pdf', format='pdf', bbox_inches='tight')    
    
    
    plt.figure()
    ax = plt.gca()
    im2 = plt.imshow(np.abs(Domain_Interior_Values - Solved_Solution), cmap='rainbow', extent=[0,72,0,30], aspect=1)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cbar = plt.colorbar(im2, cax=cax, ticks = [0, 0.02, 0.04])
    plt.clim(0,0.04)
    plt.savefig('Error_SC_Logo.pdf', format='pdf', bbox_inches='tight')    

