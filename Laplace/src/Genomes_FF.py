#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 27 15:57:45 2021

@author: robertplanas
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 09:35:49 2021

@author: robertplanas
"""
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import random

# Check if a data point belongs to a border
def Check_Surroundings(i,j,Domain):
    if(np.isnan(Domain[i,j])):
        return True
    if(i == 0 or j == 0 or i == len(Domain[:,0]) - 1 or j == len(Domain[0,:]) -1 ):
        return True
    
    if(np.isnan(Domain[i-1,j]) or np.isnan(Domain[i+1,j]) or np.isnan(Domain[i,j+1]) or np.isnan(Domain[i,j-1])):
        return True
 
    return False 

# Extract the Borders from an arbitrary domain
def Extract_Border_Domain(Domain, Nan_or_Zero = "NaN"):
    if Nan_or_Zero == "NaN":
        Value = np.nan
    else:
        Value = 0
    Size_x = len(Domain[:,0])
    Size_y = len(Domain[0,:])
    Domain_Return = np.empty((Size_x, Size_y))
    for i in range(Size_x):
        for j in range(Size_y):
            if(Check_Surroundings(i,j, Domain)):
                Domain_Return[i,j] = Domain[i,j]
            else: 
                Domain_Return[i,j] = Value
    return Domain_Return 

# Normalize Domain
def Normalize_Domain(Domain, Min, Max):
    return (Domain -Min)/(Max - Min)

# UnNormalize the Domain
def Un_Normalize_Domain(Domain, Min, Max):
    return (Domain)*(Max - Min) + Min 


class Genome:
    def __init__(self, width = 1, Points_Border = 32, x0_global = 0, y0_global = 0 ):
        self.x0_global = x0_global
        self.y0_global = y0_global
        self.width = width
        self.Points_Border = Points_Border
        self.h = 1/self.Points_Border
        X = np.linspace(self.h/2, self.width - self.h/2, self.Points_Border)
        Y = np.linspace(self.h/2, self.width - self.h/2, self.Points_Border)
        XY = np.reshape(np.meshgrid(X,Y), (2,self.Points_Border**2)).T
        self.Inner_Points_Grid = XY
        self.Inner_Borders = [] # (X_R, Y_R, Value, index_shared)
        self.BC_Values = np.zeros((4*self.Points_Border))
        self.BC_Info = []
        self.i0 = int(self.x0_global*self.Points_Border + 1)
        self.j0 = int(self.y0_global*self.Points_Border + 1)
        
    def Set_Borders(self, Shared_Borders, p = np.zeros((34,34)) ):
        
        # Define Top Borders
        self.BC_Values[0:self.Points_Border] = 0.5*(p[self.i0-1,self.j0:(self.j0 + self.Points_Border)] + p[self.i0,self.j0:(self.j0 + self.Points_Border)])
        I = np.linspace(self.x0_global, self.x0_global, self.Points_Border)[:,None]
        J = np.linspace(self.y0_global+ self.h/2, self.y0_global + self.width - self.h/2, self.Points_Border)[:,None]
        index_BC = np.linspace(0,self.Points_Border-1, self.Points_Border)[:,None]
        for i in range(0,self.Points_Border):
            if np.isnan(self.BC_Values[i]):
                self.BC_Values[i] = 0
                Value = 0
                index_shared = -1
                Border_Info = np.hstack([I[i],J[i],index_BC[i],index_shared, Value])
                if(len(self.BC_Info) == 0):
                    self.BC_Info = Border_Info
                else:
                    self.BC_Info = np.vstack([self.BC_Info, Border_Info])
        
        # Define Rigth Borders
        self.BC_Values[self.Points_Border:2*self.Points_Border] = 0.5*(p[self.i0:(self.i0 + self.Points_Border),(self.j0 + self.Points_Border)] + p[self.i0:(self.i0 + self.Points_Border),(self.j0 + self.Points_Border -1)])
        I = np.linspace(self.x0_global + self.h/2, self.x0_global + self.width - self.h/2, self.Points_Border)[:,None]
        J = np.linspace(self.y0_global + self.width, self.y0_global + self.width, self.Points_Border)[:,None]
        index_BC = np.linspace(self.Points_Border,2*self.Points_Border-1, self.Points_Border)[:,None]
        for i in range(0,self.Points_Border):
            if np.isnan(self.BC_Values[i + self.Points_Border ]):
                self.BC_Values[i + self.Points_Border ] = 0
                Value = 0
                index_shared = -1
                Border_Info = np.hstack([I[i],J[i],index_BC[i],index_shared, Value])
                if(len(self.BC_Info) == 0):
                    self.BC_Info = Border_Info
                else:
                    self.BC_Info = np.vstack([self.BC_Info, Border_Info])
        
        #Define bottom borders
        self.BC_Values[2*self.Points_Border:3*self.Points_Border] = np.flip(0.5*(p[(self.i0 + self.Points_Border), self.j0:(self.j0 + self.Points_Border)] + p[(self.i0 + self.Points_Border-1), self.j0:(self.j0 + self.Points_Border)]))
        I = np.linspace(self.x0_global + self.width, self.x0_global + self.width , self.Points_Border)[:,None]
        J = np.flip(np.linspace(self.y0_global+ self.h/2, self.y0_global + self.width - self.h/2, self.Points_Border))[:,None]
        index_BC = np.linspace(2*self.Points_Border,3*self.Points_Border-1, self.Points_Border)[:,None]
        for i in range(0,self.Points_Border):
            if np.isnan(self.BC_Values[i + 2*self.Points_Border ]):
                self.BC_Values[i + 2*self.Points_Border ] = 0
                Value = 0
                index_shared = -1
                Border_Info = np.hstack([I[i],J[i],index_BC[i],index_shared, Value])
                if(len(self.BC_Info) == 0):
                    self.BC_Info = Border_Info
                else:
                    self.BC_Info = np.vstack([self.BC_Info, Border_Info])
        
           
                

        self.BC_Values[3*self.Points_Border:4*self.Points_Border] = np.flip(0.5*(p[self.i0:(self.i0 + self.Points_Border), self.j0] + p[self.i0:(self.i0 + self.Points_Border), self.j0 -1 ]))
        I = np.flip(np.linspace(self.x0_global + self.h/2, self.x0_global + self.width - self.h/2, self.Points_Border))[:,None]
        J = np.linspace(self.y0_global, self.y0_global, self.Points_Border)[:,None]
        index_BC = np.linspace(3*self.Points_Border,4*self.Points_Border-1, self.Points_Border)[:,None]
        for i in range(0,self.Points_Border):
            if np.isnan(self.BC_Values[i + 3*self.Points_Border ]):
                self.BC_Values[i + 3*self.Points_Border ] = 0
                Value = 0
                index_shared = -1
                Border_Info = np.hstack([I[i],J[i],index_BC[i],index_shared, Value])
                if(len(self.BC_Info) == 0):
                    self.BC_Info = Border_Info
                else:
                    self.BC_Info = np.vstack([self.BC_Info, Border_Info])
           
        index_shared = len(Shared_Borders)
        for index in range(len(self.BC_Info)):
            self.BC_Info[index,-2] = index_shared +index
            if(index_shared +index == 0):
                Shared_Borders = self.BC_Info[index,:]
            else:
                Shared_Borders = np.vstack([Shared_Borders, self.BC_Info[index,:]])
        
        return Shared_Borders
    
    def Update_BC(self,Shared_Borders):
        for index in range(len(self.BC_Info)):
            self.BC_Values[int(self.BC_Info[index,2])] = Shared_Borders[int(self.BC_Info[index,-2]), -1]
    
    def Update_Shared_Borders(self,model,Shared_Borders):
        Input_BC = np.zeros((len(self.Inner_Borders), 4*self.Points_Border))
        Input_BC[:] = self.BC_Values
        Input = [Input_BC,self.Inner_Borders[:,0:2]]
        Predictions = (model.predict(Input)).flatten()
        Convergence = np.mean(np.square(self.Inner_Borders[:,2] - Predictions))
        self.Inner_Borders[:,2] = Predictions
        for index in range(len(self.Inner_Borders)):
            Shared_Borders[int(self.Inner_Borders[index,-1]), -1] = self.Inner_Borders[index,2]
        return Shared_Borders, Convergence
    
    def Update_Domain(self, model, Domain):
        Input_BC = np.zeros((self.Points_Border*self.Points_Border, 4*self.Points_Border))
        Input_BC[:] = self.BC_Values
        Input = [Input_BC,self.Inner_Points_Grid]
        Output = np.reshape((model.predict(Input)).flatten(), (self.Points_Border,self.Points_Border)).T
        Domain[self.i0:(self.i0+self.Points_Border), self.j0:(self.j0 + self.Points_Border)] = Output[:,:]
        return Domain
        
    def Include_Inner_Border_Points(self,Shared_Borders):
        
        for element in Shared_Borders:
            X = element[0]
            Y = element[1]
            Value = 0
            index_shared = element[-2]
            if(X > self.x0_global and X < (self.x0_global + self.width) and Y > self.y0_global and Y < self.y0_global +self.width):
                x_r = X - self.x0_global
                y_r = Y - self.y0_global
                Point_Info = [x_r,y_r, Value, index_shared]
                if(len(self.Inner_Borders) == 0):
                    self.Inner_Borders = Point_Info
                else:
                    self.Inner_Borders = np.vstack([self.Inner_Borders, Point_Info])
                    

def Iterative_Solution_Optimized(Shared_Borders, Gnomes, Model, U_N, Max = 1, Min = 0, Iterations = 200, Un_normalize = True):
    Convergences = []
    if len(Gnomes) == 1:
        Solution = np.zeros_like(U_N)
        Solution = Gnomes[0].Update_Domain(Model, Solution)
    else:   
        for iteration in range(Iterations):
            print("Iteration = ", iteration, "--------" )
            Convergence = 0
            for index, Gnome in enumerate(Gnomes):
                #if(index%10 == 0):
                    #print("Predicting Genome ", index +1, "of ", len(Gnomes), "Total Genomes")
                Gnome.Update_BC(Shared_Borders)
                Shared_Borders, Convergence_1 = Gnome.Update_Shared_Borders(Model, Shared_Borders)            
                Convergence = max(Convergence, Convergence_1)
            Convergence = Convergence
            Convergences.append(Convergence)
            if(Convergence < 1e-12):
                break
        #Final iteration for obtaining the solution
        Solution = np.zeros_like(U_N)
        for index, Gnome in enumerate(Gnomes):
            if(index%10 == 0):
                print("Solving Genome ", index, "of ", len(Gnomes), "Total Genomes")
            Gnome.Update_BC(Shared_Borders)
            Solution = Gnome.Update_Domain(Model, Solution)

    
    Number_of_Pixels = 0 
    for i in range(len(U_N[1:-1,0])):
        for j in range(len(U_N[0,1:-1])):
            if(not np.isnan(Solution[i+1, j+1])):
                Number_of_Pixels += 1
                
    # Compute the metrics
    MAE = Un_Normalize_Domain(np.sum(np.abs(np.nan_to_num(Solution[1:-1,1:-1])-np.nan_to_num(U_N[1:-1,1:-1])))/Number_of_Pixels, 0, Max - Min)
    MSE = Un_Normalize_Domain(np.sum(np.square(np.nan_to_num(Solution[1:-1,1:-1])-np.nan_to_num(U_N[1:-1,1:-1])))/Number_of_Pixels, 0, Max- Min)

    return MAE, MSE, Convergences, Un_Normalize_Domain(Solution, Min, Max)

def Generate_Genomes_Automatically(Width, Height, Genome_width, Points_Borders, Domain_Interior_Ones,
                                   Domain_Int_Nan, ):
    Gnomes = []
    Shared_Borders = []
    Number_Gnomes_x = int(Width/Genome_width)
    Number_Gnomes_y = int(Height/Genome_width)
    
    print("Placing Genomes...")
    #Set the regular gnomes    
    for x in range(Number_Gnomes_x):
        for y in range(Number_Gnomes_y):

            x0_global = x*Genome_width
            y0_global = y*Genome_width
            i0_global = int(x0_global*Points_Borders + 1)
            j0_global = int(y0_global*Points_Borders + 1)
            if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
               Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                Shared_Borders = Genome_element.Set_Borders(Shared_Borders,p = Domain_Int_Nan)
                Gnomes.append(Genome_element)

            
    # Set the overlapping Gnomes in the horizontal line
    for x in range(Number_Gnomes_x):
        for y in range(Number_Gnomes_y -1):
            x0_global = x*Genome_width
            y0_global = y*Genome_width + Genome_width/2
            i0_global = int(x0_global*Points_Borders + 1)
            j0_global = int(y0_global*Points_Borders + 1)
            if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
               Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                Shared_Borders = Genome_element.Set_Borders(Shared_Borders,p = Domain_Int_Nan)
                Gnomes.append(Genome_element)
                
    # Set the overlapping Gnomes in the vertical line
    for x in range(Number_Gnomes_x- 1):
        for y in range(Number_Gnomes_y):
            x0_global = x*Genome_width  + Genome_width/2
            y0_global = y*Genome_width
            i0_global = int(x0_global*Points_Borders + 1)
            j0_global = int(y0_global*Points_Borders + 1)
            if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
               Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                Shared_Borders = Genome_element.Set_Borders(Shared_Borders,p = Domain_Int_Nan)
                Gnomes.append(Genome_element)
                
    # Set the overlapping Gnomes in the diagonal line
    for x in range(Number_Gnomes_x -1):
        for y in range(Number_Gnomes_y -1):
            x0_global = x*Genome_width  + Genome_width/2
            y0_global = y*Genome_width + Genome_width/2
            i0_global = int(x0_global*Points_Borders + 1)
            j0_global = int(y0_global*Points_Borders + 1)
            if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
               Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
               Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                Shared_Borders = Genome_element.Set_Borders(Shared_Borders,p = Domain_Int_Nan)
                Gnomes.append(Genome_element)
                
    print("A total of ", len(Gnomes), " genomes are being used")
    # Update the Inner Pixels
    for index,Gnome in enumerate(Gnomes):
        if(index%10 == 0):
            print("Gather information from borders inside the Genome", index + 1, "of ", len(Gnomes), "Total Genomes")
        Gnome.Include_Inner_Border_Points(Shared_Borders)
    return Gnomes, Shared_Borders
                         
        
            
        
def Generate_Genomes_with_Grid(Width, Height, Genome_width, Points_Borders, Domain_Interior_Ones,
                                   Domain_Int_Nan, Grid, Random_Order = False ):
    
    # Grid can be
        # "Basic" = 0
        # "Regular Auxiliar" = 1
        # "Double density Auxiliar" = 2
        # "Quadruble density" = 3
        # " 2^4 density" = 4
        
    Gnomes = []
    Shared_Borders = []
    Number_Gnomes_x = int(Width/Genome_width)
    Number_Gnomes_y = int(Height/Genome_width)
    
    if (Grid == 0):
        Steps = [0]
    else:
        Steps = [0]
        for point in range(1,Grid+1):
            Aux = np.linspace(1,2**point -1 , num = 2**point-1)/2**point
            for element in Aux:
                if(element not in Steps):
                    Steps.append(element)
    
    List_Layers = []
    for step_x in Steps:
        for step_y in Steps: 
            Element = (step_x, step_y)
            if(Element not in List_Layers):
                List_Layers.append(Element)
            Element = (step_y, step_x)
            if(Element not in List_Layers):
                List_Layers.append(Element)
            Element = (step_y, step_y)
            if(Element not in List_Layers):
                List_Layers.append(Element)
    print(List_Layers)
    if Random_Order:
        Random_Permut = np.random.permutation(len(List_Layers)-1) +1
        List_Layers_Aux = []
        List_Layers_Aux.append(List_Layers[0])
        for i in Random_Permut:
            List_Layers_Aux.append(List_Layers[i])
        List_Layers = List_Layers_Aux
    print(List_Layers)
    for element in List_Layers:
        step_x, step_y = element
        for x in range(Number_Gnomes_x):
            for y in range(Number_Gnomes_y):
                x0_global = x*Genome_width + step_x
                y0_global = y*Genome_width + step_y
                if( ((x0_global  + Genome_width) <= Width) and ((y0_global + Genome_width) <= Height)):
                    i0_global = int(x0_global*Points_Borders + 1)
                    j0_global = int(y0_global*Points_Borders + 1)
                    if(Domain_Interior_Ones[i0_global,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global + Points_Borders -1 ,j0_global] == 1 and 
                       Domain_Interior_Ones[i0_global,j0_global + Points_Borders-1] == 1 and
                       Domain_Interior_Ones[i0_global + Points_Borders -1,j0_global + Points_Borders -1] == 1):
                        Genome_element = Genome(width = Genome_width, Points_Border = Points_Borders, x0_global = x0_global, y0_global = y0_global)
                        Shared_Borders = Genome_element.Set_Borders(Shared_Borders,p = Domain_Int_Nan)
                        Gnomes.append(Genome_element)
                            
    print("A total of ", len(Gnomes), " genomes are being used")
    # Update the Inner Pixels
    for index,Gnome in enumerate(Gnomes):
        #if(index%10 == 0):
            #print("Gather information from borders inside the Genome", index + 1, "of ", len(Gnomes), "Total Genomes")
        Gnome.Include_Inner_Border_Points(Shared_Borders)
    return Gnomes, Shared_Borders
            

        
        
    

            
        
                
        