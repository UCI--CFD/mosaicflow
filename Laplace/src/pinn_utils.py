import tensorflow as tf
import numpy as np

def BcXybcXydupGen(p, solBegin, solEnd, nSolPerBatch, bcXyOnly=True, length=1.0):
  nCell     = p.shape[1] - 2
  nBcCell   = 4 * nCell
  nInCell   = nCell * nCell
  nCellPerBatch = nInCell
  h         = length / nCell
  pMax      = np.amax(p)
  pMin      = np.amin(p)
  batchSize = nSolPerBatch * nCellPerBatch
  bc        = np.zeros((batchSize, nBcCell))
  solBc     = np.zeros(nBcCell)
  xy        = np.zeros((batchSize ,nBcCell, 2))
  xyIn      = np.zeros((nInCell, 2))
  xyBc      = np.zeros((batchSize, nBcCell, 2))
  label     = np.zeros((batchSize))
  # weights, xyBc, and xy does not change with solutions.
  # Those data duplicate for each solution, even each cell, which are highly
  # ineffecient for memory allocation and needs to be optimized asap.
  # boundary coordinates
  for m in range(batchSize):
    xyBc[m, :nCell, 0] = 0.0
    xyBc[m, :nCell, 1] = (np.arange(nCell) + 0.5) * h
    xyBc[m, nCell:2*nCell, 0] = (np.arange(nCell) + 0.5) * h
    xyBc[m, nCell:2*nCell, 1] = 1.0
    xyBc[m, 2*nCell:3*nCell, 0] = 1.0
    xyBc[m, 2*nCell:3*nCell, 1] = (np.arange(nCell,0,-1) - 0.5) * h
    xyBc[m, 3*nCell:, 0] = (np.arange(nCell,0,-1) - 0.5) * h
    xyBc[m, 3*nCell:, 1] = 0.0
  # inner coordinates
  m = 0
  for i in range(1, nCell+1):
    for j in range(1, nCell+1):
      xyIn[m,:] = [(i-0.5)*h, (j-0.5)*h]
      m += 1
  # colloction points' cooridates
  for m in range(0, batchSize,  nCellPerBatch):
    for mm in range(nBcCell):
      # inner
      xy[m:m+nCellPerBatch, mm, :] = xyIn[:,:]
  # set bc and label per solution
  solBatchBegin = solBegin
  while 1:
    if solBatchBegin + nSolPerBatch > solEnd: solBatchBegin = solBegin
    for s in range(solBatchBegin, solBatchBegin + nSolPerBatch):
      ss = s - solBatchBegin
      # bc for this solution
      solBc[:nCell]          = 0.5*(p[s, 0, 1:-1] + p[s,1,1:-1])
      solBc[nCell:2*nCell]   = 0.5*(p[s, 1:-1, -1] + p[s, 1:-1, -2])
      solBc[2*nCell:3*nCell] = 0.5*(p[s, -1, -2:0:-1] + p[s, -2, -2:0:-1])
      solBc[3*nCell:]        = 0.5*(p[s, -2:0:-1, 0] + p[s, -2:0:-1, 1])
      for m in range(ss*nCellPerBatch, (ss+1)*nCellPerBatch): bc[m,:] = solBc[:]
      # label
      start = ss * nCellPerBatch
      label[start:start+nInCell] = p[s,1:-1,1:-1].reshape((nInCell,))
    bc    = (bc    - pMin) / (pMax - pMin)
    label = (label - pMin) / (pMax - pMin)
    solBatchBegin += nSolPerBatch

    # return based on model type
    if bcXyOnly:
      yield [bc, np.squeeze(xy[:,0,:])], label
    else:
      yield [bc, xyBc, xy], label


def BcXybcXyWGen(p, solBegin, solEnd, nSolPerBatch, iStride=2, jStride=2):
  nCell     = p.shape[1] - 2
  nBcCell   = 4 * nCell
  nInCell   = (nCell // iStride) * (nCell // jStride)
  nCellPerBatch = nBcCell + nInCell
  h         = 1.0 / nCell
  pMax      = np.amax(p)
  pMin      = np.amin(p)
  batchSize = nSolPerBatch * (nBcCell + nInCell)
  bc        = np.zeros((batchSize, nBcCell))
  solBc     = np.zeros(nBcCell)
  w         = np.zeros((batchSize,2))
  xy        = np.zeros((batchSize ,nBcCell, 2))
  xyIn      = np.zeros((nInCell,2))
  xyBc      = np.zeros((batchSize, nBcCell, 2))
  label     = np.zeros((batchSize))
  # weights, xyBc, and xy does not change with solutions.
  # Those data duplicate for each solution, even each cell, which are highly
  # ineffecient for memory allocation and needs to be optimized asap.
  # boundary coordinates
  for m in range(batchSize):
    xyBc[m, :nCell, 0] = 0.0
    xyBc[m, :nCell, 1] = (np.arange(nCell) + 0.5) * h
    xyBc[m, nCell:2*nCell, 0] = (np.arange(nCell) + 0.5) * h
    xyBc[m, nCell:2*nCell, 1] = 1.0
    xyBc[m, 2*nCell:3*nCell, 0] = 1.0
    xyBc[m, 2*nCell:3*nCell, 1] = (np.arange(nCell,0,-1) - 0.5) * h
    xyBc[m, 3*nCell:, 0] = (np.arange(nCell,0,-1) - 0.5) * h
    xyBc[m, 3*nCell:, 1] = 0.0
  # inner coordinates
  m = 0
  for i in range(1, nCell+1, iStride):
    for j in range(1, nCell+1, jStride):
      xyIn[m,:] = [(i-0.5)*h, (j-0.5)*h]
      m += 1
  # colloction points' cooridates
  for m in range(0, batchSize,  nCellPerBatch):
    for mm in range(nBcCell):
      # boundary
      xy[m:m+nBcCell, mm, :] = xyBc[0,:,:]
      # inner
      xy[m+nBcCell:m+nCellPerBatch, mm, :] = xyIn[:,:]
  # weights
  for m in range(0, batchSize, nCellPerBatch):
    w[m:m+nBcCell, 0] = 1.0
    w[m:m+nBcCell, 1] = 0.0
    w[m+nBcCell:m+nCellPerBatch, 0] = 0.0
    w[m+nBcCell:m+nCellPerBatch, 1] = 1.0
  # set bc and label per solution
  solBatchBegin = solBegin
  while 1:
    if solBatchBegin + nSolPerBatch > solEnd: solBatchBegin = solBegin
    for s in range(solBatchBegin, solBatchBegin + nSolPerBatch):
      ss = s - solBatchBegin
      # bc for this solution
      solBc[:nCell]          = 0.5*(p[s, 0, 1:-1] + p[s,1,1:-1])
      solBc[nCell:2*nCell]   = 0.5*(p[s, 1:-1, -1] + p[s, 1:-1, -2])
      solBc[2*nCell:3*nCell] = 0.5*(p[s, -1, -2:0:-1] + p[s, -2, -2:0:-1])
      solBc[3*nCell:]        = 0.5*(p[s, -2:0:-1, 0] + p[s, -2:0:-1, 1])
      for m in range(ss*nCellPerBatch, (ss+1)*nCellPerBatch): bc[m,:] = solBc[:]
      # label's boundary part
      start = ss * nCellPerBatch
      label[start:start+nBcCell] = solBc[:]
      # label's inner part
      start += nBcCell
      label[start:start+nInCell] = p[s,1:-1:iStride,1:-1:jStride].reshape((nInCell,))
    bc    = (bc    - pMin) / (pMax - pMin)
    label = (label - pMin) / (pMax - pMin)
    solBatchBegin += nSolPerBatch
    yield [bc, xyBc, xy, w], label


def BcXybcXyWMixGen(p, points, solBegin, solEnd, nSolPerBatch, \
                    nDataPoint1D=0, normalize=True, bcXyOnly=True,\
                    length=1.0):
  '''
  There are there type of points: boundary, inner data, and inner
  collocation points. The first two kinds lie on cell centers.
  '''
  # number of boudnary, collocation points
  nCell     = p.shape[1] - 2
  nBcCell   = 4 * nCell
  nColPnt   = points.shape[1]
  # determine number of inner data points
  nDatPnt   = nDataPoint1D * nDataPoint1D
  if nDatPnt != 0:
    datPntI   = np.linspace(1, nCell, nDataPoint1D, dtype=int)
    datPntJ   = np.linspace(1, nCell, nDataPoint1D, dtype=int)
  # total number of points for one solution
  nPntPerSol = nBcCell + nDatPnt + nColPnt
  # batchsize is number of solution x number of all points
  batchSize   = nSolPerBatch * nPntPerSol
  # local variables
  h         = length / nCell
  pMax      = np.amax(p)
  pMin      = np.amin(p)
  solBc     = np.zeros(nBcCell)
  if nDatPnt != 0:
    xyDat   = np.zeros((nDataPoint1D, nDataPoint1D, 2))    
  # allocate inputs for network
  bc        = np.zeros((batchSize, nBcCell))
  w         = np.zeros((batchSize,2))
  xy        = np.zeros((batchSize ,nBcCell, 2))
  xyBc      = np.zeros((batchSize, nBcCell, 2))
  label     = np.zeros((batchSize))
  # weights, xyBc, does not change with solutions.
  # Those data duplicate for each solution, even each cell, which are highly
  # ineffecient for memory allocation and needs to be optimized asap.
  # boundary coordinates
  for m in range(batchSize):
    xyBc[m, :nCell, 0] = 0.0
    xyBc[m, :nCell, 1] = (np.arange(nCell) + 0.5) * h
    xyBc[m, nCell:2*nCell, 0] = (np.arange(nCell) + 0.5) * h
    xyBc[m, nCell:2*nCell, 1] = 1.0
    xyBc[m, 2*nCell:3*nCell, 0] = 1.0
    xyBc[m, 2*nCell:3*nCell, 1] = (np.arange(nCell,0,-1) - 0.5) * h
    xyBc[m, 3*nCell:, 0] = (np.arange(nCell,0,-1) - 0.5) * h
    xyBc[m, 3*nCell:, 1] = 0.0
  # add boundary points to xy input for the network
  for m in range(0, batchSize, nPntPerSol):
    for mm in range(nBcCell):
      xy[m:m+nBcCell, mm, :] = xyBc[0,:,:]
  # set xy for data points if any
  if nDatPnt > 0:
    for i in range(nDataPoint1D):
      for j in range(nDataPoint1D):
        xyDat[i, j, 0] = (datPntI[i] - 0.5) * h
        xyDat[i, j, 1] = (datPntJ[j] - 0.5) * h
    for m in range(0, batchSize, nPntPerSol):
      for mm in range(nBcCell):
        xy[m+nBcCell:m+nBcCell+nDatPnt, mm, :] = xyDat.reshape((nDatPnt, 2))
  # mark to distinguish boundary and data between collocation
  for m in range(0, batchSize, nPntPerSol):
    s, e = m, m+nBcCell+nDatPnt
    w[s:e, 0] = 1.0
    w[s:e, 1] = 0.0
    s, e = e, m+nPntPerSol
    w[s:e, 0] = 0.0
    w[s:e, 1] = 1.0
  # print basic info
  print('--------------------------------')
  print('generator:')
  print('{} boundary points'.format(nBcCell))
  print('{} data points'.format(nDatPnt))
  print('{} collocation points'.format(nColPnt))
  if bcXyOnly:
    print('output [bc, xy, w], label')
  else:
    print('output [bc, xyBc, xy, w], label')
  print('--------------------------------')
  # set bc and label per solution
  solBatchBegin = solBegin
  while 1:
    if solBatchBegin + nSolPerBatch > solEnd: solBatchBegin = solBegin
    for s in range(solBatchBegin, solBatchBegin + nSolPerBatch):
      ss = s - solBatchBegin
      # bc for this solution
      solBc[:nCell]          = 0.5*(p[s, 0, 1:-1] + p[s,1,1:-1])
      solBc[nCell:2*nCell]   = 0.5*(p[s, 1:-1, -1] + p[s, 1:-1, -2])
      solBc[2*nCell:3*nCell] = 0.5*(p[s, -1, -2:0:-1] + p[s, -2, -2:0:-1])
      solBc[3*nCell:]        = 0.5*(p[s, -2:0:-1, 0] + p[s, -2:0:-1, 1])
      for m in range(ss*nPntPerSol, (ss+1)*nPntPerSol): bc[m,:] = solBc[:]
      # label's boundary part
      start = ss * nPntPerSol
      label[start:start+nBcCell] = solBc[:]
      # label's data part if any
      if nDatPnt != 0:
        start = ss * nPntPerSol + nBcCell
        m = 0
        for i in range(nDataPoint1D):
          for j in range(nDataPoint1D):
            label[start+m] = p[s, datPntI[i], datPntJ[j]]
            m += 1
      # label's collocation part, no solution them
      start = ss * nPntPerSol + nBcCell + nDatPnt
      label[start:start+nColPnt] = 0.0
      # collocation points
      for mm in range(nBcCell):
        xy[start:start+nColPnt, mm, :] = points[s, :, :]
    # normalization
    if normalize:
      bc    = (bc    - pMin) / (pMax - pMin)
      label = (label - pMin) / (pMax - pMin)
    # move to next batch
    solBatchBegin += nSolPerBatch

    # return based on model type
    if bcXyOnly:
      yield [bc, np.squeeze(xy[:,0,:]), w], label
    else:
      yield [bc, xyBc, xy, w], label


def select_colloc_band(nCell, band, stride):
  ''' 
  Select collocation points in a [0,1]x[0,1] domain discretized uniformly by
  nCell x nCell cells with a halo layer. 
  The collocation points consist of the points inside the band of the input width 
  starting from the boundary and the points in the square treaks begining from 
  the inner boundary of the band and marking toward the center of domain with a 
  given stride.
  The the indices of collocation points
  '''
  streakList = [i for i in range(band)] + [i for i in range(band, nCell//2, stride)]  
  print(streakList)
  # number of collocation points
  nPoint = 0
  for s in streakList:
    nPoint += 4 * (nCell - 2*s) - 4
  # allocate indices and coordinates of collocation points
  idx = np.zeros((nPoint, 2), dtype=np.int)
  # setup indices and coordinates
  start = 0
  for s in streakList:
    l = nCell - 2*s - 1
    # i-
    idx[start:start+l, 0] = 1 + s
    idx[start:start+l, 1] = np.arange(1+s, nCell-s)
    start += l
    # j + 
    idx[start:start+l, 0] = np.arange(1+s, nCell-s)
    idx[start:start+l, 1] = nCell - s
    start += l
    # i + 
    idx[start:start+l, 0] = nCell - s
    idx[start:start+l, 1] = np.arange(nCell-s, 1+s, -1)
    start += l
    # j -
    idx[start:start+l, 0] = np.arange(nCell-s, 1+s, -1)
    idx[start:start+l, 1] = 1 + s
    start += l
  assert start == nPoint

  return idx


def BcXybcXyWBandGen(p, solBegin, solEnd, nSolPerBatch, band=4, stride=2, \
                     normalize=True, bcXyOnly=True, length=1.0):
  nCell     = p.shape[1] - 2
  nBcCell   = 4 * nCell
  # index of collocation points
  idxColloc = select_colloc_band(nCell, band, stride) 
  nInCell   = idxColloc.shape[0]
  print('{} boundary points'.format(nBcCell))
  print('{} collocation points'.format(nInCell))
  #
  nCellPerBatch = nBcCell + nInCell
  batchSize = nSolPerBatch * (nBcCell + nInCell)
  h         = length / nCell
  pMax      = np.amax(p)
  pMin      = np.amin(p)
  # allocate variables
  bc        = np.zeros((batchSize, nBcCell))
  solBc     = np.zeros(nBcCell)
  w         = np.zeros((batchSize,2))
  xy        = np.zeros((batchSize ,nBcCell, 2))
  xyIn      = np.zeros((nInCell,2))
  xyBc      = np.zeros((batchSize, nBcCell, 2))
  label     = np.zeros((batchSize))
  # weights, xyBc, and xy does not change with solutions.
  # Those data duplicate for each solution, even each cell, which are highly
  # ineffecient for memory allocation and needs to be optimized asap.
  # boundary coordinates
  for m in range(batchSize):
    xyBc[m, :nCell, 0] = 0.0
    xyBc[m, :nCell, 1] = (np.arange(nCell) + 0.5) * h
    xyBc[m, nCell:2*nCell, 0] = (np.arange(nCell) + 0.5) * h
    xyBc[m, nCell:2*nCell, 1] = 1.0
    xyBc[m, 2*nCell:3*nCell, 0] = 1.0
    xyBc[m, 2*nCell:3*nCell, 1] = (np.arange(nCell,0,-1) - 0.5) * h
    xyBc[m, 3*nCell:, 0] = (np.arange(nCell,0,-1) - 0.5) * h
    xyBc[m, 3*nCell:, 1] = 0.0
  # inner coordinates
  xyIn = (idxColloc - 0.5) * h
  # colloction points' cooridates
  for m in range(0, batchSize,  nCellPerBatch):
    for mm in range(nBcCell):
      # boundary
      xy[m:m+nBcCell, mm, :] = xyBc[0,:,:]
      # inner
      xy[m+nBcCell:m+nCellPerBatch, mm, :] = xyIn[:,:]
  # weights
  for m in range(0, batchSize, nCellPerBatch):
    w[m:m+nBcCell, 0] = 1.0
    w[m:m+nBcCell, 1] = 0.0
    w[m+nBcCell:m+nCellPerBatch, 0] = 0.0
    w[m+nBcCell:m+nCellPerBatch, 1] = 1.0
  # set bc and label per solution
  solBatchBegin = solBegin
  while 1:
    if solBatchBegin + nSolPerBatch > solEnd: solBatchBegin = solBegin
    for s in range(solBatchBegin, solBatchBegin + nSolPerBatch):
      ss = s - solBatchBegin
      # bc for this solution
      solBc[:nCell]          = 0.5*(p[s, 0, 1:-1] + p[s,1,1:-1])
      solBc[nCell:2*nCell]   = 0.5*(p[s, 1:-1, -1] + p[s, 1:-1, -2])
      solBc[2*nCell:3*nCell] = 0.5*(p[s, -1, -2:0:-1] + p[s, -2, -2:0:-1])
      solBc[3*nCell:]        = 0.5*(p[s, -2:0:-1, 0] + p[s, -2:0:-1, 1])
      for m in range(ss*nCellPerBatch, (ss+1)*nCellPerBatch): bc[m,:] = solBc[:]
      # label's boundary part
      start = ss * nCellPerBatch
      label[start:start+nBcCell] = solBc[:]
      # label's inner part
      start += nBcCell
      for m in range(nInCell):
        label[start+m] = p[s, idxColloc[m,0], idxColloc[m,1]]
    if normalize:
      bc    = (bc    - pMin) / (pMax - pMin)
      label = (label - pMin) / (pMax - pMin)
    solBatchBegin += nSolPerBatch

    # return based on model type
    if bcXyOnly:
      yield [bc, np.squeeze(xy[:,0,:]), w], label
    else:
      yield [bc, xyBc, xy, w], label


@tf.function
def max_abs_grad(grads):
  gMaxList = [tf.reduce_max(tf.abs(g)) for g in grads]
  return tf.reduce_max(tf.stack(gMaxList))


@tf.function
def avg_abs_grad(grads):
  gSumList = [tf.reduce_sum(tf.abs(g))  for g in grads]
  ngList   = [tf.reduce_sum(tf.size(g)) for g in grads]
  gSum = tf.add_n(gSumList)
  ng   = tf.add_n(ngList)
  return gSum/tf.cast(ng, tf.float32)


# distribute collocation points
def adapt_collocation(gradNorm, xy, solBegin, stride=2, length=1.0,\
                      nearBoundary=None): 
  nPoint = xy.shape[1]
  nSol  = gradNorm.shape[0]
  nCell = gradNorm.shape[1]
  h     = length / nCell
  for s in range(solBegin, solBegin+nSol):
    ss = s - solBegin
    # get number of points in each cell
    nSampleCell = (nCell//stride) * (nCell//stride)
    nPntInCell  = np.zeros(nSampleCell, dtype=int)
    density = gradNorm[ss,:,:] / np.sum(gradNorm[ss,:,:])
    if nearBoundary != None:
      density = np.power(density, nearBoundary) 
      density = density / np.sum(density)
    m = 0
    for i  in range(0, nCell, stride):
      for j in range(0, nCell, stride):
        # number of points in current cell: stride x stride
        cellDensity   = np.sum(density[i:i+stride, j:j+stride])
        nPntInCell[m] = int(np.rint(cellDensity * nPoint))
        m += 1
    # adjust number of points to fit nPoint
    diff = nPoint - np.sum(nPntInCell)
    m    = 0
    if diff > 0:
      while diff > 0:
        nPntInCell[m] += 1
        diff -= 1
        m     = (m + 1) % nSampleCell
    else:
      while diff < 0:
        if nPntInCell[m] > 1:
          nPntInCell[m] -= 1
          diff += 1
        m = (m + 1) % nSampleCell
    # generate points
    m = 0
    pntStart, pntEnd = 0, 0
    for i  in range(0, nCell, stride):
      for j in range(0, nCell, stride):
        pntStart, pntEnd = pntEnd, pntEnd + nPntInCell[m]
        xy[s, pntStart:pntEnd, :]  = np.random.rand(nPntInCell[m], 2) * stride * h
        xy[s, pntStart:pntEnd, 0] += i * h
        xy[s, pntStart:pntEnd, 1] += j * h
        m += 1
