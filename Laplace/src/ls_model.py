import sys
sys.path.append('../src')
import h5py as h5
import numpy as np
import matplotlib.pyplot as plt
import argparse
import time
import os


class LeastSquareSurrogate():
  def __init__(self, domainShape):
    assert len(domainShape) == 2
    self.shape = domainShape
    nBcPoint   = 2 * (self.shape[0] + self.shape[1])
    self.coef  = np.zeros((np.prod(self.shape), nBcPoint),\
                           dtype=np.single)


  def build_model(self, p):
    assert p.shape[1]-2 == self.shape[0] and p.shape[2]-2 == self.shape[1]

    nTrain   = int(0.9 * p.shape[0])
    nValid   = p.shape[0] - nTrain
    nBcPoint = 2 * (self.shape[0] + self.shape[1])

    matTrain = np.zeros((nTrain, nBcPoint))
    for m in range(nTrain):
      # i-
      s, e = 0, self.shape[1]
      matTrain[m, s:e] = 0.5*(p[m, 0, 1:-1] + p[m, 1, 1:-1])
      # j+
      s, e = e, e + self.shape[0]
      matTrain[m, s:e] = 0.5*(p[m, 1:-1, -1] + p[m, 1:-1, -2])
      # i+
      s, e = e, e + self.shape[1]
      matTrain[m, s:e] = 0.5*(p[m, -1, -2:0:-1] + p[m, -2, -2:0:-1])
      # j-
      s, e = e, e + self.shape[0]
      matTrain[m, s:e] = 0.5*(p[m, -2:0:-1, 0] + p[m, -2:0:-1, 1])
    print ('LS model build matrix, done')

    # compute coef using least square
    iCell = 0
    for i in range(1, 1+self.shape[0]):
      for j in range(1, 1+self.shape[1]):
        self.coef[iCell, :] = np.linalg.lstsq(matTrain, p[:nTrain, i, j])[0]
        iCell += 1
    print ('LS model compute LS coefficents, done')

    # matrix for inference
    matValid = np.zeros((nValid, nBcPoint))                                          
    for m in range(nTrain, p.shape[0]):                                                  
      s, e = 0, self.shape[1]
      matValid[m-nTrain, s:e] = 0.5*(p[m, 0, 1:-1] + p[m,1,1:-1])       
      s, e = e, e + self.shape[0]
      matValid[m-nTrain, s:e] = 0.5*(p[m, 1:-1, -1] + p[m, 1:-1, -2])   
      s, e = e, e + self.shape[1]
      matValid[m-nTrain, s:e] = 0.5*(p[m, -1, -2:0:-1] + p[m, -2, -2:0:-1])
      s, e = e, e + self.shape[0]
      matValid[m-nTrain, s:e] = 0.5*(p[m, -2:0:-1, 0] + p[m, -2:0:-1, 1])

    # inference
    pPred = np.zeros((nValid, p.shape[1], p.shape[2]))
    pPred[...] = p[nTrain:, :, :]
    for m in range(nValid):
      iCell = 0
      for i in range(1, 1+self.shape[0]):
        for j in range(1, 1+self.shape[1]):
          pPred[m, i, j] = np.dot(matValid[m,:], self.coef[iCell,:])
          iCell += 1
    print('LS model infer validation cases, done')

    #  check error                                                                  
    err      = np.absolute(pPred[:,1:-1,1:-1] - p[nTrain:,1:-1,1:-1])          
    residual = np.absolute( pPred[:,2:,1:-1] + pPred[:,:-2,1:-1] + pPred[:,1:-1,2:] \
                          + pPred[:,1:-1,:-2] - 4.0*pPred[:,1:-1,1:-1])             
    print('LS model dtype ', end ='')
    print(self.coef.dtype)
    print('LS model Error    {:.4e} {:.4e}'.format(np.amax(err), np.mean(err)))              
    print('LS model Residual {:.4e} {:.4e}'.format(np.amax(residual), np.mean(residual))) 


  def predict(self, g):
    assert g.shape[0] == self.shape[0] and g.shape[1] == self.shape[1] 
    iCell = 0
    for i in range(1, 1+self.shape[0]):
      for j in range(1, 1+self.shape[1]):
        g.vars[i, j, 0] = np.dot(self.coef[iCell, :], g.bc[:])
        iCell += 1





























