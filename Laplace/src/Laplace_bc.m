Plot_Flag = 1;
rng(1523)

%% Geometry
% Rectangle is code 3, 4 sides, followed by x-coordinates and then y-coordinates of the vertecies
width = 1;
height = 1;
R1 = [3, 4, 0, width, width, 0, 0, 0, height, height]';
% Names for the geometric objects
ns = (char('R1'))';
% Set formula
sf = 'R1';
% Create geometry
g = decsg(R1,sf,ns);

% Create geometry model
model = createpde;
model_homo = createpde;

% Include the geometry in the model and view the geometry
geometryFromEdges(model, g);
geometryFromEdges(model_homo, g);
if Plot_Flag ~= 0
    figure(102);subplot(2, 2, 1)
    pdegplot(model, 'EdgeLabels', 'on'); axis equal; axis([0 width 0 height])
end
%% Mesh
generateMesh(model,'Hmax',0.005);
generateMesh(model_homo,'Hmax',0.005);
if Plot_Flag ~= 0
    figure(102);subplot(2, 2, 2)
    pdemesh(model); axis equal; axis([0 width 0 height])
end
%% Define PDE

F_Fun = @(location, state) 20*pi^2*sin(2*pi*location.x).*sin(4*pi*location.y);
specifyCoefficients(model,'m',0,'d',0,'c',1,'a',-1,'f',F_Fun);
specifyCoefficients(model_homo,'m',0,'d',0,'c',1,'a',-1,'f',0);
%% DOE for generating BCs
umin = 1; %min u value at the BCs
umax = -1;
wmin = 0.3; %min w value used in GP
wmax = 1;
pmin = 2;
pmax = 2;
Samples = 64;
nCell = 32;
n = 2; %number of BCs to generate
nd = 4; % number of points across each edge. 4 edge in total (see edge number in figure!) BUT they share some nodes! So: 
%1) total number of nodes across the circumstance: 4(nd-1)
%2) at the shared nodes (e.g., node 1 is shared between edge 1 and 4), the
%GP should generate the same value for u. lines 53, 71-74 ensure this!


d = linspace(0, 4*width, 4*nd)'; 
%d = [d', (d(end) +d(2)), ]';
d = [(d(1:(end-1))' -4*width), d', (d(2:end)' +4*width) ]';
A = sobolset((4*nd) , 'skip', 1e3, 'Leap', 2);
u = A(1: n, 1: 4*(nd) -1 );
u = [u, u, u, u(:,1)]*(umax - umin) + umin; 
w = A(1:n, 4*(nd)  : 4*(nd) )*(wmax - wmin) + wmin;
p = 2*ones(n,length(w));
bcs = zeros(4*nCell, n);
%% Loop through BCs
Time = tic;
for i = 1: n
    if(mod(i,100) == 0)
        fprintf('i = %i \n', i)
    end 
    Rinvu = zeros(length(d), 1);
    R = CorrMat(d, d, w(i, 1), p(i, 1));
    temp = u(i, :)';

    Rinvu(:, 1) = R\temp;
  
    BC1 = @(location, state) (Rinvu(:, 1)')*exp(-10^w(i, 1) * abs(reshape(repmat(d, length(location.x), 1) - reshape(repmat((location.x)', 1, length(d))', length(d)*length(location.x), 1), length(d), length(location.x))).^ p(i, 1));
    BC2 = @(location, state) (Rinvu(:, 1)')*exp(-10^w(i, 1) * abs(reshape(repmat(d, length(location.y), 1) - reshape(repmat((location.y + width)', 1, length(d))', length(d)*length(location.y), 1), length(d), length(location.y))).^ p(i, 1));
    BC3 = @(location, state) (Rinvu(:, 1)')*exp(-10^w(i, 1) * abs(reshape(repmat(d, length(location.x), 1) - reshape(repmat(((width - location.x) + width + height )', 1, length(d))', length(d)*length(location.x), 1), length(d), length(location.x))).^ p(i, 1));
    BC4 = @(location, state) (Rinvu(:, 1)')*exp(-10^w(i, 1) * abs(reshape(repmat(d, length(location.y), 1) - reshape(repmat(((height - location.y) + 2*width + height )', 1, length(d))', length(d)*length(location.y), 1), length(d), length(location.y))).^ p(i, 1));

    f1 = @(x) (Rinvu(:, 1)')*exp(-10^w(i, 1) * abs(reshape(repmat(d, length(x), 1) - reshape(repmat((x)', 1, length(d))', length(d)*length(x), 1), length(d), length(x))).^ p(i, 1));
    f2 = @(y) (Rinvu(:, 1)')*exp(-10^w(i, 1) * abs(reshape(repmat(d, length(y), 1) - reshape(repmat((y + width)', 1, length(d))', length(d)*length(y), 1), length(d), length(y))).^ p(i, 1));
    f3 = @(x) (Rinvu(:, 1)')*exp(-10^w(i, 1) * abs(reshape(repmat(d, length(x), 1) - reshape(repmat(((width - x) + width + height )', 1, length(d))', length(d)*length(x), 1), length(d), length(x))).^ p(i, 1));
    f4 = @(y) (Rinvu(:, 1)')*exp(-10^w(i, 1) * abs(reshape(repmat(d, length(y), 1) - reshape(repmat(((height - y) + 2*width + height )', 1, length(d))', length(d)*length(y), 1), length(d), length(y))).^ p(i, 1));
 
    x  = linspace(0, width, nCell+2);
    y1 = f1(x);
    y2 = f2(x);
    y3 = flip(f3(x));
    y4 = flip(f4(x));
    plot(x,y1,x+width,y2,x+2*width,y3,x+3*width,y4)
    hold on
    
    bcs(        1:  nCell+1,i) = y1(1:end-1);
    bcs(  nCell+2:2*nCell+2,i) = y2(1:end-1);
    bcs(2*nCell+3:3*nCell+3,i) = y3(1:end-1);
    bcs(3*nCell+4:4*nCell+4,i) = y4(1:end-1);
    
end     

Total_time = toc(Time);
%fname = 'bcs.h5';
%h5create(fname, '/bcs', size(bcs));
%h5write(fname, '/bcs', bcs);
%h5disp(fname);

% Mesh = results.Mesh;
%%
%%
function R = CorrMat(X1, X2, W, P)
if size(X1, 2) ~= size(X2, 2)
    error('The number of columns of X1 and X2 should be the same.')
end

[n, ~] = size(X1);
m = size(X2, 1);
R = zeros(n, m);

W = (10.^W).^(1./P);
X1 = X1.*repmat(W, n, 1);
X2 = X2.*repmat(W, m, 1);	
if n >= m
	for i = 1: m; R(:, i) = sum(abs(X1 - repmat(X2(i, :), n, 1)).^P, 2); end
else
	for i = 1: n; R(i, :) = sum(abs(repmat(X1(i, :), m, 1) - X2).^P, 2); end
end
R = exp(-R);

end


function Write_H5_File(Name, u_save, BCs_save)
   File = strcat(Name,'.h5');
   h5create(File,'/U',size(u_save))
   h5write(File, '/U', u_save)
   h5create(File,'/BC',size(BCs_save))
   h5write(File, '/BC', BCs_save)
   h5disp(File)
end 

function Plot_BC(BC1, BC2, BC3, BC4, Width, Height, Points)
    W = linspace(0,Width, Points)
    H = linspace(0,Height, Points)
    Z = zeros(Points,1)
    plot(W, BC1)
end 
