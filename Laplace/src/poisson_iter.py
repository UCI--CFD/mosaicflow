import numpy as np
import pyamg
from pyamg.gallery import poisson
import argparse
import Poisson_eqn as psnEqn

PI = np.pi

# command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-dsize', '--domain_size', nargs=2, help='domain size', type=int)
parser.add_argument('-gsize', '--genome_size', nargs=2, help='genome size', type=int)
args = parser.parse_args()

nx, ny = args.domain_size
gx, gy = args.genome_size
assert ny % gy == 0
assert nx % gx == 0

# domain length and grid space
h  = 1.0/gx if gx < gy else 1.0/gy
lx = nx * h
ly = ny * h
print('length {:f} {:f}, grid step {:f}'.format(lx, ly, h))

# matrix
A = poisson((nx, ny), format='csr')

# variable with random dirichlet bc
p = np.zeros((nx+2, ny+2))
psnEqn.set_random_halo(p)

# rhs, using rhs function values
rhs = np.zeros((nx, ny))
#for i in range(0, nx):
  #for j in range(0, ny):
    #x = (i + 0.5) * h
    #y = (j + 0.5) * h
    #rhs[i,j] = -20*PI*PI*np.sin(2*PI*x)*np.sin(4*PI*y) * (-h*h)

# solver for whole domain
psnEqn.solve(p, rhs)

# data buffer for  genomes
maxIter = 200
pIter0 = np.copy(p)
pIter1 = np.copy(p)
err    = np.zeros(p.shape)
pIter0[1:-1,1:-1] = 0.0
pIter1[1:-1,1:-1] = 0.0
Ag = poisson((gx, gy), format='csr')

# iterate 
for i in range(maxIter):
  # basic genomes
  for gi in range(1, nx+1, gx):
    for gj in range(1, ny+1, gy):
      psnEqn.solve_genome(pIter0, Ag, rhs, gi, gj, gx, gy, pIter1)
  err = np.absolute(p - pIter1)
  errL0 = np.amax(err)
  print('{} basic   genomes error {:e} {:e}'.format(i, errL0, np.sum(err)/nx/ny))
  if errL0 < 1.0e-10: break
  pIter0[:,:] = pIter1[:,:]
  # genomes shift along i
  if gx//2 < nx-gx:
    for gi in range(1+gx//2, nx+1-gx, gx):
      for gj in range(1, ny+1, gy):
        psnEqn.solve_genome(pIter0, Ag, rhs, gi, gj, gx, gy, pIter1)
    err = np.absolute(p - pIter1)
    pIter0[:,:] = pIter1[:,:]
    errL0 = np.amax(err)
    print('{} i-shift genomes error {:e} {:e}'.format(i, errL0, np.sum(err)/nx/ny))
    if errL0 < 1.0e-10: break
  # genomes shift along j
  if gy//2 < ny-gy:
    for gi in range(1, nx+1, gx):
      for gj in range(1+gy//2, ny+1-gy, gy):
        psnEqn.solve_genome(pIter0, Ag, rhs, gi, gj, gx, gy, pIter1)
    err = np.absolute(p - pIter1)
    errL0 = np.amax(err)
    print('{} j-shift genomes error {:e} {:e}'.format(i, errL0, np.sum(err)/nx/ny))
    if errL0 < 1.0e-10: break
    pIter0[:,:] = pIter1[:,:]
